using CVU.Condica.Persistance.ReferenceData;

namespace CVU.Condica.Application.Account.Models
{
    public class CurrentUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public Roles Role { get; set; }
        public int CompanyId { get; set; }
    }
}
