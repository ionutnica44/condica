﻿using CVU.Condica.Application.VacationRequestApprovals.Commands;
using CVU.Condica.Application.VacationRequestApprovals.Models;
using CVU.Condica.Application.VacationRequestApprovals.Queries;
using CVU.Condica.Application.Vacations.Commands;
using CVU.Condica.Application.Vacations.Models;
using CVU.Condica.Application.Vacations.Queries;
using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Infrastructure.Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CVU.Condica.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class VacationsController : BaseController
    {
        private readonly IVacationService vacationService;
        public VacationsController(IVacationService vacationService)
        {
            this.vacationService = vacationService;
        }

        [HttpPost("{userId?}")]
        public IEnumerable<VacationDto> ListVacations(int? userId, VacationQuery vacationQuery)
        {
            vacationQuery.UserId = userId;

            var mediatorResponse = Mediator.Send(vacationQuery).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);
            Response.Headers.Add("VacationUserStatus", JsonConvert.SerializeObject(mediatorResponse.UserVacationStatus));
            Response.Headers.Add("TotalRemainingDays", mediatorResponse.UserVacationStatus.Sum(d => d.RemainingDays).ToString());
            return mediatorResponse.Items;
        }

        [HttpPost]
        public Task<Unit> CreateVacation([FromBody] CreateVacationCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPatch("{id}")]
        public Task<Unit> EditVacation(int id, [FromBody] EditVacationCommand command)
        {
            command.Id = id;
            return Mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public Task<Unit> DeleteVacation(int id, [FromBody] DeleteVacationCommand command)
        {
            command.Id = id;
            return Mediator.Send(command);
        }

        [HttpPost("request")]
        public IEnumerable<VacationRequestApprovalDto> ListVacationRequestApproval(VacationRequestApprovalQuery vacationRequestApprovalQuery)
        {
            var mediatorResponse = Mediator.Send(vacationRequestApprovalQuery).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);

            return mediatorResponse.Items;
        }

        [HttpPost("all-requests")]
        public IEnumerable<VacationRequestApprovalDto> ListVacationRequestApprovalFirstPriority(VacationRequestApprovalFirstPriorityQuery vacationRequestApprovalFirstPriorityQuery)
        {
            var mediatorResponse = Mediator.Send(vacationRequestApprovalFirstPriorityQuery).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);

            return mediatorResponse.Items;
        }

        [HttpPatch("request/{id}")]
        public Task<Unit> EditVacationRequestApproval(int id, [FromBody] EditVacationRequestApprovalCommand command)
        {
            command.Id = id;

            return Mediator.Send(command);
        }

        [HttpPatch("{vacationId}/all-request/")]
        public Task<Unit> EditVacationRequestApprovalFirstPriority(int vacationId, [FromBody] EditVacationRequestApprovalFirstPriorityCommand command)
        {
            command.VacationId = vacationId;

            return Mediator.Send(command);
        }

        [HttpPost("request-signed")]
        public IEnumerable<VacationRequestApprovalDto> ListVacationRequestApprovalAlreadySigned(VacationRequestApprovalAlreadySignedQuery vacationRequestApprovalQuery)
        {
            var mediatorResponse = Mediator.Send(vacationRequestApprovalQuery).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);

            return mediatorResponse.Items;
        }

        [HttpPatch("request-signed/{id}")]
        public Task<Unit> EditVacationRequestApprovalAlreadySigned(int id, [FromBody] EditVacationRequestApprovalAlreadySignedCommand command)
        {
            command.Id = id;

            return Mediator.Send(command);
        }

        [HttpGet("vacationsReport")]
        public FileContentResult VacationsReport([FromQuery] DateTime dateTime)
        {
            var byteArr = Mediator.Send(new VacationsReportQuery { DateTime = dateTime }).Result;
            return File(byteArr, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"Vacations report {dateTime.ToString("MMMM yyyy", CultureInfo.InvariantCulture)}.xlsx");
        }

        [HttpGet("updateVacationDays")]
        public bool UpdateVacationDays()
        {
            return vacationService.UpdateVacationDays();
        }

        [HttpGet("vacationStats")]
        public Task<IEnumerable<VacationStatsDto>> GetVacationStats()
        {
            return Mediator.Send(new VacationStatsQuery());
        }
    }
}