import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private _toast: ToastrService, private _router: Router) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {

                if (error.status === 401) {
                    this._toast.error('Error 401', error.message);
                    // if (this._authService.logOut()) {
                    //     location.reload(true);
                    // }
                    this._router.navigateByUrl('/auth/login');
                }

                if (error.status === 500) {
                    this._toast.error('Error', error.message);
                }

                if (error.status === 400) {
                    blobToText(error.error).subscribe(text => {
                        const errorObject = JSON.parse(text);
                        const validationObject = errorObject.Validation;

                        if (validationObject) {
                            let textValidation = '';
                            // tslint:disable-next-line:forin
                            for (const propr in errorObject.Validation) {
                                const propObj: string[] = errorObject.Validation[propr];
                                // tslint:disable-next-line:forin
                                for (const valMsg in propObj) {
                                    textValidation += propObj[valMsg] + '<br>';
                                }
                            }
                            this._toast.warning(textValidation);
                        } else {
                            this._toast.warning(errorObject.Message, '');
                        }
                    });
                }
                const message = error.message || error.status;

                return throwError(message);
            })
        );

        function blobToText(blob: any): Observable<string> {
            return new Observable<string>((observer: any) => {
                if (!blob) {
                    observer.next('');
                    observer.complete();
                } else {
                    const reader = new FileReader();
                    reader.onload = event => {
                        observer.next((<any>event.target).result);
                        observer.complete();
                    };
                    reader.readAsText(blob);
                }
            });
        }
    }
}
