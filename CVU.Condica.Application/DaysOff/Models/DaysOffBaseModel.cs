﻿using System;
using System.Collections.Generic;

namespace CVU.Condica.Application.DaysOff.Models
{
    public class DaysOffBaseModel
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<int> CustomUsersIds { get; set; }
        public bool ForEveryone { get; set; }
    }
}
