﻿using System;
using System.Linq.Expressions;

namespace CVU.Condica.Application.VacationRequestApprovals.Models
{
    public class VacationRequestApprovalDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int VacationId { get; set; }
        public string UserEmail { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public DateTime VacationStartDate { get; set; }
        public DateTime VacationEndDate { get; set; }
        public int VacationDuration { get; set; }
        public bool? Approved { get; set; }
        public int ApprovedBy { get; set; }
        public string Comment { get; set; }

        public static Expression<Func<Persistence.Models.VacationRequestApproval, VacationRequestApprovalDto>> Projection
        {
            get
            {
                return p => new VacationRequestApprovalDto()
                {
                    Id = p.Id,
                    UserId = p.Vacation.UserId,
                    VacationId = p.VacationId,
                    UserEmail = p.Vacation.User.Email,
                    UserFirstName = p.Vacation.User.FirstName,
                    UserLastName = p.Vacation.User.LastName,
                    VacationStartDate = p.Vacation.StartDate,
                    VacationEndDate = p.Vacation.EndDate,
                    VacationDuration = p.Vacation.NumberOfDays,
                    Approved = p.Approved,
                    ApprovedBy = p.SigningUserId,
                    Comment = p.Comment
                };
            }
        }

        public static VacationRequestApprovalDto Create(Persistence.Models.VacationRequestApproval VacationRequestApproval)
        {
            return Projection.Compile().Invoke(VacationRequestApproval);
        }
    }
}
