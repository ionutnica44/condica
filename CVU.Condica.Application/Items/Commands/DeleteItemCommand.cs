﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Items.Commands
{
    public class DeleteItemCommand : IRequest<Unit>
    {
        public int Id { get; set; }
    }

    public class DeleteItemCommandHandler : RequestHandler<DeleteItemCommand, Unit>
    {
        public DeleteItemCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Unit> Handle(DeleteItemCommand request, CancellationToken cancellationToken)
        {
            var item = appDbContext.Item.First(d => d.Id == request.Id);

            appDbContext.Remove(item);

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class DeleteItemCommandValidator : AbstractValidator<DeleteItemCommand>
    {
        public DeleteItemCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateDeleteItemCommand);
        }
    }

    public class DeleteItemCommandAuthorization : AuthorizationSetup<DeleteItemCommand>
    {
        public DeleteItemCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
