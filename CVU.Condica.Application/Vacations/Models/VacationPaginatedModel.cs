﻿using CVU.Condica.Common.Pagination;
using System.Collections.Generic;

namespace CVU.Condica.Application.Vacations.Models
{
    public class VacationPaginatedModel<T> : PaginatedModel<T>
    {
        public IEnumerable<VacationUserManagementDto> UserVacationStatus { get; set; }
        public VacationPaginatedModel(BasePagedList<T> list, IEnumerable<VacationUserManagementDto> userVacationStatus) : base(list)
        {
            UserVacationStatus = userVacationStatus;
        }
    }
}
