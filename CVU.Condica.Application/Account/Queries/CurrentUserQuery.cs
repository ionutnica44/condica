using CVU.Condica.Application.Account.Models;
using CVU.Condica.Common.Authorization;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Queries
{
    public class CurrentUserQuery : IRequest<UserDto>
    {
    }

    public class CurrentUserQueryHandler : RequestHandler<CurrentUserQuery, UserDto>
    {
        public CurrentUserQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<UserDto> Handle(CurrentUserQuery request, CancellationToken cancellationToken)
        {
            var userDb = appDbContext.User
                .Include(d => d.Role)
                .Include(d => d.Company)
                .Include(d => d.Position)
                .Include(d=>d.Items)
                .ThenInclude(d=>d.ItemType)
                .FirstOrDefault(d => d.Id == CurrentUser.Id);

            var user = UserDto.Create(userDb);

            return user;
        }
    }

    public class CurrentUserQueryAuthorizationHandler : AuthorizationSetup<CurrentUserQuery>
    {
        public CurrentUserQueryAuthorizationHandler()
        {
        }
    }
}
