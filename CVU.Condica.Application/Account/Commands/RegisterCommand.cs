using CVU.Condica.Application.Enums;
using CVU.Condica.Application.Interfaces;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Extensions;
using CVU.Condica.Common.Security;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{

    public class RegisterCommand : IRequest<int>
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }
        public string PhoneNumber { get; set; }
        public int CompanyId { get; set; }
        public int PositionId { get; set; }
        public bool CanSign { get; set; }
        public Dictionary<int, string> SigningUsers { get; set; }

        public static User Create(RegisterCommand model)
        {
            return new User
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = Crypto.HashPassword(AuthorizationVariables.Salt + StringExtensions.GenerateRandomString(8)),
                PhoneNumber = model.PhoneNumber,
                RoleId = model.RoleId,
                CreatedAt = DateTime.Now,
                LastUpdatedAt = DateTime.Now,
                IsActivated = false,
                CompanyId = model.CompanyId,
                PositionId = model.PositionId,
                SecurityCodeExpiresAt = DateTime.Now.AddMonths(15),
                SignPriorityId = (model.CanSign == true) ? (int)SignPriorities.Third : (int?)null,
                SecurityCode = StringExtensions.GenerateRandomString(12),
                VacationUserManagement = new List<VacationUserManagement> { new VacationUserManagement { TotalDays = 21, Year = DateTime.Now.Year.ToString() } },
                SigningUsers = model.SigningUsers
                                .Select(d => new SigningUser { SigningUserId = d.Key }).ToList()
            };
        }
    }

    public class RegisterCommandHandler : RequestHandler<RegisterCommand, int>
    {
        private readonly IEmailService emailService;
        private readonly IConfiguration configuration;

        public RegisterCommandHandler(
            IServiceProvider serviceProvider,
            IEmailService emailService,
            IConfiguration configuration) : base(serviceProvider)
        {
            this.emailService = emailService;
            this.configuration = configuration;
        }

        public override async Task<int> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            if (!request.SigningUsers.ContainsKey(1))
            {
                request.SigningUsers.Add(1, "Marius Dumitrescu");
            }

            if (!request.SigningUsers.ContainsKey(2))
            {
                request.SigningUsers.Add(2, "Iustinian Nita");
            }

            var user = RegisterCommand.Create(request);

            appDbContext.Add(user);
            appDbContext.SaveChanges();

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\UserRegister.txt");
#else
            var template = File.ReadAllText("UserRegister.txt");
#endif

            var hostName = $"{configuration["BaseUrl"]}/auth/confirm-account?SecurityCode={user.SecurityCode}";

            template = template.Replace("{BaseUrl}", hostName)
                .Replace("{FirstName}", user.FirstName);

            emailService.SendAsync(subject: "New account",
                body: template,
                from: "Do Not Reply",
                recipients: new List<string> { user.Email });

            return user.Id;
        }
    }

    public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
    {
        public RegisterCommandValidator(ValidationService validationService)
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(x => x).Custom(validationService.ValidateUserRegister);
        }
    }
}
