using CVU.Condica.Application.Enums;
using CVU.Condica.Application.Interfaces;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Extensions;
using CVU.Condica.Common.Security;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{
    public class ActivateCommand : IRequest
    {
        public string SecurityCode { get; set; }
    }

    public class ActivateCommandHandler : RequestHandler<ActivateCommand, Unit>
    {
        private readonly IEmailService emailService;
        private readonly IConfiguration configuration;
        public ActivateCommandHandler(IServiceProvider serviceProvider, IEmailService emailService, IConfiguration configuration) : base(serviceProvider)
        {
            this.emailService = emailService;
            this.configuration = configuration;
        }

        public override async Task<Unit> Handle(ActivateCommand request, CancellationToken cancellationToken)
        {
            var user = appDbContext.User.First(d => d.SecurityCode == request.SecurityCode);

            user.SecurityCode = null;
            user.IsActivated = true;
            user.SecurityCodeExpiresAt = null;

            var tempPassword = StringExtensions.GenerateRandomString(12);

            user.Password = Crypto.HashPassword(AuthorizationVariables.Salt + tempPassword);

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SendPassword.txt");
#else
            var template = File.ReadAllText("SendPassword.txt");
#endif

            var hostName = $"{configuration["BaseUrl"]}";

            template = template.Replace("{BaseUrl}", hostName)
                .Replace("{Password}", tempPassword)
                .Replace("{FirstName}", user.FirstName);

            emailService.SendAsync(subject: "New account",
                                   body: template,
                                   from: "Do Not Reply",
                                   recipients: new List<string> { user.Email });

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class ActivateCommandValidator : AbstractValidator<ActivateCommand>
    {
        public ActivateCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateActivateCommand);
        }
    }
}
