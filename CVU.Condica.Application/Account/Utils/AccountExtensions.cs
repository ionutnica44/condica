using CVU.Condica.Application.Enums;
using CVU.Condica.Common.Security;
using CVU.Condica.Persistence.Models;
using CVU.Utilities.ErrorHandling;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;

namespace CVU.Condica.Application.Account.Utils
{
    public static class AccountExtensions
    {
        public static ClaimsIdentity GetIdentity(this User User, string password)
        {
            if (!User.IsActivated)
            {
                throw new AppException($"User {User.Email} is not activated. Please confirm your email address before using application", HttpStatusCode.BadRequest);

            }
            if (User.IsBlocked)
            {
                throw new AppException($"User {User.Email} is locked. Please contact a system administrator.", HttpStatusCode.BadRequest);
            }

            var isCorrectPassword = Crypto.VerifyHashedPassword(User.Password, AuthorizationVariables.Salt + password);

            if (!isCorrectPassword)
            {
                throw new AppException($"Invalid password.", HttpStatusCode.BadRequest);
            }
            else
            {
                var identity = new GenericIdentity(User.Email, "Token");

                return new ClaimsIdentity(identity, null);
            }
        }

    }
}
