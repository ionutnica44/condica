using CVU.Condica.Persistence.Models;
using System;

namespace CVU.Condica.Persistence.Interfaces
{
    public interface ITrackingEntity
    {
        int Id { get; set; }

        int CreatedById { get; set; }
        int LastUpdatedById { get; set; }

        User CreatedBy { get; set; }
        User LastUpdatedBy { get; set; }

        DateTime CreatedAt { get; set; }
        DateTime LastUpdatedAt { get; set; }


    }
}
