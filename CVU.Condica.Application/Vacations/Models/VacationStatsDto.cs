﻿using CVU.Condica.Persistence.Models;
using System.Collections.Generic;

namespace CVU.Condica.Application.Vacations.Models
{
    public class VacationStatsDto
    {
        public string UserName { get; set; }
        public IEnumerable<VacationUserManagement> Vacations { get; set; }
    }
}
