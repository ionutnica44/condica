﻿using CVU.Condica.Application.Attendances.Commands;
using CVU.Condica.Application.DaysOff.Commands;
using CVU.Condica.Application.DaysOff.Models;
using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation.Validators;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace CVU.Condica.Application.Validation
{
    public class Utilities
    {
        private readonly AppDbContext appDbContext;
        private readonly ISession session;
        private readonly IVacationService vacationService;

        public Utilities(AppDbContext appDbContext, ISession session, IVacationService vacationService)
        {
            this.appDbContext = appDbContext;
            this.session = session;
            this.vacationService = vacationService;
        }

        #region Account

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                static string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool NameLength(string name)
        {
            if (name != null)
            {
                if (name.Length <= 2 || name.Length >= 50)
                {
                    return false;
                }
            }

            return true;
        }

        public bool CanRevokeSignPermission(int userId)
        {
            var userSignPriorityId = appDbContext.User.Where(d => d.Id == userId)
                .Select(d => d.SignPriorityId)
                .First();

            //dont leave things whis way!
            if (userId == session.CurrentUser.Id)
            {
                return true;
            }

            if (userSignPriorityId.HasValue && userSignPriorityId != (int)SignPriorities.Third)
            {
                return false;
            }

            return true;
        }

        public bool SelfBlock(int userId, bool isBlocked)
        {
            if (isBlocked == true && session.CurrentUser.Id == userId)
            {
                return true;
            }

            return false;
        }

        public bool CanNotDeleteSigningUsers(int id)
        {
            var signingUsers = appDbContext.SigningUser
                .Where(d => d.UserId == id)
                .Select(d => d.SigningUserId)
                .ToList();

            if (!signingUsers.Any())
            {
                return true;
            }

            if (signingUsers.Contains(1) && signingUsers.Contains(2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool PasswordLength(string password)
        {
            if (password != null)
            {
                if (password.Length < 6 && password.Length > 100)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool ValidatePassword(string password)
        {
            if (password != null)
            {
                if (!Regex.IsMatch(password, "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+~()_]).{8,}$"))
                {
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region Attendances

        public bool ValidateAttendance(EditAttendanceCommand request)
        {
            var attendanceDb = appDbContext.Attendance.FirstOrDefault(d => d.Id == request.AttendanceId);
            // Both values edited
            if (request.ArrivalTime.HasValue && request.DepartureTime.HasValue)
            {
                if (request.ArrivalTime.Value.Date != request.DepartureTime.Value.Date)
                {
                    return false;
                }
                if (request.ArrivalTime > request.DepartureTime)
                {
                    return false;
                }
                if (request.ArrivalTime.Value.Date != attendanceDb.ArrivalTime.Date)
                {
                    return false;
                }
            }

            // Only departure time edited
            if (request.DepartureTime.HasValue && request.ArrivalTime == null)
            {
                if (request.DepartureTime.Value.Date != attendanceDb.ArrivalTime.Date)
                {
                    return false;
                }
                if (attendanceDb.ArrivalTime > request.DepartureTime)
                {
                    return false;
                }
            }
            // Only arrival time edited 
            if (request.ArrivalTime.HasValue && request.DepartureTime == null)
            {
                if (request.ArrivalTime.Value.Date != attendanceDb.ArrivalTime.Date)
                {
                    return false;
                }
                if (attendanceDb.DepartureTime.HasValue &&
                    request.ArrivalTime > attendanceDb.DepartureTime)
                {
                    return false;
                }
            }
            return true;
        }

        public bool CheckInDuringDayOff(DateTime? date)
        {
            var dayOff = appDbContext.DaysOff
                .Where(d => d.StartDate <= date && d.EndDate >= date)
                .Where(d => !d.IsCustomDaysOff)
                .Where(x => x.IsCustomDaysOff && x.UserDaysOff.Any(d => d.UserId == session.CurrentUser.Id))
                .Where(d => d.IsCustomDaysOff && !d.UserDaysOff.Any());

            if (dayOff.Any())
            {
                return false;
            }
            return true;
        }

        internal bool CheckInDuringVacation(DateTime? arg)
        {
            if (appDbContext.Vacation.Where(d => d.UserId == session.CurrentUser.Id).Any(v => (v.StartDate.Date <= arg.Value.Date && v.EndDate.Date >= arg.Value.Date && v.StatusId != (int)VacationsStatus.Rejected)))
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Vacation Requests
        public bool AlreadySigned(int id)
        {
            var alreadySigned = appDbContext.VacationRequestApproval.Any(d => d.Id == id && d.Approved != null);

            if (alreadySigned)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool AlreadySignedFirstPriority(int vacationId)
        {
            var alreadySigned = appDbContext.VacationRequestApproval.Any(d => d.VacationId == vacationId && d.SigningUserId == session.CurrentUser.Id && d.Approved != null);

            if (alreadySigned)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Vacations

        public bool HasVacationDaysleft(DateTime startDate, DateTime endDate, int? id = null)
        {
            var daysLeft = appDbContext.VacationUserManagement.Where(a => a.UserId == session.CurrentUser.Id)
                .Select(d => d.TotalDays - d.TakenDays)
                .Sum();

            if (id.HasValue)
            {
                var vacationToEditDays = appDbContext.Vacation
                    .Where(d => d.Id == id.Value)
                    .Select(d => d.NumberOfDays)
                    .First();

                daysLeft += vacationToEditDays;
            }

            var daysRequested = vacationService.CalculateNumberOfVacationDays(startDate, endDate);

            var validVacation = daysRequested < daysLeft;

            return validVacation;
        }

        public bool CheckAttendance(DateTime startDate)
        {
            var checkedIn = appDbContext.Attendance.Where(d => d.UserId == session.CurrentUser.Id).Count();

            var allDays = vacationService.CalculateNumberOfBusinessDays(appDbContext.User.Where(d => d.Id == session.CurrentUser.Id)
                .Select(d => d.CreatedAt).FirstOrDefault(), DateTime.Now);

            var daysOff = appDbContext.DaysOff.Sum(d => d.NumberOfDays);

            var vacations = appDbContext.Vacation.Where(d => d.UserId == session.CurrentUser.Id && d.StatusId != (int)VacationsStatus.Pending && d.EndDate <= DateTime.Now).Sum(d => d.NumberOfDays);

            var workingDays = allDays - daysOff - vacations;

            if (startDate.Date.Equals(DateTime.Now.Date))
            {
                workingDays--;
            }

            if (checkedIn >= workingDays)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EndDateEarlierThanStartDateForVacation(DateTime? startDate, DateTime? endDate, int id)
        {
            var vacation = appDbContext.Vacation.Where(a => a.Id == id).FirstOrDefault();

            while (vacation != null)
            {
                return EndDateEarlierThanStartDate(startDate, endDate, vacation.StartDate, vacation.EndDate);
            }

            return false;
        }
        #endregion

        #region Days Off
        public bool DayOffIntersectsExistingDayOff(DateTime startDate, DateTime endDate, int id)
        {
            var alreadyExists = appDbContext.DaysOff
                .Where(d => !d.IsCustomDaysOff)
                .Where(d => d.Id != id)
                //start date in existing interval
                .Where(d => (startDate.Date >= d.StartDate.Date && startDate.Date <= d.EndDate.Date) ||
                            //end date in existing interval
                            (endDate.Date >= d.StartDate.Date && endDate.Date <= d.EndDate.Date) ||
                            //requesting vacation incudes an existing interval
                            (startDate.Date <= d.StartDate.Date && endDate.Date >= d.EndDate.Date) ||
                            startDate.Date == d.StartDate.Date || startDate.Date == d.EndDate.Date ||
                            endDate.Date == d.StartDate.Date || endDate.Date == d.EndDate.Date)
                .Any();

            return alreadyExists;
        }

        public void CustomDayOffIntersectsExistingDayOff(DaysOffBaseModel command, CustomContext customContext)
        {
            var sw = new Stopwatch();
            sw.Start();

            if (command.CustomUsersIds.Any() || command.ForEveryone)
            {
                var daysOff = appDbContext.DaysOff
                    .Select(d => new
                    {
                        d.Id,
                        d.StartDate,
                        d.EndDate,
                        d.IsCustomDaysOff,
                        UserDaysOff = d.UserDaysOff.Select(x => new
                        {
                            x.User.Email,
                            x.UserId
                        })
                    })
                    .Where(d => (command.StartDate.Date >= d.StartDate.Date && command.StartDate.Date <= d.EndDate.Date) ||
                            //end date in existing interval
                            (command.EndDate.Date >= d.StartDate.Date && command.EndDate.Date <= d.EndDate.Date) ||
                            //requesting vacation incudes an existing interval
                            (command.StartDate.Date <= d.StartDate.Date && command.EndDate.Date >= d.EndDate.Date) ||
                            command.StartDate.Date == d.StartDate.Date || command.StartDate.Date == d.EndDate.Date ||
                            command.EndDate.Date == d.StartDate.Date || command.EndDate.Date == d.EndDate.Date).ToList()
                     .Where(d => d.GetType() == typeof(EditDaysOffCommand) ? d.Id != ((EditDaysOffCommand)command).Id : d.Id == 0);

                //checking if there is any legal day off during the selected period 
                var standardDaysOff = daysOff.Where(d => !d.IsCustomDaysOff);

                if (standardDaysOff.Any())
                {
                    customContext.AddFailure(ValidationMessages.DayOffAlreadyExists);
                }


                //checking if there is a custom day off assigned to every user
                var customDaysOffAllUsers = daysOff.Where(d => d.IsCustomDaysOff)
                    .Where(d => !d.UserDaysOff.Any())
                    .Where(d => d.GetType() == typeof(EditDaysOffCommand) ? d.Id != ((EditDaysOffCommand)command).Id : d.Id == 0);

                if (customDaysOffAllUsers.Any())
                {
                    customContext.AddFailure(ValidationMessages.CustomDayOffAlreadyExists);
                }


                //when assigned to specific users, it checks if any of them already has a custom day off that intersects the specified period
                var customDaysOffPerUser = daysOff.Where(d => d.UserDaysOff.Any(a => command.CustomUsersIds.Contains(a.UserId))).SelectMany(d => d.UserDaysOff);

                if (customDaysOffPerUser.Any())
                {
                    var message = $"The following users already have a custom day off set during that period:<br> {string.Join(' ', customDaysOffPerUser.Select(d => "<br>" + d.Email))}";

                    customContext.AddFailure(message);
                }

                //assigned to everyone, checks if tere is any user that has a vacation during that period
                if (command.ForEveryone && daysOff.Where(d => d.UserDaysOff.Any()).Any())
                {
                    var message = $"The following users already have a custom day off set during that period:<br> {string.Join(' ', daysOff.Where(d => d.UserDaysOff.Any()).SelectMany(d => d.UserDaysOff).Select(d => "<br>" + d.Email))}";

                    customContext.AddFailure(message);
                }


            }

            sw.Stop();
            Debug.WriteLine($"##### VALIDATION ELAPSED MS: {sw.ElapsedMilliseconds} #####");

        }

        public static bool ValidateDaysOff(DaysOffBaseModel command)
        {
            if (command.ForEveryone && command.CustomUsersIds.Any())
            {
                return false;
            }

            return true;
        }

        public bool EndDateEarlierThanStartDateForDaysOff(DateTime? startDate, DateTime? endDate, int id)
        {
            var dayOff = appDbContext.DaysOff.Where(a => a.Id == id).FirstOrDefault();

            while (dayOff != null)
            {
                return EndDateEarlierThanStartDate(startDate, endDate, dayOff.StartDate, dayOff.EndDate);
            }

            return false;
        }

        public bool DayOffTypeChanged(EditDaysOffCommand command)
        {
            var dayOffDb = appDbContext.DaysOff.First(d => d.Id == command.Id);

            if (dayOffDb.IsCustomDaysOff)
            {
                if (!command.ForEveryone && !command.CustomUsersIds.Any())
                {
                    return true;
                }
            }
            if (!dayOffDb.IsCustomDaysOff && (command.ForEveryone || command.CustomUsersIds.Any()))
            {
                return true;
            }
            return false;

        }
        #endregion

        #region Helpers
        public static bool DateHasPassed(DateTime date)
        {
            if (date >= DateTime.Now.Date)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsWeekend(DateTime date)
        {
            if ((date.DayOfWeek == DayOfWeek.Saturday) || (date.DayOfWeek == DayOfWeek.Sunday))
            {
                return false;
            }
            else
                return true;
        }

        private static bool EndDateEarlierThanStartDate(DateTime? startDate, DateTime? endDate, DateTime startDateDB, DateTime endDateDB)
        {
            if (!string.IsNullOrEmpty(startDate.ToString()) && !string.IsNullOrEmpty(endDate.ToString()))
            {
                if (startDate <= endDate)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (!string.IsNullOrEmpty(startDate.ToString()))
            {
                if (startDate <= endDateDB)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (!string.IsNullOrEmpty(endDate.ToString()))
            {
                if (startDateDB <= endDate)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
