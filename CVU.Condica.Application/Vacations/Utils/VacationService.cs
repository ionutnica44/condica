﻿using CVU.Condica.Application.Interfaces;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CVU.Condica.Application.Vacations.Utils
{
    public class VacationService : IVacationService
    {
        protected AppDbContext appDbContext;

        private readonly IEmailService emailService;
        private readonly IConfiguration configuration;
        private readonly ISession session;

        public VacationService(AppDbContext appDbContext, IEmailService emailService, IConfiguration configuration, ISession session)
        {
            this.emailService = emailService;
            this.appDbContext = appDbContext;
            this.configuration = configuration;
            this.session = session;
        }

        public void SendVacationRequestEmail(int vacationId)
        {
            var vacationRequestApprovals = appDbContext.VacationRequestApproval
                 .Where(d => d.VacationId == vacationId)
                 .Where(d => d.Approved == null)
                 .Select(d => new
                 {
                     SigningUserName = d.SigningUser.FirstName,
                     RequestingUserFullName = d.Vacation.User.FirstName + " " + d.Vacation.User.LastName,
                     StartDate = d.Vacation.StartDate,
                     EndDate = d.Vacation.EndDate,
                     SigningUserEmail = d.SigningUser.Email,
                     Days = d.Vacation.NumberOfDays
                 })
                 .ToList();


            string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\EmailTemplates";

            var hostName = $"{configuration["BaseUrl"]}/admin/approval";

            vacationRequestApprovals.ForEach(signer =>
            {

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\VacationRequest.txt");
#else
            var template = File.ReadAllText("VacationRequest.txt");
#endif

                template = template.Replace("{BaseUrl}", hostName)
                .Replace("{FirstName}", signer.SigningUserName)
                .Replace("{RequesterFullName}", signer.RequestingUserFullName)
                .Replace("{StartDate}", signer.StartDate.ToString("dd MMMM yyyy"))
                .Replace("{EndDate}", signer.EndDate.ToString("dd MMMM yyyy"))
                .Replace("{Days}", signer.Days.ToString());


                emailService.SendAsync(subject: $"{signer.RequestingUserFullName} requested a vacation!",
                    body: template,
                    from: "Do Not Reply",
                    recipients: new List<string> { signer.SigningUserEmail });
            });
        }

        public void SendVacationStatusEmail(int vacationId, bool Approved)
        {
            var status = Approved ? VacationsStatus.Approved.ToString() : VacationsStatus.Rejected.ToString();

            var vacation = appDbContext.Vacation
                .Where(d => d.Id == vacationId)
                .Include(d => d.VacationRequestApproval)
                .Select(d => new
                {
                    d.NumberOfDays,
                    d.StartDate,
                    d.EndDate,
                    d.User.FirstName,
                    d.User.Email,
                    d.StatusId
                })
                .First();

            var vacationRequests = appDbContext.VacationRequestApproval
                .Include(d => d.SigningUser)
                .Where(d => d.VacationId == vacationId && d.Comment != null)
                .Select(d => new
                {
                    d.SigningUserId,
                    d.SigningUser.FirstName,
                    d.SigningUser.LastName,
                    d.Comment
                });

            string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\EmailTemplates";

            var hostName = $"{configuration["BaseUrl"]}/admin/approval";

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\VacationStatus.txt");
#else
            var template = File.ReadAllText("VacationStatus.txt");
#endif

            template = template.Replace("{BaseUrl}", hostName)
            .Replace("{FirstName}", vacation.FirstName)
            .Replace("{StartDate}", vacation.StartDate.ToString("dd MMMM yyyy"))
            .Replace("{EndDate}", vacation.EndDate.ToString("dd MMMM yyyy"))
            .Replace("{Days}", vacation.NumberOfDays.ToString())
            .Replace("{Approved}", status);

            string[] subStrings = template.Split("{mark}");

            if (vacationRequests.Count() != 0)
            {
                subStrings[0] += "<p style=\"font - size: 22px; font - weight: 300; \">Comments: </p> <ul>";
            }

            foreach (var vacationRequest in vacationRequests)
            {
                subStrings[0] += $"<li style=\"font - size: 22px; font - weight: 300; \">{vacationRequest.FirstName} {vacationRequest.LastName}: {vacationRequest.Comment} </li>";
            }

            subStrings[0] += "</ul>";

            template = subStrings[0] + subStrings[1];

            emailService.SendAsync(subject: $"Vacation {status}!",
                body: template,
                from: "Do Not Reply",
                recipients: new List<string> { vacation.Email });
        }

        public void SendVacationStatusUpdated(int vacationId, int signingUser, string signingUserName, string resigningUserName)
        {
            var vacation = appDbContext.Vacation
                .Where(d => d.Id == vacationId)
                .Select(d => new
                {
                    d.NumberOfDays,
                    d.StartDate,
                    d.EndDate,
                    d.User.FirstName,
                    d.User.Email,
                    d.StatusId
                })
                .First();

            string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\EmailTemplates";

            var hostName = $"{configuration["BaseUrl"]}/admin/approval";

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\VacationUpdate.txt");
#else
            var template = File.ReadAllText("VacationUpdate.txt");
#endif

            template = template.Replace("{BaseUrl}", hostName)
            .Replace("{FirstName}", signingUserName)
            .Replace("{UserName}", resigningUserName)
            .Replace("{EmployeeName}", vacation.FirstName)
            .Replace("{StartDate}", vacation.StartDate.ToString("dd MMMM yyyy"))
            .Replace("{EndDate}", vacation.EndDate.ToString("dd MMMM yyyy"));


            emailService.SendAsync(subject: $"Vacation status update",
                body: template,
                from: "Do Not Reply",
                recipients: new List<string> { vacation.Email });
        }

        public int CalculateNumberOfBusinessDays(DateTime StartDate, DateTime EndDate)
        {
            var span = EndDate.Date - StartDate.Date;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;
            // find out if there are weekends during the time exceedng the full weeks
            if (businessDays > fullWeekCount * 7)
            {
                // we are here to find out if there is a 1-day or 2-days weekend
                // in the time interval remaining after subtracting the complete weeks
                //int firstDayOfWeek = (int)StartDate.DayOfWeek;
                //int lastDayOfWeek = (int)EndDate.DayOfWeek;
                int firstDayOfWeek = StartDate.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)StartDate.DayOfWeek;
                int lastDayOfWeek = EndDate.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)EndDate.DayOfWeek;

                if (lastDayOfWeek < firstDayOfWeek)
                    lastDayOfWeek += 7;
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                        businessDays -= 2;
                    else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                        businessDays -= 1;
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                    businessDays -= 1;
            }

            // subtract the weekends during the full weeks in the interval
            businessDays -= fullWeekCount + fullWeekCount;

            return businessDays;
        }

        public int CalculateNumberOfBussinesDaysWithDaysOff(DateTime StartDate, DateTime EndDate)
        {
            var customDaysOff = appDbContext.UserDaysOff.Where(d => d.UserId == session.CurrentUser.Id);

            var daysOff = appDbContext.DaysOff.Select(d => d)
                .Where(d => !d.IsCustomDaysOff || !d.UserDaysOff.Any() || d.UserDaysOff.Any(x => x.UserId == session.CurrentUser.Id));

            var numberOfDaysOff = 0;

            // subtract the number of bank holidays during the time interval
            foreach (var dayOff in daysOff)
            {
                if ((StartDate <= dayOff.EndDate) && (EndDate >= dayOff.StartDate))
                {
                    int span = 1 + Convert.ToInt32((dayOff.EndDate - dayOff.StartDate).TotalDays);
                    int nsaturdays = (span + Convert.ToInt32(dayOff.StartDate.DayOfWeek)) / 7;
                    numberOfDaysOff = span - 2 * nsaturdays
                           - (dayOff.StartDate.DayOfWeek == DayOfWeek.Sunday ? 1 : 0)
                           + (dayOff.EndDate.DayOfWeek == DayOfWeek.Saturday ? 1 : 0);
                }
            }

            return numberOfDaysOff;

        }

        public int CalculateNumberOfVacationDays(DateTime startDate, DateTime endDate)
        {
            var vacationDays = CalculateNumberOfBusinessDays(startDate, endDate) - CalculateNumberOfBussinesDaysWithDaysOff(startDate, endDate);

            return vacationDays;
        }

        public void UpdateUserVacationDays(int UserId, int NumberOfDays, int? VacationId = null, bool delete = false)
        {
            var userDaysPerYears = appDbContext.VacationUserManagement
                    .Where(d => d.UserId == UserId)
                    .OrderByDescending(d => d.Year)
                    .ToList();

            //rollback current vacation days
            //update & delete vacation
            if (VacationId.HasValue)
            {
                var vacation = appDbContext.Vacation.First(d => d.Id == VacationId);

                var currentStack = userDaysPerYears.FirstOrDefault(d => d.TakenDays > 0 && d.TakenDays < d.TotalDays);

                if (currentStack == null)
                {
                    currentStack = userDaysPerYears.Last(d => d.TakenDays == 0);
                }

                if (currentStack.TakenDays - vacation.NumberOfDays < 0)
                {
                    userDaysPerYears[userDaysPerYears.IndexOf(currentStack) + 1].TakenDays -= vacation.NumberOfDays - currentStack.TakenDays;
                    currentStack.TakenDays = 0;
                }
                else
                {
                    currentStack.TakenDays -= vacation.NumberOfDays;
                }
            }

            if (delete)
            {
                return;
            }

            var firstWithRemainingDays = userDaysPerYears.FirstOrDefault(d => d.TakenDays > 0 && d.TakenDays < d.TotalDays);

            if (firstWithRemainingDays == null)
            {
                firstWithRemainingDays = userDaysPerYears.Last(d => d.TakenDays == 0);
            }

            if (firstWithRemainingDays.TakenDays + NumberOfDays > firstWithRemainingDays.TotalDays)
            {
                userDaysPerYears[userDaysPerYears.IndexOf(firstWithRemainingDays) - 1].TakenDays += NumberOfDays - (firstWithRemainingDays.TotalDays - firstWithRemainingDays.TakenDays);
                firstWithRemainingDays.TakenDays = firstWithRemainingDays.TotalDays;
            }
            else
            {
                firstWithRemainingDays.TakenDays += NumberOfDays;
            }
        }

        public void UpdateSigningFlow(IEnumerable<int> signingUserIdsToRemove, int? userId = null)
        {
            var vacationApprovalsToDelete = appDbContext.VacationRequestApproval
                .Include(d => d.Vacation)
                .ThenInclude(d => d.VacationRequestApproval)
                .Where(d => signingUserIdsToRemove.Contains(d.SigningUserId) && d.Approved == null);

            var signingUsersDeleted = appDbContext.SigningUser
                .Where(d => signingUserIdsToRemove.Contains(d.SigningUserId));
            //If userId has value, we are just removing one or more a signing users from a specific user.
            //If userid is null, we are revoking one or more signing users the claim of approving vacations.
            if (userId.HasValue)
            {
                vacationApprovalsToDelete = vacationApprovalsToDelete
                    .Where(d => d.Vacation.UserId == userId.Value);

                signingUsersDeleted = signingUsersDeleted
                    .Where(d => d.UserId == userId.Value);
            }

            if (!vacationApprovalsToDelete.Any() || !signingUsersDeleted.Any())
            {
                return;
            }

            var vacationsIds = vacationApprovalsToDelete
                .Select(d => d.VacationId)
                .ToList();

            var vacations = appDbContext.Vacation
                .Include(d => d.VacationRequestApproval)
                .Where(d => vacationsIds.Contains(d.Id));

            appDbContext.VacationRequestApproval.RemoveRange(vacationApprovalsToDelete);
            appDbContext.SigningUser.RemoveRange(signingUsersDeleted);
            appDbContext.SaveChanges();

            foreach (var vacation in vacations)
            {
                var signingUsers = appDbContext.SigningUser
                        .Include(d => d.Signing)
                        .Where(d => d.UserId == vacation.UserId && !signingUserIdsToRemove.Contains(d.SigningUserId))
                        .ToList()
                        .Select(d => (d.SigningUserId, d.Signing.SignPriorityId.Value))
                        .AsEnumerable();

                var oldVacationRequests = vacation.VacationRequestApproval.Count;

                if (vacation.VacationRequestApproval.Any())
                {
                    if (vacation.VacationRequestApproval.All(d => d.Approved == true))
                    {
                        vacation.VacationRequestApproval.AddNextSigningUsers(signingUsers);
                    }
                }
                else
                {
                    vacation.VacationRequestApproval.AddNextSigningUsers(signingUsers);
                }


                appDbContext.SaveChanges();

                if (oldVacationRequests != vacation.VacationRequestApproval.Count)
                {
                    SendVacationRequestEmail(vacation.Id);
                }
            }
        }
        public byte[] GenerateVacationReport(DateTime requestedDateTime)
        {
            var vacations = appDbContext.Vacation
                .Include(d => d.User)
                .Where(d => (d.StartDate.Month == requestedDateTime.Month || d.EndDate.Month == requestedDateTime.Month) & (d.StartDate.Year == requestedDateTime.Year || d.EndDate.Year == requestedDateTime.Year));


            byte[] dataByteArray;
            using (var package = new ExcelPackage())
            {
                var ws = package.Workbook.Worksheets.Add("Vacations");
                ws.Cells["A1:D4"].Merge = true;
                ws.Cells.Style.Font.Name = "Arial Narrow";
                ws.Cells.Style.Font.Size = 10;
                ws.Column(1).Width = 25;
                ws.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;


                //Title cell
                var mergedCellsTitle = ws.Cells[ws.MergedCells[1, 1]];
                mergedCellsTitle.Value = $"CVU Intelligence - Concedii - {requestedDateTime:MMMM yyyy}";
                mergedCellsTitle.Style.Font.Color.SetColor(Color.DarkGreen);
                mergedCellsTitle.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
                mergedCellsTitle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                mergedCellsTitle.Style.Font.Bold = true;
                mergedCellsTitle.Style.Font.Size = 14;
                mergedCellsTitle.Style.Font.Name = "Arial Narrow";



                //Header rows

                ws.Row(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                ws.Cells[5, 1].Value = "Nume";
                ws.Cells[5, 2].Value = "Prima zi";
                ws.Cells[5, 3].Value = "Ultima zi";
                ws.Cells[5, 4].Value = "Numar zile";

                //Data rows
                CultureInfo culture = new CultureInfo("ron-ro");
                var startingRow = 6;
                foreach (var vacation in vacations)
                {
                    //Ajusteaza numarul de zile daca vacanta se intinde pe mai multe luni
                    if (vacation.StartDate.Month != requestedDateTime.Month || vacation.EndDate.Month != requestedDateTime.Month)
                    {
                        if (vacation.StartDate.Month < requestedDateTime.Month)
                        {
                            vacation.StartDate = new DateTime(requestedDateTime.Year, requestedDateTime.Month, 1);
                        }
                        else if (vacation.EndDate.Month > requestedDateTime.Month)
                        {
                            vacation.EndDate = new DateTime(requestedDateTime.Year, requestedDateTime.Month, DateTime.DaysInMonth(requestedDateTime.Year, requestedDateTime.Month));
                        }
                        vacation.NumberOfDays = CalculateNumberOfBusinessDays(vacation.StartDate, vacation.EndDate);

                    }

                    ws.Cells[startingRow, 1].Value = $"{vacation.User.FirstName} {vacation.User.LastName}";
                    ws.Cells[startingRow, 2].Value = vacation.StartDate.ToString("d MMM yyyy", culture);
                    ws.Cells[startingRow, 3].Value = vacation.EndDate.ToString("d MMM yyyy", culture);
                    ws.Cells[startingRow, 4].Value = vacation.NumberOfDays;
                    startingRow++;
                }

                package.Save();
                dataByteArray = package.GetAsByteArray();
            }
            return dataByteArray;
        }
        public bool UpdateVacationDays()
        {
            var users = appDbContext.User
                .Include(d => d.VacationUserManagement)
                .Where(d => !d.VacationUserManagement.Any(d => d.Year.ToLower() == DateTime.Now.Year.ToString().ToLower()));

            foreach (var user in users)
            {
                if (user.ContractDate.Year == DateTime.Now.Year)
                {
                    var totalDays = Convert.ToInt32(((double)(DateTime.IsLeapYear(DateTime.Now.Year) ? 366 : 365) - user.ContractDate.DayOfYear) / (double)(DateTime.IsLeapYear(DateTime.Now.Year) ? 366 : 365) * 21);
                    user.VacationUserManagement.Add(new VacationUserManagement { TotalDays = totalDays, Year = DateTime.Now.Year.ToString() });
                    continue;
                }

                user.VacationUserManagement.Add(new VacationUserManagement { TotalDays = 21, Year = DateTime.Now.Year.ToString() });
            }
            appDbContext.SaveChanges();
            return true;
        }
    }

    public static class Extensions
    {
        public static ICollection<VacationRequestApproval> AddNextSigningUsers(this ICollection<VacationRequestApproval> source, IEnumerable<(int SigningUserId, int SignPriorityId)> signingUserIds)
        {
            var vacationId = source.Any() ? source.Select(d => d.VacationId).First() : 0;

            //Request approvals in database
            var vacationRequestApprovals = source
                .Select(d => new
                {
                    SigningUserId = d.SigningUserId,
                    SigningPriorityId = d.SigningUser.SignPriorityId,
                    Approved = d.Approved
                })
                .ToList();

            if (!vacationRequestApprovals.Any() || vacationRequestApprovals.All(d => d.Approved.HasValue && d.Approved.Value))
            {
                var alreadySignedPriorities = vacationRequestApprovals.Select(d => d.SigningPriorityId).Distinct();

                var remainingUsersToSign = signingUserIds
                    .Where(d => !alreadySignedPriorities.Contains(d.SignPriorityId));

                var maxPriorityLevel = remainingUsersToSign.Any() ? remainingUsersToSign.Max(d => d.SignPriorityId) : 0;

                var currentUsersToSign = remainingUsersToSign.Where(d => d.SignPriorityId == maxPriorityLevel).Select(d => d.SigningUserId);

                var newSigningUsers = currentUsersToSign.Select(d => new VacationRequestApproval { SigningUserId = d, VacationId = vacationId }).ToList();

                newSigningUsers.ForEach(d => source.Add(d));
            }

            return source;
        }
    }
}
