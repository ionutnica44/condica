import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'not-found',
    templateUrl: 'not-found.component.html',
})

export class NotFoundComponent {
    constructor(private _router: Router, private location: Location) {
    }

    redirect() {
        this.location.back();
        // this._router.navigate(['/welcome']);
    }
}