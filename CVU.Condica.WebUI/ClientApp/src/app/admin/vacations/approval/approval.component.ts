import { Component, OnInit, OnDestroy } from '@angular/core';
import { RequestOptions } from 'services/requestOptions';
import { AuthService } from 'services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { VacationsClient, SwaggerResponse, VacationRequestApprovalDto, VacationDto, VacationRequestApprovalQuery, EditVacationRequestApprovalCommand, Unit, VacationRequestApprovalFirstPriorityQuery, UserDto, VacationRequestApprovalAlreadySignedQuery, EditVacationRequestApprovalAlreadySignedCommand } from 'app/client-api';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html'
})
export class ApprovalComponent implements OnInit, OnDestroy {
  vacationsApp: VacationRequestApprovalDto[] = [];
  firstPriorityVacationsList: VacationRequestApprovalDto[] = [];
  loading: boolean = true;
  disabled: boolean = false;
  currentUser: UserDto;

  approvalModel = new EditVacationRequestApprovalCommand();
  reverseModel = new EditVacationRequestApprovalAlreadySignedCommand();

  page: RequestOptions<VacationRequestApprovalQuery> = new RequestOptions<VacationRequestApprovalQuery>();
  pageFirstPrioriry: RequestOptions<VacationRequestApprovalFirstPriorityQuery> = new RequestOptions<VacationRequestApprovalFirstPriorityQuery>();
  pageAlreadySigned: RequestOptions<VacationRequestApprovalAlreadySignedQuery> = new RequestOptions<VacationRequestApprovalAlreadySignedQuery>();

  alreadyApprovedVacations: VacationRequestApprovalDto[] = [];

  undoModal : any;
  confirmResponseModal: any;
  
  selectedToRevert: any;

  approved:boolean;
  vetoRight: boolean;
  constructor(private _vacationClient: VacationsClient, private _authService: AuthService, private _toastr: ToastrService) {
    this.currentUser = _authService.getCurrentUser();
  }

  refreshDaysList(event) {
    if (event === "refresh") {
      this.getVacationsListApproval();
      this.getFirstPriorityVacationsList();
    }
  }

  refreshModels(){
    if(this.undoModal){
      this.undoModal.modal('hide');
      this.undoModal.remove();
    }
    if(this.confirmResponseModal){
      this.confirmResponseModal.modal('hide');
      this.confirmResponseModal.remove();
    } 
 
    this.approvalModel = new EditVacationRequestApprovalCommand();
    this.reverseModel = new EditVacationRequestApprovalAlreadySignedCommand();
  }

  ngOnInit() {
    //TODO: on hide not working - investigate
    this.undoModal = ($('.ui.modal.undoModal') as any).modal(
    //   {
    //   onHide: this.refreshModels()
    // }
    );
    this.confirmResponseModal = ($('.ui.modal.confirmResponseModal') as any).modal(
    //   {
    //   onHide: this.refreshModels()
    // }
    );
    this.getVacationsListApproval();
    this.getFirstPriorityVacationsList();
    this.getVacantionListAlreaySigned();
  }

  getVacantionListAlreaySigned() {
    this._vacationClient.listVacationRequestApprovalAlreadySigned(this.pageAlreadySigned.filter).subscribe((response: SwaggerResponse<VacationRequestApprovalDto[]>) => {
      this.pageAlreadySigned.setPaginationInfo(response);
      this.alreadyApprovedVacations = response.result;
      this.loading = false;
    })
  }



  getVacationsListApproval = () => {
    this._vacationClient.listVacationRequestApproval(this.page.filter).subscribe((response: SwaggerResponse<VacationRequestApprovalDto[]>) => {
      this.page.setPaginationInfo(response);
      this.vacationsApp = response.result;
      this.loading = false;
    });
  }

  getFirstPriorityVacationsList() {
    this._vacationClient.listVacationRequestApprovalFirstPriority(this.pageFirstPrioriry.filter).subscribe((response: SwaggerResponse<VacationRequestApprovalDto[]>) => {
      this.pageFirstPrioriry.setPaginationInfo(response);
      this.firstPriorityVacationsList = response.result;
      this.loading = false;
    });
  }


  refreshList(vetoRight) {
    if (vetoRight) {
      this.getFirstPriorityVacationsList();
    }
    else
      this.getVacationsListApproval();
  }
  approve() {
    // this.approvalModel.id = vetoRight ? vac.vacationId : vac.id;
    // this.approvalModel.approved = approved;
    const method = this.vetoRight ? this._vacationClient['editVacationRequestApprovalFirstPriority'].bind(this._vacationClient) : this._vacationClient['editVacationRequestApproval'].bind(this._vacationClient);
    method(this.approvalModel.id, this.approvalModel).subscribe((response: SwaggerResponse<Unit>) => {
      this.refreshList(this.vetoRight);
      this.confirmResponseModal.modal('hide');
      if (this.approvalModel.approved == true) {
        this._toastr.success('Vacation approved');
      } else if (this.approvalModel.approved == false) {
        this._toastr.success('Vacation denied');
      }
      this.getVacationsListApproval();
      this.getFirstPriorityVacationsList();
      this.getVacantionListAlreaySigned();
      this.approvalModel = new EditVacationRequestApprovalCommand();
    }, (e) => {
      this._toastr.success('Error on approving vacation');
      this.confirmResponseModal.modal('hide');
      this.approvalModel = new EditVacationRequestApprovalCommand();

    })
  }

  openUndoModal(alreadySignedV){
    this.reverseModel = alreadySignedV;
    this.undoModal.modal('show');
  }


  openConfirmModal(vac, approved, vetoRight){
    this.vetoRight = vetoRight;
    this.approvalModel.id = vetoRight ? vac.vacationId : vac.id;
    this.approvalModel.approved = approved;
    this.confirmResponseModal.modal('show');
  }

  reverseVacation() {
  //  this.reverseModel.id = vacationSigned.id;
   // this.reverseModel.comment = vacationSigned.comment;
    this._vacationClient.editVacationRequestApprovalAlreadySigned(this.reverseModel.id, this.reverseModel).subscribe((response: SwaggerResponse<Unit>) => {
      this._toastr.success("Reverted vacation status successfully");
      this.getVacationsListApproval();
      this.getFirstPriorityVacationsList();
      this.getVacantionListAlreaySigned();
      this.undoModal.modal('hide');
      this.reverseModel = new EditVacationRequestApprovalAlreadySignedCommand();
    }, (e) => {
      this._toastr.error("Could not revert vacation status");
      this.undoModal.modal('hide');
      this.reverseModel = new EditVacationRequestApprovalAlreadySignedCommand();

    })
  }

  ngOnDestroy() {
    if(this.undoModal){
      this.undoModal.modal('hide');
      this.undoModal.remove();
    }
    if(this.confirmResponseModal){
      this.confirmResponseModal.modal('hide');
      this.confirmResponseModal.remove();
    } 
  }

}

