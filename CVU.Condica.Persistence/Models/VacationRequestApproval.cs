﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CVU.Condica.Persistence.Models
{
    public partial class VacationRequestApproval
    {
        public int Id { get; set; }
        public int VacationId { get; set; }
        public bool? Approved { get; set; }
        public int SigningUserId { get; set; }
        public string Comment { get; set; }

        public virtual User SigningUser { get; set; }
        public virtual Vacation Vacation { get; set; }
    }

    public class VacationRequestApprovalConfiguration : IEntityTypeConfiguration<VacationRequestApproval>
    {
        public void Configure(EntityTypeBuilder<VacationRequestApproval> builder)
        {
            builder
                .HasOne(d => d.SigningUser)
                .WithMany(d => d.VacationRequestApproval)
                .HasForeignKey(d => d.SigningUserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(d => d.Vacation)
                .WithMany(d => d.VacationRequestApproval)
                .HasForeignKey(d => d.VacationId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
