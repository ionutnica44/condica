﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Reference
{
    public class CompaniesQuery : IRequest<Dictionary<int, string>>
    {
    }

    public class CompaniesQueryHandler : RequestHandler<CompaniesQuery, Dictionary<int, string>>
    {
        public CompaniesQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<Dictionary<int, string>> Handle(CompaniesQuery request, CancellationToken cancellationToken)
        {
            var companies = appDbContext.Company
                .ToDictionary(d => d.Id, d => d.Name);

            return companies;
        }
    }
}
