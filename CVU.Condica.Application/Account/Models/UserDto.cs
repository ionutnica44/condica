using CVU.Condica.Application.Items.Models;
using CVU.Condica.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Account.Models
{
    public class UserDto
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }
        public int? CompanyId { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsBlocked { get; set; }
        public int PositionId { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string Role { get; set; }
        public bool CanSign { get; set; }
        public int? SignPriorityId { get; set; }
        public bool IsActivated { get; set; }
        public Dictionary<int, string> SigningUsers { get; set; }
        public List<ItemShortDto> Items { get; set; }

        public static Expression<Func<User, UserDto>> Projection
        {
            get
            {
                return p => new UserDto()
                {
                    UserId = p.Id,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    RoleId = p.RoleId,
                    Email = p.Email,
                    Company = p.Company.Name,
                    Position = p.Position.Name,
                    Role = p.Role.Name,
                    CompanyId = p.CompanyId,
                    PhoneNumber = p.PhoneNumber,
                    IsBlocked = p.IsBlocked,
                    PositionId = p.PositionId,
                    CanSign = (p.SignPriorityId != null) ? true : false,
                    SigningUsers = p.SigningUsers
                                    .Select(d => new { d.SigningUserId, d.Signing.FirstName, d.Signing.LastName })
                                    .ToDictionary(t => t.SigningUserId, t => (t.FirstName + " " + t.LastName)),
                    Items = p.Items.Select(d => new ItemShortDto()
                    {
                        Name = d.Name,
                        ItemType = d.ItemType.Name
                    }).ToList(),
                    SignPriorityId = p.SignPriorityId,
                    IsActivated = p.IsActivated
                };
            }
        }

        public static UserDto Create(User User)
        {
            return Projection.Compile().Invoke(User);
        }
    }
}
