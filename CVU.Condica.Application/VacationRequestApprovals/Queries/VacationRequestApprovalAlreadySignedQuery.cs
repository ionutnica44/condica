﻿using CVU.Condica.Application.VacationRequestApprovals.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.VacationRequestApprovals.Queries
{
    public class VacationRequestApprovalAlreadySignedQuery : PaginatedQueryParameter, IRequest<PaginatedModel<VacationRequestApprovalDto>>
    {
    }
    public class VacationRequestApprovalAlreadySignedQueryHandler : RequestHandler<VacationRequestApprovalAlreadySignedQuery, PaginatedModel<VacationRequestApprovalDto>>
    {
        private readonly IPaginationService paginationService;

        public VacationRequestApprovalAlreadySignedQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<PaginatedModel<VacationRequestApprovalDto>> Handle(VacationRequestApprovalAlreadySignedQuery request, CancellationToken cancellationToken)
        {
            var vacationRequestApprovals = appDbContext.VacationRequestApproval
                .Include(d => d.Vacation)
                .ThenInclude(d => d.User)
                .Where(d => d.Approved != null)
                .Where(d => d.SigningUserId == CurrentUser.Id);
            // .OrderBy(d => d.Approved);

            var paginatedResponse = paginationService.PaginatedResults<Persistence.Models.VacationRequestApproval, VacationRequestApprovalDto>(vacationRequestApprovals, request);

            return new PaginatedModel<VacationRequestApprovalDto>(paginatedResponse);
        }
    }

    public class VacationRequestApprovalAlreadySignedQueryAuthorization : AuthorizationSetup<VacationRequestApprovalAlreadySignedQuery>
    {
        public VacationRequestApprovalAlreadySignedQueryAuthorization()
        {
        }
    }
}
