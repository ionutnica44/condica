﻿using CVU.Condica.Application.Vacations.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.DaysOff.NewFolder
{
    public class ListDaysOffQuery : PaginatedQueryParameter, IRequest<PaginatedModel<DaysOffDto>>
    {
    }
    public class ListDaysOffQueryHandler : RequestHandler<ListDaysOffQuery, PaginatedModel<DaysOffDto>>
    {
        private readonly IPaginationService paginationService;

        public ListDaysOffQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<PaginatedModel<DaysOffDto>> Handle(ListDaysOffQuery request, CancellationToken cancellationToken)
        {
            var daysOff = appDbContext.DaysOff
                .Include(d => d.UserDaysOff)
                .ThenInclude(d => d.User)
                .AsQueryable();

            var paginatedResponse = paginationService.PaginatedResults<Persistence.Models.DaysOff, DaysOffDto>(daysOff, request);

            return new PaginatedModel<DaysOffDto>(paginatedResponse);

        }
    }

    public class ListDaysOffQueryAuthorization : AuthorizationSetup<ListDaysOffQuery>
    {
        public ListDaysOffQueryAuthorization()
        {
        }
    }
}
