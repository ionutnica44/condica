﻿using CVU.Condica.Application.VacationRequestApprovals.Models;
using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using CVU.Condica.Persistence.ReferenceData;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.VacationRequestApprovals.Queries
{
    public class VacationRequestApprovalQuery : PaginatedQueryParameter, IRequest<PaginatedModel<VacationRequestApprovalDto>>
    {
    }
    public class VacationRequestApprovalQueryHandler : RequestHandler<VacationRequestApprovalQuery, PaginatedModel<VacationRequestApprovalDto>>
    {
        private readonly IPaginationService paginationService;

        private readonly IVacationService vacationService;

        public VacationRequestApprovalQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService, IVacationService vacationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
            this.vacationService = vacationService;
        }

        public async override Task<PaginatedModel<VacationRequestApprovalDto>> Handle(VacationRequestApprovalQuery request, CancellationToken cancellationToken)
        {
            var vacationRequestApprovals = appDbContext.VacationRequestApproval
                .Include(d => d.Vacation)
                .ThenInclude(d => d.User)
                .Where(d => d.Vacation.StatusId == (int)VacationsStatus.Pending)
                .Where(d => d.SigningUserId == CurrentUser.Id && d.Approved == null)
                .OrderBy(d => d.Approved);

            var paginatedResponse = paginationService.PaginatedResults<Persistence.Models.VacationRequestApproval, VacationRequestApprovalDto>(vacationRequestApprovals, request);

            return new PaginatedModel<VacationRequestApprovalDto>(paginatedResponse);
        }
    }

    public class VacationRequestApprovalQueryAuthorization : AuthorizationSetup<VacationRequestApprovalQuery>
    {
        public VacationRequestApprovalQueryAuthorization()
        {
        }
    }
}
