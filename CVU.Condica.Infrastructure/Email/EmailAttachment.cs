using MimeKit;
using System.IO;

namespace CVU.Utilities.Email
{
    public sealed class EmailAttachment
    {
        public Stream EmailContent { get; set; }

        public string FileName { get; set; }

        public ContentType ContentType { get; set; }
    }
}
