using System;
using System.Linq;
using System.Linq.Expressions;

namespace CVU.Condica.Infrastructure.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<TEntity> GetFromCompany<TEntity>(this IQueryable<TEntity> source, int CompanyId, int? EntityId = null) where TEntity : class
        {
            var type = typeof(TEntity);

            var parameterExp = Expression.Parameter(type, type.Name);
            var propertyExp = Expression.Property(parameterExp, "CompanyId");
            var constExp = Expression.Constant(CompanyId, typeof(int));
            var equalExp = Expression.Equal(propertyExp, constExp);
            var lambda = Expression.Lambda<Func<TEntity, bool>>(equalExp, parameterExp);

            source = source.Where(lambda).AsQueryable();

            if (EntityId.HasValue)
            {
                var parameterExp2 = Expression.Parameter(type, "type");
                var propertyExp2 = Expression.Property(parameterExp2, "Id");
                var constExp2 = Expression.Constant(EntityId.Value, typeof(int));
                var equalExp2 = Expression.Equal(propertyExp2, constExp2);
                var lambda2 = Expression.Lambda<Func<TEntity, bool>>(equalExp2, parameterExp2);

                source = source.Where(lambda2).AsQueryable();
            }

            return source;
        }

        public static bool ExistsInCompany<TEntity>(this IQueryable<TEntity> source, int CompanyId, int EntityId, Expression<Func<TEntity, bool>> expression = null) where TEntity : class
        {
            if (expression != null)
            {
                return source.GetFromCompany(CompanyId, EntityId).Any(expression);
            }
            else
            {
                return source.GetFromCompany(CompanyId, EntityId).Any();
            }
        }

        public static bool IsNullOrExistsInCompany<TEntity>(this IQueryable<TEntity> source, int CompanyId, int? EntityId, Expression<Func<TEntity, bool>> expression = null) where TEntity : class
        {
            if (EntityId == null)
            {
                return true;
            }
            if (expression != null)
            {
                return source.GetFromCompany(CompanyId, EntityId).Any(expression);
            }
            else
            {
                return source.GetFromCompany(CompanyId, EntityId).Any();
            }
        }


    }
}
