using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CVU.Condica.Common.Authorization
{
    public abstract class AuthorizationSetup<TRequest>
    {
        internal List<int> Roles = new List<int>();
        internal bool NeedsInternalIp = false;

        protected internal virtual bool CustomAuthorizationConditions(TRequest request, int roleId)
        {
            return true;
        }

        protected void NeedsRole(int roleId)
        {
            Roles.Add(roleId);
        }
        protected void NeedsIp(bool internalIp)
        {
            NeedsInternalIp = internalIp;
        }
    }

    public class AuthorizationService<TRequest>
    {
        public AuthorizationService(TRequest request)
        {
            List<Type> dependentClasses = FindAllDerivedTypes<AuthorizationSetup<TRequest>>(typeof(TRequest).Assembly);

            Roles = new List<int>();

            if (dependentClasses.Count > 1)
            {
                throw new Exception("Invalid implementation of authorization service. You must inherit this class only once per type of TRequest");
            }
            else if (dependentClasses.Count == 1)
            {
                AuthorizationSetup<TRequest> obj;

                obj = Activator.CreateInstance(dependentClasses.First()) as AuthorizationSetup<TRequest>;

                Roles = obj.Roles;

                NeedsInternalIp = obj.NeedsInternalIp;
                AllowAnnonymous = false;

                ValidateCustomAuthorizationConditions += obj.CustomAuthorizationConditions;
            }
        }

        public IEnumerable<int> Roles { get; private set; }
        public IEnumerable<string> Ips { get; private set; }

        public Func<TRequest, int, bool> ValidateCustomAuthorizationConditions { get; private set; }

        public bool AllowAnnonymous = true;

        public bool NeedsInternalIp = false;

        private List<Type> FindAllDerivedTypes<T>(Assembly assembly)
        {
            var derivedType = typeof(T);

            var types = assembly.GetTypes()
                .Where(d => d != derivedType && derivedType.IsAssignableFrom(d))
                .ToList();

            return types;

        }

    }
}
