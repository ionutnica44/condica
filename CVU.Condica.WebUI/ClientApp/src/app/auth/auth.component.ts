// import { AuthService } from 'services/auth/auth.service';
import { Component, OnDestroy, AfterViewInit } from '@angular/core';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
})
export class AuthComponent implements AfterViewInit, OnDestroy {

    constructor() { }

    ngAfterViewInit() {
        // this.translate.setDefaultLang('en');
    }

    ngOnDestroy() {
        ($('body') as any).removeClass('login');
    }

}
