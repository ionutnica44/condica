﻿namespace CVU.Condica.Application.Vacations.Models
{
    public class VacationUserManagementDto
    {
        public string Year { get; set; }
        public int TotalDays { get; set; }
        public int TakenDays { get; set; }
        public int RemainingDays { get; set; }
    }
}
