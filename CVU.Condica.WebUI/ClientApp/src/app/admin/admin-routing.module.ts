import { VacationsComponent } from './vacations/vacations.component';
import { AttendancesComponent } from './attendances/attendances.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserComponent } from './user/user.component';
import { EditComponent } from './user/edit/edit.component';
import { SettingsComponent } from './user/settings/settings.component';
import { DaysOffComponent } from './days-off/days-off.component';
import { ApprovalComponent } from './vacations/approval/approval.component';
import { AuthGuard } from 'services/auth/guards/auth.guard';
import { PermisionsGuard } from 'services/auth/guards/permisions.guard';
import { AllAttendancesComponent } from './attendances/all-attendances/all-attendances.component';
import { InventoryComponent } from './inventory/inventory.component';


const routes: Routes = [
  {
    path: "admin", component: AdminComponent,
    children: [
      { path: "", redirectTo: "attendances", pathMatch: "full" },
      { path: "attendances", component: AttendancesComponent, canActivate: [AuthGuard] },
      { path: "all-attendances", component: AllAttendancesComponent, canActivate: [AuthGuard] },
      { path: "home", component: AttendancesComponent, canActivate: [AuthGuard] },
      { path: "cvu-users", component: UserComponent, canActivate: [AuthGuard] },
      { path: "edit-user", component: EditComponent, canActivate: [AuthGuard] },
      { path: "my-profile", component: SettingsComponent, canActivate: [AuthGuard] },
      { path: "days-off", component: DaysOffComponent, canActivate: [AuthGuard] }, 
      { path: "vacations", component: VacationsComponent, canActivate: [AuthGuard] },
      { path: "approval", component: ApprovalComponent, canActivate: [AuthGuard] },
      { path: "inventory", component: InventoryComponent, canActivate: [AuthGuard] },
      { path: "**", component: NotFoundComponent, canActivate: [AuthGuard] },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers:[AuthGuard],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
