import { AuthService } from '../../../../services/auth/auth.service';
import { Component, AfterViewInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from 'app/client-api';

@Component({
  selector: 'app-nav-top-menu',
  templateUrl: './nav-topMenu.component.html',
})

@Injectable()
export class NavTopMenuComponent  {
  user: UserDto = null;
  private sidebar: any;
  constructor(private router: Router, private _authService: AuthService) {
    this.user = this._authService.getCurrentUser();
 }

  toggleSidebar() {
    let pusher = '.pusher';
    let sidebar = '.ui.sidebar';
    let logo = ".logo";
    ($(sidebar) as any).sidebar('toggle');
    if ($(sidebar).hasClass('visible') || $(sidebar).hasClass('mobile-status')) {
      $(pusher).addClass('tweaked');
    } else {
      $(pusher).removeClass('tweaked');
    }
    //toggle logo
    ($(logo) as any).toggle('toggle');
  }


  logOut() {
    if (this._authService.logOut()) {
      if (localStorage.getItem('UserFirebaseToken')) {
        this._authService.removeUserFromNotification();
      }
      this.router.navigateByUrl('/login');
    }
  }
  changePassword() {
    this.router.navigateByUrl('/auth/change-password-auth');
  }

}

