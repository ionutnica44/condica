﻿using CVU.Condica.Application.DaysOff.Models;
using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.DaysOff.Commands
{
    public class CreateDaysOffCommand : DaysOffBaseModel, IRequest<Unit>
    {
        public static Persistence.Models.DaysOff Create(CreateDaysOffCommand model)
        {
            return new Persistence.Models.DaysOff
            {
                Name = model.Name,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                IsCustomDaysOff = false
            };
        }
    }

    public class CreateDaysOffCommandHandler : RequestHandler<CreateDaysOffCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public CreateDaysOffCommandHandler(
            IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(CreateDaysOffCommand request, CancellationToken cancellationToken)
        {
            var daysOff = CreateDaysOffCommand.Create(request);

            daysOff.NumberOfDays = vacationService.CalculateNumberOfBusinessDays(daysOff.StartDate, daysOff.EndDate);

            if ((request.CustomUsersIds != null && request.CustomUsersIds.Count() > 0) || request.ForEveryone)
            {
                daysOff.IsCustomDaysOff = true;
            }

            appDbContext.DaysOff.Add(daysOff);
            appDbContext.SaveChanges();

            if (request.CustomUsersIds != null)
            {
                var userDaysOff = request.CustomUsersIds
                    .Select(d => new UserDaysOff
                    {
                        DayOffId = daysOff.Id,
                        UserId = d
                    });

                appDbContext.UserDaysOff.AddRange(userDaysOff);
            }

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class CreateDaysOffCommandValidator : AbstractValidator<CreateDaysOffCommand>
    {
        public CreateDaysOffCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateCreateDaysOffCommand);

            //RuleFor(x => x.Name).NotNull();

            //RuleFor(x => x).Must(x => !validationService.DayOffIntersectsExistingDayOff(x.StartDate.ToLocalTime(), x.EndDate.ToLocalTime(), 0)).When(d => !d.CustomUsersIds.Any() || !d.ForEveryone)
            //    .WithMessage(ValidationMessages.DayOffAlreadyExists);


            //RuleFor(x => x).Custom((d, customContext) => validationService.CustomDayOffIntersectsExistingDayOff(d, customContext));//.When(d => d.ForEveryone || d.CustomUsersIds.Any());

            //RuleFor(x => x.StartDate.ToLocalTime()).Must(validationService.DateHasPassed)
            //    .WithMessage(ValidationMessages.DateHasPassed)
            //    .Must(validationService.IsWeekend).WithMessage(ValidationMessages.IsWeekend)
            //    .NotNull();

            //RuleFor(x => x.EndDate).Must(validationService.DateHasPassed).WithMessage(ValidationMessages.DateHasPassed)
            //    .Must(validationService.IsWeekend).WithMessage(ValidationMessages.IsWeekend)
            //    .NotNull();

            //RuleFor(x => x.EndDate).GreaterThanOrEqualTo(x => x.StartDate).WithMessage(ValidationMessages.EndDateAfterStartDate);

            //RuleFor(x => x).Must(validationService.ValidateDaysOff).WithMessage(ValidationMessages.DaysOffMultipleUsers);
        }
    }

    public class CreateDaysOffCommandAuthorization : AuthorizationSetup<CreateDaysOffCommand>
    {
        public CreateDaysOffCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
