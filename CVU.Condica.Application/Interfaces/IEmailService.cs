using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Interfaces
{
    public interface IEmailService
    {
        void From(string emailAddress);
        void AddRecipient(string emailAddress);
        void AddRecipients(IEnumerable<string> emailAddresses);

        void AddCCRecipient(string emailAddress);
        void AddCCRecipients(IEnumerable<string> emailAddresses);

        void AddSubject(string subject);
        void AddBody(string html);
        void AddAttachment(Stream content, string fileName, string mediaType, string mediaSubType);

        void Send();

        Task SendAsync(string subject, string body, string from, IEnumerable<string> recipients, IEnumerable<string> ccRecipients = null);
    }
}
