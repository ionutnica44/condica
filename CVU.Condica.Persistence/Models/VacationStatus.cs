﻿using CVU.Condica.Persistence.Infrastructure;
using CVU.Condica.Persistence.ReferenceData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public partial class VacationStatus
    {
        public VacationStatus()
        {
            Vacations = new HashSet<Vacation>();
        }

        public int Id { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Vacation> Vacations { get; set; }
    }

    public class VacationStatusConfiguration : IEntityTypeConfiguration<VacationStatus>
    {
        public void Configure(EntityTypeBuilder<VacationStatus> builder)
        {
            builder.HasReferenceData<VacationsStatus, VacationStatus>(d => d.Id, d => d.Status);
        }
    }
}
