import { ToastrService } from 'ngx-toastr';
import { Unit, EditVacationCommand } from 'app/client-api';
import { CreateVacationCommand, VacationsClient, SwaggerResponse } from '../../../client-api';
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { disableWeekends } from 'app/shared/utilities/utilities';

@Component({
  selector: 'app-vacation-edit',
  templateUrl: './vacation-edit.component.html',
})
export class VacationEditComponent implements OnInit, OnDestroy {
  private editVacationModal: any;
  vacationModel: EditVacationCommand = new EditVacationCommand();
  disabledDate = new Date();
  disableSave = false;
  @Output() eventOut: EventEmitter<string> = new EventEmitter();
  constructor(private _vacationClient: VacationsClient, private _toastr: ToastrService) { 
    this.disableSave = false;
  }

  ngOnInit() {
    this.editVacationModal = ($('.vacation-day') as any).modal({ closable: false });
  }
  addvacationEdit() {
    this.vacationModel = new CreateVacationCommand();
    this.editVacationModal.modal('show');
    this.calendarPickerInit(new Date());
  }
  editVacation(vacation){
    
    if (vacation) {
      this.vacationModel = { ...vacation};
      this.calendarPickerInit(new Date());
      this.iniEditCalendar(vacation);
      this.editVacationModal.modal('show');
 
    }
  }
  calendarPickerInit(date) {
    ($('#vacationStart') as any).calendar({
      type: 'date',
      //minDate: date,
      endCalendar: $('#vacationEnd'),
      onChange: (date) => {
        this.vacationModel.startDate = date;
      },
      isDisabled: disableWeekends

    });
    ($('#vacationEnd') as any).calendar({
      type: 'date',
      //minDate: date,
      startCalendar: $('#vacationStart'),
      onChange: (date) => {
        this.vacationModel.endDate = date;
      },
      isDisabled: disableWeekends
    });
  }
  iniEditCalendar(vacation) {
    //const createdStart = moment(new Date(vacation.startDate)).endOf('day').format('DD-MMMM-Y');
    //const createdEnd = moment(new Date(vacation.endDate)).endOf('day').format('DD-MMMM-Y');
    ($('#vacationStart') as any).calendar('set date', vacation.startDate);
    ($('#vacationEnd') as any).calendar('set date', vacation.endDate);
  }

  setStartAndEndHourForVacations(startDate, endDate) {
    startDate.setHours(12);
    startDate.setMinutes(0);
    endDate.setHours(12);
    endDate.setMinutes(0);
  }

  save() {
    if(this.vacationModel.id){
      this.disableSave = true;
      this.setStartAndEndHourForVacations(this.vacationModel.startDate, this.vacationModel.endDate);
      console.log("vacation model", this.vacationModel);
        this._vacationClient.editVacation(this.vacationModel.id, this.vacationModel).subscribe((response: SwaggerResponse<Unit>) => {
        this._toastr.success('Vacation modified');
        this.disableSave = false;
        this.editVacationModal.modal('hide');
        this.eventOut.emit('refresh');
        this.resetForm();
      }, (e) => {
        console.log('err', e);
        this.editVacationModal.modal('show');
        this.eventOut.emit('error');
      //  this.resetForm();
        this.disableSave = false;
      });
    }else{
      this.disableSave = true;
      this.setStartAndEndHourForVacations(this.vacationModel.startDate, this.vacationModel.endDate);
      this._vacationClient.createVacation(this.vacationModel).subscribe((response: SwaggerResponse<Unit>) => {
        this._toastr.success("Successfully added vacation");
        this.disableSave = false;
      this.editVacationModal.modal('hide');
      this.eventOut.emit('refresh');
      this.resetForm();
    }, (e) => {
      console.log('err', e);
      this.editVacationModal.modal('show');
      this.eventOut.emit('error');
     // this.resetForm();
      this.disableSave = false;
    });
  }
  }

  resetForm() {
    ($('#vacationStart') as any).calendar('clear');
    ($('#vacationEnd') as any).calendar('clear');
  }

  onClickCancel() {
      this.resetForm(); 
      this.editVacationModal.modal('hide');
  
  }

  ngOnDestroy(){
    if (this.editVacationModal) {
      this.resetForm();
      this.editVacationModal.modal('hide');
      this.editVacationModal.remove();
    }
  }

}
