﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Vacations.Models
{
    public class DaysOffDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumberOfDays { get; set; }
        public bool IsCustomDayOff { get; set; }
        public bool ForEveryone { get; set; }
        public Dictionary<int, string> Users { get; set; }

        public static Expression<Func<Persistence.Models.DaysOff, DaysOffDto>> Projection
        {
            get
            {
                return p => new DaysOffDto()
                {
                    Id = p.Id,
                    Name = p.Name,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate,
                    NumberOfDays = p.NumberOfDays,
                    IsCustomDayOff = p.IsCustomDaysOff,
                    Users = p.UserDaysOff.Select(d => new { d.User.FirstName, d.User.LastName, d.UserId })
                                         .ToDictionary(t => t.UserId, t => (t.FirstName + " " + t.LastName)),
                    ForEveryone = p.UserDaysOff.Any() ? false : true
                };
            }
        }

        public static DaysOffDto Create(Persistence.Models.DaysOff DaysOff)
        {
            return Projection.Compile().Invoke(DaysOff);
        }
    }
}
