import { DaysOffClient, DaysOffDto, ReferenceClient, EditDaysOffCommand, SwaggerResponse, Unit, CreateVacationCommand } from './../../../client-api';
import { Component, OnInit, OnDestroy, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import 'semantic-ui-calendar/src/definitions/modules/calendar';
import * as moment from 'moment';
import * as _ from 'lodash';
import { getDayOfYear, getDate } from 'date-fns';
import { disableWeekends } from 'app/shared/utilities/utilities';

@Component({
  selector: 'app-day-edit',
  templateUrl: './day-edit.component.html',
})
export class DayEditComponent implements OnInit, AfterViewInit, OnDestroy {
  private editDayModal: any;
  dayOffModel: any;
  //dayOffModel: EditDaysOffCommand;
  daysForm = null;
  disableSave = false;
  disabledDate = new Date();
  private allUsers: { [id: number]: string };
  private usersDropdown: any;
  private dayOffTypeDropdown: any;
  usersForDaysOff: any;
  @Output() eventOut: EventEmitter<string> = new EventEmitter();
  dayOffType = ['Legal day off', 'Custom day off'];
  selectedDayOffType: any;

  constructor(private _referenceClient: ReferenceClient, private _fb: FormBuilder, private _toastr: ToastrService, private _daysOffClient: DaysOffClient) {
    this.daysForm = _fb.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
      forEveryone: [''],
      customUsersIds: [''],
      usersForDaysOff: [''],
      selectedDayOffType: ['']
    });

    this.dayOffModel = new EditDaysOffCommand();
    this.disableSave = false;
  }

  ngOnInit() {
    this.editDayModal = ($('.edit-day') as any).modal({ closable: false });
    this.getAllUsers();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.calendarPickerInit(new Date());
    }, 2000);
    this.usersDropdown =
      ($('.users-dropdown') as any)
        .dropdown({
          onAdd: this.onAdd.bind(this),
          onRemove: this.onRemove.bind(this)
        });
    this.dayOffTypeDropdown = ($('.dayOffType-dropdown') as any).dropdown();
  }

  onAdd(addedValue, addedText, $addedChoice) {
    this.usersForDaysOff = this.usersForDaysOff || {};
    const id = _.invert(this.allUsers)[addedText];
    console.log('addedText:', addedText, id);
    this.usersForDaysOff[id] = addedText;
  }
  onRemove(removedValue, removedText, $removedChoice) {
    this.usersForDaysOff = this.usersForDaysOff || {};
    const id = _.invert(this.allUsers)[removedText];
    delete this.usersForDaysOff[id];
  }

  addDay() {

    this.dayOffModel = new EditDaysOffCommand();
    //this.dayOffModel.startDate = moment(this.dayOffModel.startDate).set({hour: 0, minute: 0, second: 0, millisecond: 0});
    this.editDayModal.modal('show');
    this.usersDropdown.dropdown('clear');
    this.dayOffTypeDropdown.dropdown('clear');
  }

  editDay(day) {
    if (day.isCustomDayOff) {
      this.selectedDayOffType = 'Custom day off';
      this.dayOffTypeDropdown.dropdown('set selected', 'Custom day off');
    }
    else {
      this.selectedDayOffType = 'Legal Day off';
      this.dayOffTypeDropdown.dropdown('set selected', 'Legal day off');
    }
    this.dayOffModel = { ...day };
    console.log("day ul ce vine pe edit", day);
    this.dayOffModel.customUsersIds = this.dayOffModel.customUsersIds || [];
    const selected = _.filter(this.allUsers, (value, key) => { return this.dayOffModel.customUsersIds.indexOf(key) > -1 });
    console.log("dayoffModel ids", Object.values(this.dayOffModel.users));
    setTimeout(() => this.usersDropdown.dropdown('set selected', Object.values(this.dayOffModel.users)), 100);
    
    this.editDayModal.modal('show');
    this.iniEditCalendar(day);

  }

  iniEditCalendar(day) {
    ($('#createdStart') as any).calendar('set date', day.startDate);
    ($('#createdEnd') as any).calendar('set date', day.endDate);
  }


  calendarPickerInit(date) {
    ($('#createdStart') as any).calendar({
      type: 'date',
      minDate: date,
      endCalendar: $('#createdEnd'),
      onChange: (date) => {
        this.dayOffModel.startDate = date;
      },
      isDisabled: disableWeekends
    });
    ($('#createdEnd') as any).calendar({
      type: 'date',
      minDate: date,
      startCalendar: $('#createdStart'),
      onChange: (date) => {
        this.dayOffModel.endDate = date;
      },
      isDisabled: disableWeekends

    });
  }
  save() {
    console.log('usersForDaysOff', this.usersForDaysOff);
    const users = _.keys(this.usersForDaysOff);
    if (this.dayOffModel.id) {
      this.setStartAndEndHour(this.dayOffModel.startDate, this.dayOffModel.endDate);
      console.log("edit start", this.dayOffModel.startDate);
      console.log("edit end", this.dayOffModel.endDate);
      this.disableSave = true;
      this.dayOffModel.customUsersIds = users;
      this._daysOffClient.editDaysOff(this.dayOffModel.id, this.dayOffModel).subscribe((response: SwaggerResponse<Unit>) => {
        this.editDayModal.modal('hide');
        this.eventOut.emit('refresh');
        this.disableSave = false;
        this.resetForm();
        this._toastr.success('Successfully edited day off');
      }, (e) => {
        this.eventOut.emit('error');
        this.editDayModal.modal('show');
        this.disableSave = false;
      });

    } else {
      this.dayOffModel.customUsersIds = users;
      this.disableSave = true;
      this.setStartAndEndHour(this.dayOffModel.startDate, this.dayOffModel.endDate);
      console.log("start:", this.dayOffModel.startDate);
      console.log("start:", this.dayOffModel.endDate);
      this._daysOffClient.createDaysOff(this.dayOffModel).subscribe((response: SwaggerResponse<Unit>) => {
        this.disableSave = false;
        this.editDayModal.modal('hide');
        this.eventOut.emit('refresh');
        this.resetForm();
        this._toastr.success('Successfully added day off');
      }, (e) => {
        this.disableSave = false;
        this.eventOut.emit('error');
        this.editDayModal.modal('show');

      });
    }
  }

  setStartAndEndHour(startDate, endDate) {
    startDate.setHours(0);
    startDate.setMinutes(0);
    endDate.setHours(23);
    endDate.setMinutes(59);
  }
  resetForm() {
    this.daysForm.reset();
    ($('#createdStart') as any).calendar('clear');
    ($('#createdEnd') as any).calendar('clear');
  }

  onClickCancel() {
    this.resetForm();
    this.usersDropdown.dropdown('clear');
    this.dayOffTypeDropdown.dropdown('clear');
    this.editDayModal.modal('hide');
  }

  ngOnDestroy() {
    this.editDayModal.modal('hide');
    this.editDayModal.remove();
  }
  getAllUsers() {
    this._referenceClient.getUsers()
      .subscribe((response: SwaggerResponse<any>) => {
        this.allUsers = response.result;
        console.log("users", this.allUsers);
      });
  }
  handleChanges(event, field) {
    this.selectedDayOffType = event.target.value;
    if (this.selectedDayOffType == 'Custom day off') {
      setTimeout(() => {
        this.usersDropdown =
          ($('.users-dropdown') as any)
            .dropdown({
              onAdd: this.onAdd.bind(this),
              onRemove: this.onRemove.bind(this)
            });
      }, 100)

    }
    console.log("selected day off type", this.selectedDayOffType);
  }
}
