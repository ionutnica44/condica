using CVU.Condica.Application.Interfaces;
using CVU.Utilities.Util;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CVU.Utilities.Email
{
    [Obsolete("Need to change email service after updating packages and framework")]
    public sealed class EmailService : IHideObjectMembers, IDisposable, IEmailService
    {
        private readonly IHostingEnvironment env;
        private readonly IEnumerable<MailboxAddress> systemErrorRecipients;
        private readonly IConfiguration configuration;

        private readonly SmtpOptions smtpOptions;

        private readonly ICollection<MailboxAddress> to;
        private readonly ICollection<MailboxAddress> cc;
        private readonly ICollection<MailboxAddress> bcc;
        private readonly ICollection<MimePart> attachments;
        private readonly MessagePriority priority;

        private MailboxAddress from;
        private string subject;
        private MimePart body;

        public EmailService(IOptions<SmtpOptions> smtpOptions, IHostingEnvironment env, IConfiguration configuration)
        {
            this.env = env;
            this.smtpOptions = smtpOptions.Value;
            this.configuration = configuration;
            systemErrorRecipients = ParseAddresses(this.smtpOptions.SystemErrorRecipients);

            from = new MailboxAddress(AssemblyName(), this.smtpOptions.From);
            to = new List<MailboxAddress>();
            cc = new List<MailboxAddress>();
            bcc = new List<MailboxAddress>();
            attachments = new List<MimePart>();
            priority = MessagePriority.Normal;
        }

        /// <summary>
        /// Creates a new email instance using the default from
        /// address from smtp config settings
        /// </summary>
        public void FromDefault()
        {
            From(smtpOptions.From);
        }

        /// <summary>
        /// Creates a new Email instance and sets the from
        /// property
        /// </summary>
        /// <param name="emailAddress">Email address to send from</param>
        public void From(string emailAddress)
        {
            if (ValidateEmailAddress(emailAddress))
            {
                from = new MailboxAddress(AssemblyName(), emailAddress);
            }
        }

        #region To

        /// <summary>
        /// Adds a reciepient to the email
        /// </summary>
        /// <param name="emailAddress">Email address of recipeient</param>
        /// <returns></returns>
        public void AddRecipient(string emailAddress)
        {
            if (ValidateEmailAddress(emailAddress))
            {
                to.Add(new MailboxAddress(emailAddress));
            }
        }

        /// <summary>
        /// Adds all reciepients in list to email
        /// </summary>
        /// <param name="mailAddresses">List of recipients</param>
        public void AddRecipients(IEnumerable<string> emailAddresses)
        {
            if (emailAddresses != null)
            {
                foreach (var address in emailAddresses)
                {
                    AddRecipient(address);
                }
            }
        }

        /// <summary>
        /// Adds system reciepients in list to email
        /// </summary>
        public EmailService AddSystemEmails()
        {
            foreach (var address in systemErrorRecipients)
            {
                to.Add(address);
            }

            return this;
        }

        #endregion

        #region CC

        /// <summary>
        /// Adds a Carbon Copy to the email
        /// </summary>
        /// <param name="emailAddress">Email address to cc</param>
        public void AddCCRecipient(string email)
        {
            if (ValidateEmailAddress(email))
            {
                cc.Add(new MailboxAddress(email));
            }
        }

        /// <summary>
        /// Adds all Carbon Copy in list to an email
        /// </summary>
        /// <param name="mailAddresses">List of recipients to CC</param>
        public void AddCCRecipients(IEnumerable<string> mailAddresses)
        {
            if (mailAddresses != null)
            {
                foreach (var address in mailAddresses)
                {
                    AddCCRecipient(address);
                }

            }
        }

        #endregion

        /// <summary>
        /// Sets the subject of the email. If subject contains '\n', or '\r', or any combination of them, it will remove them 
        /// because they are invalid in email subject. 
        /// </summary>
        /// <param name="subjectParameter">email subject</param>
        public void AddSubject(string subjectParameter)
        {
            subject = subjectParameter.Replace("\r", string.Empty).Replace("\n", string.Empty);

            if (!configuration["BaseUrl"].Contains("condica.cvuintell.com"))
            {
                subject = $"[{env.EnvironmentName}] [{AssemblyName()}] - {subject}";
            }
        }

        /// <summary>
        /// Adds a Body to the Email
        /// </summary>
        /// <param name="bodyParameter">The content of the body</param>
        public void AddBody(string bodyParameter)
        {
            // var finalHtmlEmail = PreMailer.Net.PreMailer.MoveCssInline(body, true, null, null, true, true);
            body = new TextPart(TextFormat.Html)
            {
                Text = bodyParameter
            };
        }

        #region Attachments
        /// <summary>
        /// Adds an Attachment to the Email
        /// </summary>
        /// <param name="attachment">The Attachment to add</param>
        public void AddAttachment(Stream content, string fileName, string mediaType, string mediaSubType)
        {
            var contentType = new ContentType(mediaType, mediaSubType);

            var att = new MimePart(contentType)
            {
                Content = new MimeContent(content),
                ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                ContentTransferEncoding = ContentEncoding.Base64,
                FileName = fileName
            };

            attachments.Add(att);
        }
        #endregion

        /// <summary>
        /// Sends email synchronously
        /// </summary>
        public void Send()
        {
            AddSystemEmails();

            using var client = new SmtpClient();
            if (env.IsDevelopment()) client.ServerCertificateValidationCallback = (s, c, h, e) => true;

            client.Connect(smtpOptions.Host, smtpOptions.Port, smtpOptions.EnableSsl);

            // Note: since we don't have an OAuth2 token, disable
            // the XOAUTH2 authentication mechanism.
            client.AuthenticationMechanisms.Remove("XOAUTH2");

            client.Authenticate(smtpOptions.UserName, smtpOptions.Password);

            client.Send(GenerateMessage());

            client.Disconnect(true);
        }



        /// <summary>
        /// Sends email asynchronously
        /// </summary>
        public async Task SendAsync(string subject, string body, string from, IEnumerable<string> recipients, IEnumerable<string> ccRecipients = null)
        {
            AddSubject(subject);
            AddBody(body);
            From(from);
            AddRecipients(recipients);

            if (ccRecipients != null)
            {
                AddCCRecipients(ccRecipients);
            }

            if (env.IsDevelopment())
            {
                AddSystemEmails();
            }

            using var client = new SmtpClient();
            if (env.IsDevelopment()) client.ServerCertificateValidationCallback = (s, c, h, e) => true;

            client.Connect(smtpOptions.Host, smtpOptions.Port, smtpOptions.EnableSsl);

            // Note: since we don't have an OAuth2 token, disable
            // the XOAUTH2 authentication mechanism.
            client.AuthenticationMechanisms.Remove("XOAUTH2");

            client.Authenticate(smtpOptions.UserName, smtpOptions.Password);

            await client.SendAsync(GenerateMessage());

            await client.DisconnectAsync(true);
        }


        /// <summary>
        /// Sends message asynchronously with a callback
        /// handler
        /// </summary>
        public async Task<EmailService> SendAsync()
        {
            if (env.IsDevelopment()) AddSystemEmails();

            using (var client = new SmtpClient())
            {
                client.Connect(smtpOptions.Host, smtpOptions.Port, smtpOptions.EnableSsl);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(smtpOptions.UserName, smtpOptions.Password);

                await client.SendAsync(GenerateMessage());

                await client.DisconnectAsync(true);
            }

            return this;
        }

        /// <summary>
        /// Releases all resources
        /// </summary>
        public void Dispose()
        {
        }

        [Obsolete]
        private static IEnumerable<MailboxAddress> ParseAddresses(string emailAddresses)
        {
            if (string.IsNullOrEmpty(emailAddresses)) return Enumerable.Empty<MailboxAddress>();

            return emailAddresses.Split(';')
                .Select(x => x.Trim())
                .Where(ValidateEmailAddress)
                .Select(x => new MailboxAddress(x));
        }

        private static bool ValidateEmailAddress(string emailAddress)
        {
            return !string.IsNullOrEmpty(emailAddress) && new EmailAddressAttribute().IsValid(emailAddress);
        }

        private string AssemblyName()
        {
            return System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
        }

        private MimeMessage GenerateMessage()
        {
            var message = new MimeMessage();
            message.From.Add(from);
            message.To.AddRange(to);
            message.Cc.AddRange(cc);
            message.Bcc.AddRange(bcc);
            message.ReplyTo.Add(from);
            message.Subject = subject;
            message.Priority = priority;

            var multipart = new Multipart("mixed")
                                {
                                    body
                                };
            foreach (var attachment in attachments) multipart.Add(attachment);

            message.Body = multipart;

            return message;
        }
    }
}