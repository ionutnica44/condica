using CVU.Condica.Application.Account.Models;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Interfaces;
using CVU.Condica.Persistence.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application
{
    public abstract class RequestHandler<TRequest, TResponse> : Session, IRequestHandler<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        protected AppDbContext appDbContext;

        public RequestHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            appDbContext = (AppDbContext)serviceProvider.GetService(typeof(AppDbContext));
        }

        public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);

        protected void SetUpdateFields(ITrackingEntity entity)
        {
            if (entity.Id == default)
            {
                entity.CreatedAt = DateTime.UtcNow;
                entity.CreatedById = CurrentUser.Id;
            }

            entity.LastUpdatedAt = DateTime.UtcNow;
            entity.LastUpdatedById = CurrentUser.Id;
        }
    }

    public class Session : ISession
    {
        public IHttpContextAccessor HttpContextAccessor { get; set; }
        public CurrentUser CurrentUser { get; set; }
        public int CurrentCompany { get; set; }

        public Session(IServiceProvider serviceProvider)
        {
            HttpContextAccessor = (IHttpContextAccessor)serviceProvider.GetService(typeof(IHttpContextAccessor));
            CurrentUser = GetCurrentUser();
            CurrentCompany = CurrentUser != null ? CurrentUser.CompanyId : 0;
        }

        private CurrentUser GetCurrentUser()
        {
            if (HttpContextAccessor.HttpContext == null || HttpContextAccessor.HttpContext.User == null)
            {
                return null;
            }
            var principal = HttpContextAccessor.HttpContext.User;

            var email = principal.FindFirstValue(ClaimTypes.NameIdentifier);
            var role = principal.FindFirstValue(ClaimTypes.Role);
            var id = principal.FindFirstValue(JwtRegisteredClaimNames.Sid);
            var workspace = principal.FindFirstValue("Company");

            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            var user = new CurrentUser()
            {
                Id = int.Parse(id),
                Email = email,
                Role = Enum.Parse<Roles>(role),
                CompanyId = int.Parse(workspace)
            };

            return user;
        }
    }

}
