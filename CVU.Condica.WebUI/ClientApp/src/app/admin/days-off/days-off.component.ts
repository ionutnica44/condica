import { DayEditComponent } from './day-edit/day-edit.component';
import { SwaggerResponse, DaysOffDto, DeleteDaysOffCommand, ListUsersQuery } from './../../client-api';
import { AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DaysOffClient } from 'app/client-api';
import { ToastrService } from 'ngx-toastr';
import { Command } from 'protractor';
import { RequestOptions } from 'services/requestOptions';
import { AuthService } from 'services/auth/auth.service';

@Component({
  selector: 'app-days-off',
  templateUrl: './days-off.component.html',
  styleUrls: ['days-off.component.css']
})
export class DaysOffComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(DayEditComponent) dayEdit: DayEditComponent;
  loading: boolean;
  dayToDelete: number;
  daysOff: DaysOffDto[] = [];
  disabledButtonsDate = new Date();
  private deleteModal: any;
  currentUser: any;
  page: RequestOptions<ListUsersQuery> = new RequestOptions<ListUsersQuery>();
  deleteCommand: DeleteDaysOffCommand;
  constructor(private _daysOffClient: DaysOffClient, private _toastr: ToastrService, private _auth: AuthService) {
    this.loading = true;
    this.deleteCommand = new DeleteDaysOffCommand();
    this.currentUser = _auth.getCurrentUser();
  }

  refreshDaysList(event) {
    if (event === "refresh") {
      this.getDaysOff();
    }
  }

  ngOnInit() {
    this.getDaysOff();
  }

  ngAfterViewInit() {
    this.deleteModal = ($('.delete-day') as any).modal({ closable: false });
  }

  addDay() {
    this.dayEdit.addDay();
  }

  // editDay(day: DaysOffDto) {
  //   this.dayEdit.editDay(day);
  // }


  getDaysOff = () => {
    this._daysOffClient.listDaysOff(this.page.filter).subscribe((response: SwaggerResponse<DaysOffDto[]>) => {
      this.page.setPaginationInfo(response);
      this.daysOff = response.result;
      this.loading = false;
    });
  }

  deleteDay(day: DaysOffDto) {
    if (day) {
      this.dayToDelete = day.id;
      this.deleteCommand.id = day.id;
      this.deleteModal.modal('show');
    }
  }

  delete() {
    this._daysOffClient.deleteDaysOff(this.dayToDelete, this.deleteCommand).subscribe((response: SwaggerResponse<any>) => {
      this._toastr.success('Successfully deleted day off');
      this.deleteModal.modal('hide');
      this.getDaysOff();
    }, (e) => {
      this.deleteModal.modal('show');
    });
  }

  onClickCancel() {
    this.dayToDelete = 0;
    this.deleteModal.modal('hide');

  }


  ngOnDestroy() {
    this.deleteModal.modal('hide');
    this.deleteModal.remove();
  }



}
