import { SwaggerResponse } from 'app/client-api';
import { ChangePasswordCommand, UserClient } from './../../../../client-api';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PasswordValidators } from 'helpers/password-validators';

@Component({
  selector: 'user-change-password',
  templateUrl: './user-change-password.component.html',
})
export class UserChangePasswordComponent implements OnInit {
  @Input() loading: boolean;
  changePasswordForm: FormGroup = null;
  changePaswordModel: ChangePasswordCommand;
  loggingIn: boolean;
  constructor(private _fb: FormBuilder, private _toastr: ToastrService, private _account: UserClient) {
    this.changePasswordForm = this._fb.group({
      currentPassword: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([
        Validators.required,
        PasswordValidators.patternValidator(/\d/, { hasNumber: true }),
        PasswordValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        PasswordValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
        PasswordValidators.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
        Validators.minLength(8)
      ])],
      confirmPassword: [null, Validators.compose([Validators.required])]
    }, {
      validator: PasswordValidators.passwordMatchValidator,
    });

    this.loggingIn = false;
    this.changePaswordModel = new ChangePasswordCommand();
  }

  ngOnInit() {
    setTimeout(() => {
      this.loading = false;
    }, 500);
  }


  changePassword() {
    this._account.changePassword(this.changePaswordModel).subscribe((response: SwaggerResponse<any>) => {
      this._toastr.success('Your password has been updated', 'Success');
      this.changePasswordForm.reset();
    });

  }
}
