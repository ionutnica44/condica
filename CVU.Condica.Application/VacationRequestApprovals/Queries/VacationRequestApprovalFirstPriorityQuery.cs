﻿using CVU.Condica.Application.VacationRequestApprovals.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using CVU.Condica.Persistence.ReferenceData;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.VacationRequestApprovals.Queries
{
    public class VacationRequestApprovalFirstPriorityQuery : PaginatedQueryParameter, IRequest<PaginatedModel<VacationRequestApprovalDto>>
    {
    }
    public class VacationRequestApprovalFirstPriorityQueryHandler : RequestHandler<VacationRequestApprovalFirstPriorityQuery, PaginatedModel<VacationRequestApprovalDto>>
    {
        private readonly IPaginationService paginationService;

        public VacationRequestApprovalFirstPriorityQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<PaginatedModel<VacationRequestApprovalDto>> Handle(VacationRequestApprovalFirstPriorityQuery request, CancellationToken cancellationToken)
        {
            var vacationRequestApproval = appDbContext.VacationRequestApproval
                .Include(d => d.Vacation)
                .ThenInclude(d => d.User)
                .Include(d => d.SigningUser)
                .Where(d => d.Vacation.StatusId == (int)VacationsStatus.Pending && !d.Vacation.VacationRequestApproval.Any(a => a.SigningUserId == CurrentUser.Id));

            var paginatedResponse = paginationService.PaginatedResults<Persistence.Models.VacationRequestApproval, VacationRequestApprovalDto>(vacationRequestApproval, request);
            return new PaginatedModel<VacationRequestApprovalDto>(paginatedResponse);
        }
    }

    public class VacationRequestApprovalFirstPriorityQueryAuthorization : AuthorizationSetup<VacationRequestApprovalFirstPriorityQuery>
    {
        public VacationRequestApprovalFirstPriorityQueryAuthorization()
        {
        }
    }
}
