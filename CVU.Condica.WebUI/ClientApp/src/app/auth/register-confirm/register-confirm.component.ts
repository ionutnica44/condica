import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserClient, ActivateCommand } from 'app/client-api';

@Component({
  selector: 'app-register-confirm',
  templateUrl: './register-confirm.component.html',
})
export class RegisterConfirmComponent implements OnInit {
  disableBtn: boolean;
  loggingIn: boolean;
  confirmRegisterForm: FormGroup;
  activateModel: ActivateCommand = new ActivateCommand();
  constructor(private _fb: FormBuilder, private _account: UserClient, private _route: ActivatedRoute, private _router: Router, private _toastr: ToastrService) {
    this.confirmRegisterForm = this._fb.group({
      // acceptTermsAndCondactivateAccountitions: [false, Validators.pattern('true')],
    });
    this.disableBtn = true;
    this.loggingIn = false;
  }

  ngOnInit() {
    this._route.queryParams.subscribe(params => {
      // Defaults to 0 if no query param provided.
      const securityCode = params['SecurityCode'] || 0;
      if (securityCode) {
        this.activateModel.securityCode = securityCode;
        this.disableBtn = false;
      }
    });
  }



  activateAccount() {
    this.loggingIn = true;
    this._account.activate(this.activateModel).subscribe((response) => {
      this._toastr.success('Account was activated', '');
      setTimeout(() => {
        this.loggingIn = false;
        this._router.navigateByUrl('auth/login');
      }, 500);
    }, (error) => {
      this.loggingIn = false;
      this._router.navigateByUrl('auth/login');
    });
  }




}
