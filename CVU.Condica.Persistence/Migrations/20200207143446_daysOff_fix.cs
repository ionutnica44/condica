﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace CVU.Condica.Persistence.Migrations
{
    public partial class daysOff_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDaysOff_DaysOff_DayOffId",
                table: "UserDaysOff");

            //migrationBuilder.DeleteData(
            //    table: "User",
            //    keyColumn: "Id",
            //    keyValue: 3);

            //migrationBuilder.DeleteData(
            //    table: "User",
            //    keyColumn: "Id",
            //    keyValue: 4);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDaysOff_DaysOff_DayOffId",
                table: "UserDaysOff",
                column: "DayOffId",
                principalTable: "DaysOff",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDaysOff_DaysOff_DayOffId",
                table: "UserDaysOff");

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "CompanyId", "CreatedAt", "Email", "FirstName", "IsActivated", "IsBlocked", "LastName", "LastUpdatedAt", "Password", "PhoneNumber", "PositionId", "RoleId", "SecurityCode", "SecurityCodeExpiresAt", "SignPriorityId" },
                values: new object[] { 3, 1, new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ionut.nica@cvu.ro", "Ionut", true, false, "Nica", new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AA7K81530367D3n5yedJkG+KnczUiMh7hiMsVwzrvMGFL0s+VfFVYtJM6fIFtOC2Yw==", "", 3, 1, null, null, 3 });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "CompanyId", "CreatedAt", "Email", "FirstName", "IsActivated", "IsBlocked", "LastName", "LastUpdatedAt", "Password", "PhoneNumber", "PositionId", "RoleId", "SecurityCode", "SecurityCodeExpiresAt", "SignPriorityId" },
                values: new object[] { 4, 1, new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test@cvu.ro", "Test", true, false, "Test", new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AA7K81530367D3n5yedJkG+KnczUiMh7hiMsVwzrvMGFL0s+VfFVYtJM6fIFtOC2Yw==", "", 3, 1, null, null, 3 });

            migrationBuilder.AddForeignKey(
                name: "FK_UserDaysOff_DaysOff_DayOffId",
                table: "UserDaysOff",
                column: "DayOffId",
                principalTable: "DaysOff",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
