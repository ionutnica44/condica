using System;
using System.Linq.Expressions;

namespace CVU.Condica.Common.Pagination
{
    public abstract class IPaginationEntity<TSource, TDestination>
    {
        Expression<Func<TSource, TDestination>> Projection { get; }
    }
}
