﻿using CVU.Condica.Persistence.Infrastructure;
using CVU.Condica.Persistence.ReferenceData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public class SignPriority
    {
        public SignPriority()
        {
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }

    public class SignPriorityConfiguration : IEntityTypeConfiguration<SignPriority>
    {
        public void Configure(EntityTypeBuilder<SignPriority> builder)
        {
            builder.HasReferenceData<SignPriorities, SignPriority>(d => d.Id, d => d.Name);
        }
    }
}
