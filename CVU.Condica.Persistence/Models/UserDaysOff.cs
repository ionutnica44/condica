﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CVU.Condica.Persistence.Models
{
    public class UserDaysOff
    {
        public int UserId { get; set; }
        public int DayOffId { get; set; }
        public DaysOff DaysOff { get; set; }
        public User User { get; set; }
    }
    public class UserDaysOffConfiguration : IEntityTypeConfiguration<UserDaysOff>
    {
        public void Configure(EntityTypeBuilder<UserDaysOff> builder)
        {
            builder
                .HasOne(d => d.User)
                .WithMany(d => d.UserDaysOff)
                .HasForeignKey(ud => ud.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(d => d.DaysOff)
                .WithMany(d => d.UserDaysOff)
                .HasForeignKey(ud => ud.DayOffId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasKey(ud => new { ud.DayOffId, ud.UserId });
        }
    }
}
