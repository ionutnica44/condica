using System;

namespace CVU.Condica.Persistence.Models
{
    public partial class AccessLogEntries
    {
        public int Id { get; set; }
        public DateTime AccessUtcDateTime { get; set; }
        public string ActionName { get; set; }
        public string AreaName { get; set; }
        public string ControllerName { get; set; }
        public string IpAddress { get; set; }
        public string Method { get; set; }
        public string RequestArguments { get; set; }
        public string RoleName { get; set; }
        public string UserIname { get; set; }
    }
}
