
namespace CVU.Condica.Application.Validation
{
    public static class ValidationMessages
    {
        #region Accounts

        public static readonly string InvalidSecurityCode = "The security code is either invalid or expired.";
        public static readonly string UserNotInDatabase = "User not found in the database.";
        public static readonly string UserAlreadyExists = "The user already exists in the database.";
        public static readonly string ValidationCodeRequired = "Validation code is required.";
        public static readonly string PasswordRequiremets = "Password requirements are not met.";
        public static readonly string PasswordsDoNotMatch = "Password and confirm password are not the same.";
        public static readonly string PasswordLength = "Password must have a length between 2 and 100 characters.";
        public static readonly string FirstNameLength = "First name must have a length between 2 and 50 characters.";
        public static readonly string LastNameLength = "Last name must have a length between 2 and 50 characters.";
        public static readonly string InvalidFirstName = "Invalid first name format.";
        public static readonly string InvalidLastName = "Invalid last name format.";
        public static readonly string SelfBlock = "You cannot block your own account.";
        public static readonly string NoRoleSelected = "You must select a role.";
        public static readonly string NoPositionSelected = "You must select a position.";
        public static readonly string NoCompanySelected = "You must select a company.";
        public static readonly string NoSigningUsersSelected = "You must select a signing user.";
        public static readonly string CanNotDeleteSigningUsers = "You can not delete this signing users.";
        public static readonly string UserAlreadyActivated = "The account is already active.";
        public static readonly string IsValidEmail = "The email address is not valid.";
        public static readonly string RoleNotInDatabase = "Role not found in the database.";
        public static readonly string PositionNotInDatabase = "Position not found in the database.";
        public static readonly string CompanyNotInDatabase = "Company not found in the database.";
        public static readonly string PasswordRequirements = "The password must contain at least a lowercase letter, an uppercase letter, a special character and a number.";
        #endregion

        #region Attendances

        public static readonly string AlreadyCheckedIn = "User already checked in today.";
        public static readonly string InvalidHoursRange = "The check-in hour must precede the checkout hour.";
        public static readonly string AttendanceNotFound = "Attendance not found in the database.";
        public static readonly string WeekendCheckIn = "You cannot check-in during weekends.";
        public static readonly string DayOffCheckIn = "You cannot check-in during day off.";
        public static readonly string VacationCheckIn = "You cannot check-in during vacation.";

        #endregion 

        #region Vacations
        public static readonly string VacationNotInDatabase = "Vacation not found in the database.";
        public static readonly string VacationRequestNotInDatabase = "Vacation request not found in the database.";
        public static readonly string DateHasPassed = "The date exceeded the current date.";
        public static readonly string IsWeekend = "This is a weekend day.";
        public static readonly string VacationAlreadyExists = "You have already selected these days for another vacation.";
        public static readonly string EndDateAfterStartDate = "The end date cannot be earlier than the start date.";
        public static readonly string TakenDaysLessThanTotalDays = "Your taken days can not be more than your total days.";
        public static readonly string VacationStatusNotPending = "You can not edit your vacation anymore.";
        public static readonly string VacationAlreadyApproved = "You vacation has been approved by one or more signing users. Is not editable anymore.";
        public static readonly string VacationOnDaysOff = "Vacations cannot start or end on legal holiday periods.";
        public static readonly string VacationNotEligibleForDelete = "You cannot delete vacations that already started or rejected vacations.";
        public static readonly string OutOfVacationDays = "You don't have as many vacation days left as you requested.";
        public static readonly string AlreadySigned = "You already signed this vacation.";
        public static readonly string CheckAttendance = "You can not create a vacation if you did not check in every day.";
        #endregion

        #region DaysOff
        public static readonly string DayOffNotInDatabase = "Day off not found in the database.";
        public static readonly string DayOffAlreadyExists = "The requesting Days off intersects an existing period.";
        internal static readonly string CannotRevokePermission = "You can't revoke sign permission to this user.";
        public static readonly string CustomDayOffAlreadyExists = "The requesting Custom Days off intersects an existing period.";
        public static readonly string DaysOffMultipleUsers = "Uncheck 'For Everyone' property if you want to assign a day off to a specific user.";
        #endregion

        #region Item

        public static readonly string ItemNotInDatabase = "Item not found in the database.";
        public static readonly string ItemCategoryEmpty = "The category cannot be empty.";

        #endregion
    }
}
