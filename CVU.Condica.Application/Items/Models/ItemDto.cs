﻿using System;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Items.Models
{
    public class ItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public byte[] Image { get; set; }
        public string SerialNumber { get; set; }
        public int? UserId { get; set; }
        public int ItemTypeId { get; set; }
        public string UserName { get; set; }
        public string ItemType { get; set; }

        public static Expression<Func<Persistence.Models.Item, ItemDto>> Projection
        {
            get
            {
                return p => new ItemDto()
                {
                    Id = p.Id,
                    Name = p.Name,
                    //Image = System.Convert.FromBase64String(p.Image),
                    SerialNumber = p.SerialNumber,
                    UserId = p.UserId,
                    ItemTypeId = p.ItemTypeId,
                    UserName = p.User.FirstName + " " + p.User.LastName,
                    ItemType = p.ItemType.Name
                };
            }
        }

        public static ItemDto Create(Persistence.Models.Item Item)
        {
            return Projection.Compile().Invoke(Item);
        }
    }
}
