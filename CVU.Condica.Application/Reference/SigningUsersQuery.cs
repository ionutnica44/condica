﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Reference
{
    public class SigningUsersQuery : IRequest<Dictionary<int, string>>
    {
    }

    public class SigningUsersQueryHandler : RequestHandler<SigningUsersQuery, Dictionary<int, string>>
    {
        public SigningUsersQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<Dictionary<int, string>> Handle(SigningUsersQuery request, CancellationToken cancellationToken)
        {
            var users = appDbContext.User
                .Where(d => d.SignPriorityId != null)
                .ToDictionary(d => d.Id, d => d.FirstName + " " + d.LastName);

            return users;
        }
    }
}
