using CVU.Condica.Application.Account.Commands;
using CVU.Condica.Application.Account.Queries;
using CVU.Condica.Application.Attendances.Commands;
using CVU.Condica.Application.Attendances.Queries;
using CVU.Condica.Application.DaysOff.Commands;
using CVU.Condica.Application.Items.Commands;
using CVU.Condica.Application.Items.Queries;
using CVU.Condica.Application.VacationRequestApprovals.Commands;
using CVU.Condica.Application.Vacations.Commands;
using CVU.Condica.Application.Vacations.Queries;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using FluentValidation.Validators;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CVU.Condica.Application.Validation
{
    public class ValidationService
    {
        private readonly AppDbContext appDbContext;
        private readonly ISession session;
        private readonly Utilities utilitiesValidation;

        public ValidationService(AppDbContext appDbContext, ISession session, Utilities utilitiesValidation)
        {
            this.appDbContext = appDbContext;
            this.session = session;
            this.utilitiesValidation = utilitiesValidation;
        }

        #region Accounts
        public void ValidateUserRegister(RegisterCommand model, CustomContext customContext)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                customContext.AddFailure("Email can not be empty.");
            }
            else
            {
                if (appDbContext.User.Any(d => d.Email == model.Email))
                {
                    customContext.AddFailure(ValidationMessages.UserAlreadyExists);
                }
                if (!Utilities.IsValidEmail(model.Email))
                {
                    customContext.AddFailure(ValidationMessages.IsValidEmail);
                }
            }
            if (string.IsNullOrEmpty(model.PhoneNumber))
            {
                customContext.AddFailure("Phone number can not be empty.");
            }
            else if (!Regex.IsMatch(model.PhoneNumber, @"^[0-9]*$"))
            {
                customContext.AddFailure("Phone number is not valid.");
            }
            if (string.IsNullOrEmpty(model.FirstName))
            {
                customContext.AddFailure("First name can not be empty.");
            }
            else if (!Regex.IsMatch(model.FirstName, @"^[a-zA-Z- ]+$"))
            {
                customContext.AddFailure(ValidationMessages.InvalidFirstName);
            }
            if (string.IsNullOrEmpty(model.LastName))
            {
                customContext.AddFailure("Last name can not be empty.");
            }
            else if (!Regex.IsMatch(model.LastName, @"^[a-zA-Z- ]+$"))
            {
                customContext.AddFailure(ValidationMessages.InvalidLastName);
            }
            if (!Utilities.NameLength(model.FirstName))
            {
                customContext.AddFailure(ValidationMessages.FirstNameLength);
            }
            if (!Utilities.NameLength(model.LastName))
            {
                customContext.AddFailure(ValidationMessages.LastNameLength);
            }
            if (model.RoleId == 0)
            {
                customContext.AddFailure(ValidationMessages.NoRoleSelected);
            }
            else if (!appDbContext.Role.Any(d => d.Id == model.RoleId))
            {
                customContext.AddFailure(ValidationMessages.RoleNotInDatabase);
            }
            if (model.PositionId == 0)
            {
                customContext.AddFailure(ValidationMessages.NoPositionSelected);
            }
            else if (!appDbContext.Position.Any(d => d.Id == model.PositionId))
            {
                customContext.AddFailure(ValidationMessages.PositionNotInDatabase);
            }
            if (model.CompanyId == 0)
            {
                customContext.AddFailure(ValidationMessages.NoCompanySelected);
            }
            else if (!appDbContext.Company.Any(d => d.Id == model.CompanyId))
            {
                customContext.AddFailure(ValidationMessages.CompanyNotInDatabase);
            }
            if (model.SigningUsers == null)
            {
                customContext.AddFailure(ValidationMessages.NoSigningUsersSelected);
            }
        }

        public void ValidateActivateCommand(ActivateCommand model, CustomContext customContext)
        {

            if (string.IsNullOrEmpty(model.SecurityCode))
            {
                customContext.AddFailure(ValidationMessages.ValidationCodeRequired);
            }
            else if (!appDbContext.User.Any(d => d.SecurityCode == model.SecurityCode && d.SecurityCodeExpiresAt > DateTime.Now))
            {
                customContext.AddFailure(ValidationMessages.InvalidSecurityCode);
            }
        }

        public static void ValidateChangePasswordCommand(ChangePasswordCommand model, CustomContext customContext)
        {
            if (string.IsNullOrEmpty(model.CurrentPassword))
            {
                customContext.AddFailure("Current password can not be empty.");
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword))
            {
                customContext.AddFailure("Confirm password can not be empty.");
            }
            if (string.IsNullOrEmpty(model.NewPassword) && string.IsNullOrEmpty(model.ConfirmPassword))
            {
                if (!model.NewPassword.Equals(model.ConfirmPassword))
                {
                    customContext.AddFailure(ValidationMessages.PasswordsDoNotMatch);
                }
            }
            if (string.IsNullOrEmpty(model.NewPassword))
            {
                customContext.AddFailure("New password can not be empty.");
            }
            else if (!Regex.IsMatch(model.NewPassword, @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+~()_]).{8,}$"))
            {
                customContext.AddFailure(ValidationMessages.PasswordRequiremets);
            }
        }

        public void ValidateEditProfileCommand(EditProfileCommand model, CustomContext customContext)
        {
            if (model.UserId.HasValue)
            {
                if (!(!model.CanSign.HasValue || utilitiesValidation.CanRevokeSignPermission(model.UserId.Value)))
                {
                    customContext.AddFailure(ValidationMessages.CannotRevokePermission);
                }
            }
            if (!string.IsNullOrEmpty(model.PhoneNumber))
            {
                if (!Regex.IsMatch(model.PhoneNumber, @"^[0-9]*$"))
                {
                    customContext.AddFailure("Phone number is not valid.");
                }
            }
            if (!string.IsNullOrEmpty(model.FirstName))
            {
                if (!Regex.IsMatch(model.FirstName, @"^[a-zA-Z- ]+$"))
                {
                    customContext.AddFailure(ValidationMessages.InvalidFirstName);
                }
                if (!Utilities.NameLength(model.FirstName))
                {
                    customContext.AddFailure(ValidationMessages.FirstNameLength);
                }
            }
            if (!string.IsNullOrEmpty(model.LastName))
            {
                if (!Regex.IsMatch(model.LastName, @"^[a-zA-Z- ]+$"))
                {
                    customContext.AddFailure(ValidationMessages.InvalidLastName);
                }
                if (!Utilities.NameLength(model.LastName))
                {
                    customContext.AddFailure(ValidationMessages.LastNameLength);
                }
            }
            if (model.UserId.HasValue)
            {
                if (!(!model.IsBlocked.HasValue || !utilitiesValidation.SelfBlock(model.UserId.Value, model.IsBlocked.Value)))
                {
                    customContext.AddFailure(ValidationMessages.SelfBlock);
                }
            }
            if (model.UserId.HasValue)
            {
                if (!(!model.UserId.HasValue || utilitiesValidation.CanNotDeleteSigningUsers(model.UserId.Value)))
                {
                    customContext.AddFailure(ValidationMessages.CanNotDeleteSigningUsers);
                }
            }
            if (model.RoleId.HasValue)
            {
                if (!appDbContext.Role.Any(d => d.Id == model.RoleId))
                {
                    customContext.AddFailure(ValidationMessages.RoleNotInDatabase);
                }
            }
            if (model.PositionId.HasValue)
            {
                if (!appDbContext.Position.Any(d => d.Id == model.PositionId))
                {
                    customContext.AddFailure(ValidationMessages.PositionNotInDatabase);
                }
            }
            if (model.CompanyId.HasValue)
            {
                if (!appDbContext.Company.Any(d => d.Id == model.CompanyId))
                {
                    customContext.AddFailure(ValidationMessages.CompanyNotInDatabase);
                }
            }
        }

        public void ValidateLoginCommand(LoginCommand model, CustomContext customContext)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                customContext.AddFailure("Email can not be empty.");
            }
            else
            {
                if (!Utilities.IsValidEmail(model.Email))
                {
                    customContext.AddFailure(ValidationMessages.IsValidEmail);
                }
                if (!appDbContext.User.Any(d => d.Email == model.Email))
                {
                    customContext.AddFailure(ValidationMessages.UserNotInDatabase);
                }
            }
            if (string.IsNullOrEmpty(model.Password))
            {
                customContext.AddFailure("Password can not be empty.");
            }
            else if (!Utilities.PasswordLength(model.Password))
            {
                customContext.AddFailure("Invalid password length.");
            }
        }

        public void ValidateResendActivationCommand(ResendActivationCommand model, CustomContext customContext)
        {
            if (!appDbContext.User.Any(d => d.Id == model.UserId))
            {
                customContext.AddFailure(ValidationMessages.UserNotInDatabase);
            }
            if (appDbContext.User.Any(d => d.Id == model.UserId && d.IsActivated))
            {
                customContext.AddFailure(ValidationMessages.UserAlreadyActivated);
            }
        }

        public void ValidateResetPasswordCommand(ResetPasswordCommand model, CustomContext customContext)
        {
            if (string.IsNullOrEmpty(model.Password))
            {
                customContext.AddFailure("Trebuie sa adaugati parola.");
            }
            else
            {
                if (!Utilities.ValidatePassword(model.Password))
                {
                    customContext.AddFailure(ValidationMessages.PasswordRequirements);
                }
                if (!Utilities.PasswordLength(model.Password))
                {
                    customContext.AddFailure(ValidationMessages.PasswordLength);
                }
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword))
            {
                customContext.AddFailure("Confirm password can not be empty.");
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword) && string.IsNullOrEmpty(model.Password))
            {
                if (!model.ConfirmPassword.Equals(model.Password))
                {
                    customContext.AddFailure(ValidationMessages.PasswordsDoNotMatch);
                }
            }
            if (string.IsNullOrEmpty(model.SecurityCode))
            {
                customContext.AddFailure("Security code can not be empty.");
            }
            if (!appDbContext.User.Any(d => d.SecurityCode == model.SecurityCode && d.SecurityCodeExpiresAt > DateTime.Now))
            {
                customContext.AddFailure(ValidationMessages.InvalidSecurityCode);
            }
        }

        public void ValidateResetPasswordSecurityCodeComamnd(ResetPasswordSecurityCodeComamnd model, CustomContext customContext)
        {
            if (!appDbContext.User.Any(d => d.Email == model.Email))
            {
                customContext.AddFailure(ValidationMessages.UserNotInDatabase);
            }
        }

        public void ValidateUserQuery(UserQuery model, CustomContext customContext)
        {
            if (!appDbContext.User.Any(d => d.Id == model.UserId))
            {
                customContext.AddFailure(ValidationMessages.UserNotInDatabase);
            }
        }
        #endregion

        #region Attendances

        public void ValidateSlashCommandCheckout(SlashCommandCheckout model, CustomContext customContext)
        {
            var user = appDbContext.User.Include(d => d.Attendances).FirstOrDefault(d => d.Email == model.CustomUserEmail);
            if (user == null)
            {
                customContext.AddFailure(ValidationMessages.UserNotInDatabase);
            }
            if (!user.Attendances.Any(d => d.ArrivalTime.Date == model.DepartureTime.Date))
            {
                customContext.AddFailure(ValidationMessages.AttendanceNotFound);
            }
            if (model.DepartureTime < user.Attendances.FirstOrDefault(d => d.ArrivalTime.Date == model.DepartureTime.Date).ArrivalTime)
            {
                customContext.AddFailure(ValidationMessages.InvalidHoursRange);
            }


        }
        public void ValidateEditAttendanceCommand(EditAttendanceCommand model, CustomContext customContext)
        {
            if (!appDbContext.Attendance.Any(d => d.Id == model.AttendanceId))
            {
                customContext.AddFailure(ValidationMessages.AttendanceNotFound);
            }
            else if (!utilitiesValidation.ValidateAttendance(model))
            {
                customContext.AddFailure(ValidationMessages.InvalidHoursRange);
            }
            if (!(!model.ArrivalTime.HasValue || (model.ArrivalTime.Value.DayOfWeek != DayOfWeek.Sunday && model.ArrivalTime.Value.DayOfWeek != DayOfWeek.Saturday)))
            {
                customContext.AddFailure(ValidationMessages.WeekendCheckIn);
            }
            if (model.ArrivalTime.HasValue)
            {
                if (!utilitiesValidation.CheckInDuringVacation(model.ArrivalTime))
                {
                    customContext.AddFailure(ValidationMessages.VacationCheckIn);
                }
                if (!utilitiesValidation.CheckInDuringDayOff(model.ArrivalTime))
                {
                    customContext.AddFailure(ValidationMessages.DayOffCheckIn);
                }
            }
        }

        public void ValidateCreateAttendanceCommand(CreateAttendanceCommand model, CustomContext customContext)
        {
            if (appDbContext.Attendance.Where(d => d.UserId == session.CurrentUser.Id).Any(d => d.ArrivalTime.Date == model.ArrivalTime.Value.Date))
            {
                customContext.AddFailure(ValidationMessages.AlreadyCheckedIn);
            }
            if (!(!model.ArrivalTime.HasValue || (model.ArrivalTime.Value.DayOfWeek != DayOfWeek.Sunday && model.ArrivalTime.Value.DayOfWeek != DayOfWeek.Saturday)))
            {
                customContext.AddFailure(ValidationMessages.WeekendCheckIn);
            }
            if (!utilitiesValidation.CheckInDuringVacation(model.ArrivalTime))
            {
                customContext.AddFailure(ValidationMessages.VacationCheckIn);
            }
            if (!utilitiesValidation.CheckInDuringDayOff(model.ArrivalTime))
            {
                customContext.AddFailure(ValidationMessages.DayOffCheckIn);
            }
            if (!(model.DepartureTime == null || model.DepartureTime > model.ArrivalTime))
            {
                customContext.AddFailure(ValidationMessages.InvalidHoursRange);
            }
        }

        public void ValidateAttendanceQuery(AttendanceQuery model, CustomContext customContext)
        {
            if (!appDbContext.Attendance.Any(d => d.Id == model.AttendanceId))
            {
                customContext.AddFailure(ValidationMessages.AttendanceNotFound);
            }
        }

        #endregion

        #region Vacations

        public void ValidateCreateVacationCommand(CreateVacationCommand model, CustomContext customContext)
        {
            if (model.StartDate == DateTime.MinValue)
            {
                customContext.AddFailure("Start date can not be empty.");
            }
            else
            {   //ADDING VACATIONS IN THE PAST SHOULD BE POSSIBLE
                //if (!Utilities.DateHasPassed(model.StartDate.ToLocalTime()))
                //{
                //    customContext.AddFailure(ValidationMessages.DateHasPassed);
                //}
                if (!Utilities.IsWeekend(model.StartDate.ToLocalTime()))
                {
                    customContext.AddFailure(ValidationMessages.IsWeekend);
                }
                //if (model.StartDate >= DateTime.Now.Date && !utilitiesValidation.CheckAttendance(model.StartDate))
                //{
                //    customContext.AddFailure(ValidationMessages.CheckAttendance);
                //}
            }
            if (model.EndDate == DateTime.MinValue)
            {
                customContext.AddFailure("End date can not be empty.");
            }
            else
            {
                //ADDING VACATIONS IN THE PAST SHOULD BE POSSIBLE
                //if (!Utilities.DateHasPassed(model.EndDate.ToLocalTime()))
                //{
                //    customContext.AddFailure(ValidationMessages.DateHasPassed);
                //}
                if (!Utilities.IsWeekend(model.EndDate.ToLocalTime()))
                {
                    customContext.AddFailure(ValidationMessages.IsWeekend);
                }
            }
            if (model.EndDate != DateTime.MinValue && model.StartDate != DateTime.MinValue)
            {
                if (!(model.EndDate.CompareTo(model.StartDate) >= 0))
                {
                    customContext.AddFailure(ValidationMessages.EndDateAfterStartDate);
                }
                if (!utilitiesValidation.HasVacationDaysleft(model.StartDate, model.EndDate))
                {
                    customContext.AddFailure(ValidationMessages.OutOfVacationDays);
                }
                if (appDbContext.Vacation
                .Where(d => d.UserId == session.CurrentUser.Id)
                .Where(d => d.StatusId != (int)VacationsStatus.Rejected)
                .Where(d => d.Id != 0)
                //start date in existing interval
                .Where(d => (model.StartDate.Date >= d.StartDate.Date && model.StartDate.Date <= d.EndDate.Date) ||
                            //end date in existing interval
                            (model.EndDate.Date >= d.StartDate.Date && model.EndDate.Date <= d.EndDate.Date) ||
                            //requesting vacation incudes an existing interval
                            (model.StartDate.Date <= d.StartDate.Date && model.EndDate.Date >= d.EndDate.Date) ||
                            model.StartDate.Date == d.StartDate.Date || model.StartDate.Date == d.EndDate.Date ||
                            model.EndDate.Date == d.StartDate.Date || model.EndDate.Date == d.EndDate.Date)
                .Any())
                {
                    customContext.AddFailure(ValidationMessages.VacationAlreadyExists);
                }
                if (appDbContext.DaysOff
                .Where(d => !d.IsCustomDaysOff || !d.UserDaysOff.Any() || !d.UserDaysOff.Where(x => x.UserId == session.CurrentUser.Id).Any())
                //start date in existing interval
                .Where(d => (model.StartDate.Date >= d.StartDate.Date && model.StartDate.Date <= d.EndDate.Date) ||
                            //end date in existing interval
                            (model.EndDate.Date >= d.StartDate.Date && model.EndDate.Date <= d.EndDate.Date))
                .Where(d => !d.IsCustomDaysOff || (d.IsCustomDaysOff && !d.UserDaysOff.Any()))
                .Any())
                {
                    customContext.AddFailure(ValidationMessages.VacationOnDaysOff);
                }
                if (appDbContext.DaysOff
                .Where(d => d.IsCustomDaysOff || d.UserDaysOff.Any() || d.UserDaysOff.Where(x => x.UserId == session.CurrentUser.Id).Any())
                //start date in existing interval
                .Where(d => (model.StartDate.Date >= d.StartDate.Date && model.StartDate.Date <= d.EndDate.Date) ||
                            //end date in existing interval
                            (model.EndDate.Date >= d.StartDate.Date && model.EndDate.Date <= d.EndDate.Date))
                .Where(d => (d.IsCustomDaysOff && d.UserDaysOff.Where(x => x.UserId == session.CurrentUser.Id).Any()))
                .Any())
                {
                    customContext.AddFailure("Vacations cannot start or end on custom days off.");
                }
            }
            if (appDbContext.User.Any(d => d.Id == session.CurrentUser.Id && d.RoleId == (int)Roles.Intern))
            {
                customContext.AddFailure("Interns do not have a vacation section.");
            }
        }

        public void ValidateEditVacationCommand(EditVacationCommand model, CustomContext customContext)
        {
            if (model.Id != 0)
            {
                if (!appDbContext.Vacation.Any(d => d.Id == model.Id))
                {
                    customContext.AddFailure(ValidationMessages.VacationNotInDatabase);
                }
                else
                {
                    if (!utilitiesValidation.HasVacationDaysleft(model.StartDate, model.EndDate, model.Id))
                    {
                        customContext.AddFailure(ValidationMessages.OutOfVacationDays);
                    }
                    var vacation = appDbContext.Vacation.FirstOrDefault(d => d.Id == model.Id);

                    if (vacation != null)
                    {
                        if (vacation.StatusId != (int)VacationsStatus.Pending)
                        {
                            customContext.AddFailure(ValidationMessages.VacationStatusNotPending);
                        }
                    }
                    if (model.StartDate != DateTime.MinValue)
                    {
                        //if (!Utilities.DateHasPassed(model.StartDate.ToLocalTime()))
                        //{
                        //    customContext.AddFailure(ValidationMessages.DateHasPassed);
                        //}
                        if (!Utilities.IsWeekend(model.StartDate.ToLocalTime()))
                        {
                            customContext.AddFailure(ValidationMessages.IsWeekend);
                        }
                        // If the attendances checker worked on vacation creation, I see no point in checking on edit
                        //if (!utilitiesValidation.CheckAttendance(model.StartDate))
                        //{
                        //    customContext.AddFailure(ValidationMessages.CheckAttendance);
                        //}
                    }
                    if (model.EndDate != DateTime.MinValue)
                    {
                        //if (!Utilities.DateHasPassed(model.EndDate.ToLocalTime()))
                        //{
                        //    customContext.AddFailure(ValidationMessages.DateHasPassed);
                        //}
                        if (!Utilities.IsWeekend(model.EndDate.ToLocalTime()))
                        {
                            customContext.AddFailure(ValidationMessages.IsWeekend);
                        }
                    }
                    if (model.EndDate != DateTime.MinValue && model.StartDate != DateTime.MinValue)
                    {
                        if (!utilitiesValidation.EndDateEarlierThanStartDateForVacation(model.StartDate, model.EndDate, model.Id))
                        {
                            customContext.AddFailure(ValidationMessages.EndDateAfterStartDate);
                        }
                        if (appDbContext.Vacation
                        .Where(d => d.UserId == session.CurrentUser.Id)
                        .Where(d => d.StatusId != (int)VacationsStatus.Rejected)
                        .Where(d => d.Id != model.Id)
                        //start date in existing interval
                        .Where(d => (model.StartDate.Date >= d.StartDate.Date && model.StartDate.Date <= d.EndDate.Date) ||
                                    //end date in existing interval
                                    (model.EndDate.Date >= d.StartDate.Date && model.EndDate.Date <= d.EndDate.Date) ||
                                    //requesting vacation incudes an existing interval
                                    (model.StartDate.Date <= d.StartDate.Date && model.EndDate.Date >= d.EndDate.Date) ||
                                    model.StartDate.Date == d.StartDate.Date || model.StartDate.Date == d.EndDate.Date ||
                                    model.EndDate.Date == d.StartDate.Date || model.EndDate.Date == d.EndDate.Date)
                        .Any())
                        {
                            customContext.AddFailure(ValidationMessages.VacationAlreadyExists);
                        }
                        if (appDbContext.DaysOff
                        .Where(d => !d.IsCustomDaysOff || !d.UserDaysOff.Any() || !d.UserDaysOff.Where(x => x.UserId == session.CurrentUser.Id).Any())
                        //start date in existing interval
                        .Where(d => (model.StartDate.Date >= d.StartDate.Date && model.StartDate.Date <= d.EndDate.Date) ||
                                    //end date in existing interval
                                    (model.EndDate.Date >= d.StartDate.Date && model.EndDate.Date <= d.EndDate.Date))
                        .Where(d => !d.IsCustomDaysOff || (d.IsCustomDaysOff && !d.UserDaysOff.Any()))
                        .Any())
                        {
                            customContext.AddFailure(ValidationMessages.VacationOnDaysOff);
                        }
                        if (appDbContext.DaysOff
                        .Where(d => d.IsCustomDaysOff || d.UserDaysOff.Any() || d.UserDaysOff.Where(x => x.UserId == session.CurrentUser.Id).Any())
                        //start date in existing interval
                        .Where(d => (model.StartDate.Date >= d.StartDate.Date && model.StartDate.Date <= d.EndDate.Date) ||
                                    //end date in existing interval
                                    (model.EndDate.Date >= d.StartDate.Date && model.EndDate.Date <= d.EndDate.Date))
                        .Where(d => (d.IsCustomDaysOff && d.UserDaysOff.Where(x => x.UserId == session.CurrentUser.Id).Any()))
                        .Any())
                        {
                            customContext.AddFailure("Vacations cannot start or end on custom days off.");
                        }
                    }
                    if (!appDbContext.Vacation
                       .Include(d => d.VacationRequestApproval)
                       .Where(d => d.Id == model.Id)
                       .SelectMany(d => d.VacationRequestApproval)
                       .All(d => d.Approved == null))
                    {
                        customContext.AddFailure(ValidationMessages.VacationAlreadyApproved);
                    }

                }
            }
        }

        public void ValidateDeleteVacationCommand(DeleteVacationCommand model, CustomContext customContext)
        {
            if (!appDbContext.Vacation.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.VacationNotInDatabase);
            }
            else
            {
                if (model.Id != 0)
                {
                    var vacation = appDbContext.Vacation.Where(d => d.Id == model.Id)
                       .First();

                    if (vacation.StatusId != (int)VacationsStatus.Pending )
                    {
                        customContext.AddFailure(ValidationMessages.VacationNotEligibleForDelete);
                    }
                }
            }
        }

        public void ValidateVacationQuery(VacationQuery model, CustomContext customContext)
        {
            if (model.UserId.HasValue)
            {
                if (!appDbContext.User.Any(d => d.Id == model.UserId))
                {
                    customContext.AddFailure(ValidationMessages.UserNotInDatabase);
                }
                if (appDbContext.User.Any(d => d.Id == model.UserId && d.RoleId == (int)Roles.Intern))
                {
                    customContext.AddFailure("Interns do not have a vacation section.");
                }
            }
        }
        #endregion

        #region DaysOff

        public void ValidateCreateDaysOffCommand(CreateDaysOffCommand model, CustomContext customContext)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                customContext.AddFailure("Day off name can not be empty.");
            }
            if (!model.CustomUsersIds.Any() || !model.ForEveryone)
            {
                if (utilitiesValidation.DayOffIntersectsExistingDayOff(model.StartDate.ToLocalTime(), model.EndDate.ToLocalTime(), 0))
                {
                    customContext.AddFailure(ValidationMessages.AlreadySigned);
                }
            }

            utilitiesValidation.CustomDayOffIntersectsExistingDayOff(model, customContext);

            if (model.StartDate == DateTime.MinValue)
            {
                customContext.AddFailure("Start date can not be empty.");
            }
            else
            {
                if (!Utilities.DateHasPassed(model.StartDate.ToLocalTime()))
                {
                    customContext.AddFailure(ValidationMessages.DateHasPassed);
                }
                if (!Utilities.IsWeekend(model.StartDate.ToLocalTime()))
                {
                    customContext.AddFailure(ValidationMessages.IsWeekend);
                }
            }
            if (model.EndDate == DateTime.MinValue)
            {
                customContext.AddFailure("End date can not be empty.");
            }
            else
            {
                if (!Utilities.DateHasPassed(model.EndDate.ToLocalTime()))
                {
                    customContext.AddFailure(ValidationMessages.DateHasPassed);
                }
                if (!Utilities.IsWeekend(model.EndDate.ToLocalTime()))
                {
                    customContext.AddFailure(ValidationMessages.IsWeekend);
                }
            }
            if (model.EndDate != DateTime.MinValue && model.StartDate != DateTime.MinValue)
            {
                if (!(model.EndDate.CompareTo(model.StartDate) >= 0))
                {
                    customContext.AddFailure(ValidationMessages.EndDateAfterStartDate);
                }
            }
            if (!Utilities.ValidateDaysOff(model))
            {
                customContext.AddFailure(ValidationMessages.DaysOffMultipleUsers);
            }
        }

        public void ValidateEditDaysOffCommand(EditDaysOffCommand model, CustomContext customContext)
        {
            if (!appDbContext.DaysOff.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.DayOffNotInDatabase);
            }
            else
            {
                if (string.IsNullOrEmpty(model.Name))
                {
                    customContext.AddFailure("Day off name can not be empty.");
                }
                if (!model.CustomUsersIds.Any() || !model.ForEveryone)
                {
                    if (utilitiesValidation.DayOffIntersectsExistingDayOff(model.StartDate.ToLocalTime(), model.EndDate.ToLocalTime(), model.Id))
                    {
                        customContext.AddFailure(ValidationMessages.AlreadySigned);
                    }
                }

                utilitiesValidation.CustomDayOffIntersectsExistingDayOff(model, customContext);

                if (model.StartDate != DateTime.MinValue)
                {
                    if (!Utilities.DateHasPassed(model.StartDate.ToLocalTime()))
                    {
                        customContext.AddFailure(ValidationMessages.DateHasPassed);
                    }
                    if (!Utilities.IsWeekend(model.StartDate.ToLocalTime()))
                    {
                        customContext.AddFailure(ValidationMessages.IsWeekend);
                    }
                }
                if (model.EndDate != DateTime.MinValue)
                {
                    if (!Utilities.DateHasPassed(model.EndDate.ToLocalTime()))
                    {
                        customContext.AddFailure(ValidationMessages.DateHasPassed);
                    }
                    if (!Utilities.IsWeekend(model.EndDate.ToLocalTime()))
                    {
                        customContext.AddFailure(ValidationMessages.IsWeekend);
                    }
                }
                if (!Utilities.ValidateDaysOff(model))
                {
                    customContext.AddFailure(ValidationMessages.DaysOffMultipleUsers);
                }
                if (!utilitiesValidation.EndDateEarlierThanStartDateForDaysOff(model.StartDate, model.EndDate, model.Id))
                {
                    customContext.AddFailure(ValidationMessages.EndDateAfterStartDate);
                }
            }
        }

        public void ValidateDeleteDaysOffCommand(DeleteDaysOffCommand model, CustomContext customContext)
        {
            if (!appDbContext.DaysOff.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.DayOffNotInDatabase);
            }
            else
            {
                if (!appDbContext.DaysOff.Where(d => d.Id == model.Id && d.StartDate > DateTime.Now).Any())
                {
                    customContext.AddFailure(ValidationMessages.DateHasPassed);
                }
            }
        }

        #endregion

        #region Vacation Requests

        public void ValidateEditVacationRequestApprovalFirstPriorityCommand(EditVacationRequestApprovalFirstPriorityCommand model, CustomContext customContext)
        {
            if (!appDbContext.Vacation.Any(d => d.Id == model.VacationId))
            {
                customContext.AddFailure(ValidationMessages.VacationNotInDatabase);
            }
            else
            {
                if (!utilitiesValidation.AlreadySignedFirstPriority(model.VacationId))
                {
                    customContext.AddFailure(ValidationMessages.AlreadySigned);
                }
            }
        }

        public void ValidateEditVacationRequestApprovalCommand(EditVacationRequestApprovalCommand model, CustomContext customContext)
        {
            if (!appDbContext.VacationRequestApproval.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.VacationRequestNotInDatabase);
            }
            else
            {
                if (!utilitiesValidation.AlreadySigned(model.Id))
                {
                    customContext.AddFailure(ValidationMessages.AlreadySigned);
                }
            }
        }

        public void ValidateEditVacationRequestApprovalAlreadySignedCommand(EditVacationRequestApprovalAlreadySignedCommand model, CustomContext customContext)
        {
            if (!appDbContext.VacationRequestApproval.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.VacationRequestNotInDatabase);
            }
        }

        #endregion

        #region Item

        public static void ValidateCreateItemCommand(CreateItemCommand model, CustomContext customContext)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                customContext.AddFailure("Item name can not be empty.");
            }
            if (string.IsNullOrEmpty(model.SerialNumber))
            {
                customContext.AddFailure("Serial number can not be empty.");
            }
            if (model.ItemTypeId == 0)
            {
                customContext.AddFailure(ValidationMessages.ItemCategoryEmpty);
            }
        }

        public void ValidateEditItemCommand(EditItemCommand model, CustomContext customContext)
        {
            if (!appDbContext.Item.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.ItemNotInDatabase);
            }
        }

        public void ValidateDeleteItemCommand(DeleteItemCommand model, CustomContext customContext)
        {
            if (!appDbContext.Item.Any(d => d.Id == model.Id))
            {
                customContext.AddFailure(ValidationMessages.ItemNotInDatabase);
            }
        }

        public void ValidateItemQuery(ItemQuery model, CustomContext customContext)
        {
            if (!appDbContext.Item.Any(d => d.Id == model.ItemId))
            {
                customContext.AddFailure(ValidationMessages.ItemNotInDatabase);
            }
        }
        #endregion
    }
}