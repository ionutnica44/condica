using CVU.Condica.Application.Interfaces;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Extensions;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{
    public class ResetPasswordSecurityCodeComamnd : IRequest<Unit>
    {
        public string Email { get; set; }
    }

    public class ResetPasswordSecurityCodeComamndHandler : RequestHandler<ResetPasswordSecurityCodeComamnd, Unit>
    {
        private readonly IEmailService emailService;
        private readonly IConfiguration configuration;

        public ResetPasswordSecurityCodeComamndHandler(IServiceProvider serviceProvider, IEmailService emailService, IConfiguration configuration) : base(serviceProvider)
        {
            this.emailService = emailService;
            this.configuration = configuration;
        }

        public override async Task<Unit> Handle(ResetPasswordSecurityCodeComamnd request, CancellationToken cancellationToken)
        {
            var userDb = appDbContext.User.First(d => d.Email == request.Email);

            userDb.SecurityCode = StringExtensions.GenerateRandomString(30);
            userDb.SecurityCodeExpiresAt = DateTime.Now.AddMinutes(15);

            appDbContext.SaveChanges();

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\ResetPassword.txt");
#else
            var template = File.ReadAllText("ResetPassword.txt");
#endif

            var hostName = $"{configuration["BaseUrl"]}/auth/change-password?SecurityCode={userDb.SecurityCode}";

            template = template.Replace("{BaseUrl}", hostName)
                .Replace("{FirstName}", userDb.FirstName);

            emailService.SendAsync(subject: "Reset Your Password",
                body: template,
                from: "Do Not Reply",
                recipients: new List<string> { userDb.Email });

            return Unit.Value;
        }
    }
    public class ResetPasswordSecurityCodeComamndValidator : AbstractValidator<ResetPasswordSecurityCodeComamnd>
    {
        public ResetPasswordSecurityCodeComamndValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateResetPasswordSecurityCodeComamnd);
        }
    }
}
