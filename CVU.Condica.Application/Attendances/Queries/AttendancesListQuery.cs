﻿using CVU.Condica.Application.Attendances.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using CVU.Condica.Persistence.ReferenceData;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Queries
{
    public class AttendancesListQuery : IRequest<IEnumerable<UserAttendancesDto>>
    {
        public AttendancesListQuery(bool currentUser)
        {
            CurrentUser = currentUser;
        }
        public AttendancesListQuery()
        {

        }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int[] UserIds { get; set; }
        internal bool CurrentUser { get; set; }
    }

    public class AttendancesListQueryHandler : RequestHandler<AttendancesListQuery, IEnumerable<UserAttendancesDto>>
    {
        protected readonly IPaginationService paginationService;
        public AttendancesListQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<IEnumerable<UserAttendancesDto>> Handle(AttendancesListQuery request, CancellationToken cancellationToken)
        {

            var sw = new Stopwatch();
            sw.Start();
            var usersDb = appDbContext.User.Include(d => d.Vacations).ThenInclude(d => d.VacationStatus).AsQueryable();

            if (request.UserIds != null)
            {
                usersDb = usersDb.Where(d => request.UserIds.Contains(d.Id));
            }
            else if (request.CurrentUser)
            {
                usersDb = usersDb.Where(d => d.Id == CurrentUser.Id);
            }

            var users = usersDb.Select(w => new
            {
                UserId = w.Id,
                UserEmail = w.Email,
                FirstName = w.FirstName,
                LastName = w.LastName,
                CreatedAt = w.CreatedAt,
                Attendances = w.Attendances.Where(x => x.ArrivalTime > request.StartDate.Value.Date && x.ArrivalTime < request.EndDate).ToList(),
                //Custom days off strict pt userul curent
                CustomDaysOff = w.UserDaysOff.Select(d => d.DaysOff),
                Vacations = w.Vacations
                .Where(d => (d.StartDate.Month == request.StartDate.Value.ToLocalTime().Month) || (d.EndDate.Month == request.StartDate.Value.ToLocalTime().Month))
                .Where(d => d.StatusId != (int)VacationsStatus.Rejected)
                .ToList()
            }).ToList();

            //Zile libere legale + custom days off pt toata lumea!
            var daysOff = appDbContext.DaysOff
                .Include(d => d.UserDaysOff)
                .Where(d => (d.StartDate >= request.StartDate.Value) || (d.EndDate >= request.StartDate.Value))
                .Where(d => !d.IsCustomDaysOff || (d.IsCustomDaysOff && !d.UserDaysOff.Any()))
                .ToList();

            var userAttendances = users.Select(d => new UserAttendancesDto
            {
                UserId = d.UserId,
                UserEmail = d.UserEmail,
                LastName = d.LastName,
                FirstName = d.FirstName,
                CreatedAt = d.CreatedAt,
                Attendances = d.Attendances.Select(a => new AttendanceDto
                {
                    ArrivalTime = a.ArrivalTime,
                    DepartureTime = a.DepartureTime,
                    FromHome = a.FromHome,
                    Status = "Prezent",
                    AttendanceDuration = a.DepartureTime.HasValue ? (a.DepartureTime - a.ArrivalTime).Value.ToString(@"hh\:mm") : null,
                    Id = a.Id,
                    UserId = d.UserId
                })
                .Concat(d.Vacations
                    .SelectMany(v => EachDay(v.StartDate, v.EndDate))
                    .Where(b => b >= request.StartDate.Value.Date && b <= request.EndDate.Value.Date)
                    //.Where(b => !daysOff.Any(x => x.StartDate <= b && x.EndDate >= b))
                    .Select(w => new AttendanceDto
                    {
                        ArrivalTime = w,
                        Status = "Concediu",
                        FromHome = false,
                        UserId = d.UserId
                    }))
                .Concat(d.CustomDaysOff
                    .SelectMany(x => EachDay(x.StartDate, x.EndDate))
                    .Where(x => x >= request.StartDate && x <= request.EndDate)
                    .Where(b => !daysOff.Any(x => x.StartDate <= b && x.EndDate >= b))
                    .Select(y => new AttendanceDto
                    {
                        ArrivalTime = y,
                        Status = $"Custom Day Off - { d.CustomDaysOff.Where(x => x.StartDate.Date <= y && x.EndDate.Date >= y).First().Name }",
                        FromHome = false,
                        UserId = d.UserId
                    }))
                .Concat(daysOff
                    //.Where(a => a.IsCustomDaysOff == false)
                    .SelectMany(x => EachDay(x.StartDate, x.EndDate))
                    .Where(x => x >= request.StartDate && x <= request.EndDate)
                    .Select(y => new AttendanceDto
                    {
                        ArrivalTime = y,
                        Status = $"Day Off - { daysOff.Where(x => x.StartDate.Date <= y && x.EndDate.Date >= y).First().Name }",
                        FromHome = false,
                        UserId = d.UserId
                    }))
                   .OrderByDescending(z => z.ArrivalTime)
            })
                .ToList();

            return userAttendances;

        }
        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            {
                yield return day;

            }
        }
    }

    public class AttendancesListQueryAuthorization : AuthorizationSetup<AttendancesListQuery>
    {
        public AttendancesListQueryAuthorization()
        {
        }
    }
}
