using System;

namespace CVU.Condica.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
