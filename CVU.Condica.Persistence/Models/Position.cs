﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public class Position
    {
        public Position()
        {
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
    public class PositionConfiguration : IEntityTypeConfiguration<Position>
    {
        public void Configure(EntityTypeBuilder<Position> builder)
        {
            builder.HasData(
                new Position() { Id = 1, Name = "programator" },
                new Position() { Id = 2, Name = "ajutor programator" },
                new Position() { Id = 3, Name = "director financiar" },
                new Position() { Id = 4, Name = "inspector in domeniul SSM" },
                new Position() { Id = 5, Name = "manager sistem informatic" },
                new Position() { Id = 6, Name = "functionar administrativ" },
                new Position() { Id = 7, Name = "programator de sistem informatic" },
                new Position() { Id = 8, Name = "femeie de serviciu" },
                new Position() { Id = 9, Name = "intern" }
                );
        }
    }
}