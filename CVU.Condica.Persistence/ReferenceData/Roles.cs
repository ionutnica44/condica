namespace CVU.Condica.Persistance.ReferenceData
{
    public enum Roles
    {
        Administrator = 1,
        Employee = 2,
        Intern = 3
    }
}
