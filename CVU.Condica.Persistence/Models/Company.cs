﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public class Company
    {
        public Company()
        {
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }

    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasData(
                new Company() { Id = 1, Name = "CVU Intelligence" },
                new Company() { Id = 2, Name = "ByteSeven Studios" },
                new Company() { Id = 3, Name = "PFA" },
                new Company() { Id = 4, Name = "Internship" }
                );
        }
    }
}
