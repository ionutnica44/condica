﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.VacationRequestApprovals.Commands
{
    public class EditVacationRequestApprovalAlreadySignedCommand : IRequest<Unit>
    {
        public int Id { get; set; }
    }

    public class EditVacationRequestApprovalAlreadySignedCommandHandler : RequestHandler<EditVacationRequestApprovalAlreadySignedCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public EditVacationRequestApprovalAlreadySignedCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(EditVacationRequestApprovalAlreadySignedCommand request, CancellationToken cancellationToken)
        {
            var vacationRequest = appDbContext.VacationRequestApproval
                .Include(d => d.SigningUser)
                .Include(d => d.Vacation)
                .FirstOrDefault(d => d.Id == request.Id);

            vacationRequest.Approved = null;
            vacationRequest.Comment = null;
            vacationRequest.Vacation.StatusId = (int)VacationsStatus.Pending;

            var currentUserSignPriority = appDbContext.User.Where(d => d.Id == CurrentUser.Id).Select(d => d.SignPriorityId).FirstOrDefault();

            var vacationRequestFurthers = appDbContext.VacationRequestApproval
                    .Include(d => d.SigningUser)
                    .Where(d => d.VacationId == vacationRequest.VacationId && d.SigningUserId != CurrentUser.Id && d.SigningUser.SignPriorityId < currentUserSignPriority);


            if (vacationRequestFurthers.Count() != 0)
            {
                foreach (var vacationRequestFurther in vacationRequestFurthers)
                {
                    if (vacationRequestFurther.SigningUserId >= currentUserSignPriority - 1)
                    {
                        if (vacationRequest.Approved.HasValue && !vacationRequestFurther.Approved.Value)
                        {
                            vacationService.SendVacationStatusUpdated(vacationRequest.VacationId, vacationRequestFurther.SigningUserId, vacationRequestFurther.SigningUser.FirstName, vacationRequest.SigningUser.FirstName);
                        }
                    }
                    appDbContext.VacationRequestApproval.Remove(vacationRequestFurther);
                }
            }
            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class EditVacationRequestApprovalAlreadySignedCommandValidator : AbstractValidator<EditVacationRequestApprovalAlreadySignedCommand>
    {
        public EditVacationRequestApprovalAlreadySignedCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditVacationRequestApprovalAlreadySignedCommand);
        }
    }

    public class EditVacationRequestApprovalAlreadySignedCommandAuthorization : AuthorizationSetup<EditVacationRequestApprovalAlreadySignedCommand>
    {
        public EditVacationRequestApprovalAlreadySignedCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
            NeedsRole((int)Roles.Employee);
        }
    }
}
