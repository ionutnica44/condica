﻿namespace CVU.Condica.Application.Attendances.Models
{
    public class MmUserDto
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
