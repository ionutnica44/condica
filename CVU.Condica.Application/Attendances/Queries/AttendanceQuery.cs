﻿using CVU.Condica.Application.Attendances.Models;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Queries
{
    public class AttendanceQuery : IRequest<AttendanceDto>
    {
        public int AttendanceId { get; set; }
    }

    public class AttendanceQueryHandler : RequestHandler<AttendanceQuery, AttendanceDto>
    {
        public AttendanceQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<AttendanceDto> Handle(AttendanceQuery request, CancellationToken cancellationToken)
        {
            var attendance = appDbContext.Attendance
                .FirstOrDefault(a => a.Id == request.AttendanceId);

            return AttendanceDto.Create(attendance);
        }
    }

    public class AttendanceQueryValidator : AbstractValidator<AttendanceQuery>
    {
        public AttendanceQueryValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateAttendanceQuery);
        }
    }

    public class AttendancesQueryAuthorization : AuthorizationSetup<AttendanceQuery>
    {
        public AttendancesQueryAuthorization()
        {
        }
    }
}
