using Microsoft.EntityFrameworkCore;
using CVU.Condica.Persistence.Infrastructure;
using System;
using CVU.Condica.Persistence.Models;

namespace CVU.Condica.Persistence
{
    public class AppDbContextFactory : DesignTimeDbContextFactoryBase<AppDbContext>
    {
        protected override AppDbContext CreateNewInstance(DbContextOptions<AppDbContext> options)
        {
            return new AppDbContext(options);
        }
    }
}
