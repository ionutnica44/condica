﻿using CVU.Condica.Application.Interfaces;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Extensions;
using CVU.Condica.Persistance.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{
    public class ResendActivationCommand : IRequest<Unit>
    {
        public int UserId { get; set; }

    }

    public class ResendActivationCommandHandler : RequestHandler<ResendActivationCommand, Unit>
    {
        IEmailService emailService;
        IConfiguration configuration;

        public ResendActivationCommandHandler(
            IServiceProvider serviceProvider,
            IEmailService emailService,
            IConfiguration configuration) : base(serviceProvider)
        {
            this.emailService = emailService;
            this.configuration = configuration;
        }

        public override async Task<Unit> Handle(ResendActivationCommand request, CancellationToken cancellationToken)
        {
            var userDb = appDbContext.User.First(d => d.Id == request.UserId);

            userDb.SecurityCode = StringExtensions.GenerateRandomString(12);
            userDb.SecurityCodeExpiresAt = DateTime.Now.AddMonths(15);
            userDb.IsActivated = false;

            appDbContext.SaveChanges();

#if DEBUG
            string template = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\UserRegister.txt");
#else
            var template = File.ReadAllText("UserRegister.txt");
#endif

            var hostName = $"{configuration["BaseUrl"]}/auth/confirm-account?SecurityCode={userDb.SecurityCode}";

            template = template.Replace("{BaseUrl}", hostName)
                .Replace("{FirstName}", userDb.FirstName);

            emailService.SendAsync(subject: "New account - Reactivation",
                body: template,
                from: "Do Not Reply",
                recipients: new List<string> { userDb.Email });

            return Unit.Value;
        }
    }

    public class ResendActivationCommandValidator : AbstractValidator<ResendActivationCommand>
    {
        public ResendActivationCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateResendActivationCommand);
        }
    }

    public class ResendActivationCommandAuthorization : AuthorizationSetup<ResendActivationCommand>
    {
        public ResendActivationCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
