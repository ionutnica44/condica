using System;

namespace CVU.Condica.Persistence.Models
{
    public partial class Logs
    {
        public int Id { get; set; }
        public string Exception { get; set; }
        public string Level { get; set; }
        public string LogEvent { get; set; }
        public string Message { get; set; }
        public string MessageTemplate { get; set; }
        public string Properties { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
    }
}
