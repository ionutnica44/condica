﻿using System;

namespace CVU.Condica.Application.Attendances
{
    public interface IAttendancesService
    {
        public byte[] AttendancesReport(DateTime dateTime);
    }
}
