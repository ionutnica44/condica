using System;
using System.Linq;
using System.Linq.Expressions;

namespace CVU.Condica.Common.Pagination
{
    public interface IPaginationService
    {
        PaginatedList<T> PaginatedResults<T>(IQueryable<T> list, PaginatedQueryParameter query, Expression<Func<T, string>>[] membersToSearch = null);
        PaginatedList<TSource, TDestination> PaginatedResults<TSource, TDestination>(IQueryable<TSource> list, PaginatedQueryParameter query, Expression<Func<TSource, string>>[] membersToSearch = null);
    }
}