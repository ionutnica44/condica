﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CVU.Condica.Persistence.Migrations
{
    public partial class UpdateVacationRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "VacationRequestApproval",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                table: "VacationRequestApproval");
        }
    }
}
