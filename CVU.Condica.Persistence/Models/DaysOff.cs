﻿using System;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public class DaysOff
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumberOfDays { get; set; }
        public bool IsCustomDaysOff { get; set; }
        public virtual ICollection<UserDaysOff> UserDaysOff { get; set; }
    }
}
