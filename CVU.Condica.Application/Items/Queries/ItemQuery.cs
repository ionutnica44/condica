﻿using CVU.Condica.Application.Items.Models;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Items.Queries
{
    public class ItemQuery : IRequest<ItemDto>
    {
        public int ItemId { get; set; }
    }

    public class ItemQueryHandler : RequestHandler<ItemQuery, ItemDto>
    {
        public ItemQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<ItemDto> Handle(ItemQuery request, CancellationToken cancellationToken)
        {
            var item = appDbContext.Item
                .Include(d => d.ItemType)
                .Include(d => d.User)
                .Select(ItemDto.Projection)
                .First(d => d.Id == request.ItemId);

            return item;
        }
    }

    public class ItemQueryValidator : AbstractValidator<ItemQuery>
    {
        public ItemQueryValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateItemQuery);
        }
    }

    public class ItemQueryAuthorization : AuthorizationSetup<ItemQuery>
    {
        public ItemQueryAuthorization()
        {
        }
    }
}
