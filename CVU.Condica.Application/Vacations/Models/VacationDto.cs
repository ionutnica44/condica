﻿using CVU.Condica.Persistence.ReferenceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Vacations.Models
{
    public class VacationDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumberOfDays { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public List<string> ApprovedBy { get; set; }
        public List<string> RejectedBy { get; set; }
        public List<string> RemainingToApprove { get; set; }

        public static Expression<Func<Persistence.Models.Vacation, VacationDto>> Projection
        {
            get
            {
                return p => new VacationDto()
                {
                    Id = p.Id,
                    UserId = p.UserId,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate,
                    NumberOfDays = p.NumberOfDays,
                    StatusId = p.StatusId,
                    Status = p.VacationStatus.Status,
                    ApprovedBy = p.VacationRequestApproval.Where(d => d.Approved == true)
                                    .Select(d => " " + d.SigningUser.FirstName + " " + d.SigningUser.LastName)
                                    .ToList(),
                    RejectedBy = p.VacationRequestApproval.Where(d => d.Approved == false)
                                    .Select(d => " " + d.SigningUser.FirstName + " " + d.SigningUser.LastName)
                                    .ToList(),
                    RemainingToApprove = p.User.SigningUsers.Where(d => !p.VacationRequestApproval.Where(x => x.Approved != null).Any(x => x.SigningUserId == d.SigningUserId))
                                    .Where(d => !p.VacationRequestApproval.Any(x => x.SigningUser.SignPriorityId == (int)SignPriorities.First && x.Approved != null))
                                    .Select(d => " " + d.Signing.FirstName + " " + d.Signing.LastName)
                                    .ToList()
                };
            }
        }

        public static VacationDto Create(Persistence.Models.Vacation Vacation)
        {
            return Projection.Compile().Invoke(Vacation);
        }

        private class ListHelper<T>
        {
            public static bool ContainsAllItems(List<T> a, List<T> b)
            {
                return !b.Except(a).Any();
            }
        }
    }
}
