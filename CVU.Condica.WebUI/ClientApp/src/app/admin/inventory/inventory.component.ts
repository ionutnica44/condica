import { Component, OnInit, ViewChild } from '@angular/core';
import {  ItemClient, ItemDto, ListUsersQuery, SwaggerResponse, DeleteItemCommand, Unit} from 'app/client-api';
import { InventoryModalComponent } from './inventory-modal/inventory-modal.component';
import { RequestOptions } from 'services/requestOptions';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html'
})
export class InventoryComponent implements OnInit {
  allItems: ItemDto[];
  page: RequestOptions<ListUsersQuery> = new RequestOptions<ListUsersQuery>();
  deleteComand: DeleteItemCommand;
  itemToDelete: number;
  private deleteModal: any;
  @ViewChild(InventoryModalComponent) inventoryChild: InventoryModalComponent;
  constructor(private _inventory: ItemClient, private _toastr: ToastrService) { 
    this.deleteComand = new DeleteItemCommand();
  }

  ngOnInit() {
    this.getItems();
    this.deleteModal = ($('.delete-item') as any).modal({ closable: false });
  }
  getItems(){
    this._inventory.listItems(this.page.filter)
    .subscribe((response: SwaggerResponse<ItemDto[]>) =>{
      this.allItems = response.result;
      console.log("all items:", this.allItems);
    });
  }
  addItem(){
    this.inventoryChild.addItem();
  }
  refreshItemsList(event) {
    if (event === "refresh") {
      this.getItems();
    }
  }
  deleteItem(item: ItemDto){
    if(item){
      this.itemToDelete = item.id;
      this.deleteComand.id = item.id;
      this.deleteModal.modal('show');
    }
  }
  delete(){
      this._inventory.deleteItem(this.itemToDelete, this.deleteComand).subscribe((response: SwaggerResponse<Unit>) => {
        this._toastr.success('Successfully deleted item');
        this.deleteModal.modal('hide');
        this.getItems();
      }, (e) => {
        this.deleteModal.modal('show');
      });
    }
    onClickCancel(){
      this.itemToDelete = 0;
      this.deleteModal.modal('hide');
    }
  editItem(item: ItemDto){
    this.inventoryChild.editItem(item);
  }
}
