using CVU.Condica.Application.Exceptions;
using CVU.Utilities.Email;
using CVU.Utilities.ErrorHandling;
using CVU.Utilities.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Net;

namespace CVU.Utilities.Filters
{
    public class CustomExceptionFilter : IExceptionFilter
    {
        private readonly EmailService emailService;

        public CustomExceptionFilter(EmailService emailService)
        {
            this.emailService = emailService;
        }

        public virtual void OnException(ExceptionContext context)
        {
            HttpStatusCode status;
            string message;
            object validation = null;

            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                message = "Unauthorized Access.";
                status = HttpStatusCode.Unauthorized;
            }
            else if (context.Exception is ValidationException)
            {
                status = HttpStatusCode.BadRequest;
                validation = ((ValidationException)context.Exception).Failures;
                message = "Validation failed.";
                status = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                message = "A server error occurred.";
                status = HttpStatusCode.NotImplemented;
            }
            else if (exceptionType == typeof(AppException))
            {
                message = context.Exception.Message;
                status = ((AppException)context.Exception).HttpStatusCode;
            }
            else
            {
                message = "A server error occurred.";
                status = HttpStatusCode.InternalServerError;

                emailService.AddSubject("Internal Error");
                emailService.AddBody(EmailTableFormat.ErrorToString(context.HttpContext, context.Exception, string.Empty));
                emailService.Send();
            }

            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)status;
            response.ContentType = "application/json";

            var responseObj = new ErrorResponse
            {
                Message = message,
                Validation = validation,
#if DEBUG
                Detail = $"{context.Exception?.Message} {context.Exception?.StackTrace} {context.Exception?.InnerException?.Message} {context.Exception?.InnerException?.StackTrace}"
#endif
            };

            response.WriteAsync(JsonConvert.SerializeObject(responseObj));
        }
    }
}
