﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.VacationRequestApprovals.Commands
{
    public class EditVacationRequestApprovalFirstPriorityCommand : IRequest<Unit>
    {
        public int VacationId { get; set; }
        public bool? Approved { get; set; }
        public string Comment { get; set; }
    }

    public class EditVacationRequestApprovalFirstPriorityCommandHandler : RequestHandler<EditVacationRequestApprovalFirstPriorityCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public EditVacationRequestApprovalFirstPriorityCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(EditVacationRequestApprovalFirstPriorityCommand request, CancellationToken cancellationToken)
        {
            var vacation = appDbContext.Vacation
                   .Include(d => d.VacationRequestApproval)
                   .ThenInclude(d => d.SigningUser)
                   .Include(d => d.User)
                   .FirstOrDefault(d => d.Id == request.VacationId);

            if (!appDbContext.VacationRequestApproval.Any(d => d.SigningUserId == CurrentUser.Id && d.VacationId == request.VacationId))
            {
                var vacationApprovalRequest = new VacationRequestApproval()
                {
                    VacationId = (int)request.VacationId,
                    Approved = request.Approved,
                    SigningUserId = CurrentUser.Id
                };

                vacationApprovalRequest.Comment = request.Comment;

                appDbContext.VacationRequestApproval.Add(vacationApprovalRequest);
                appDbContext.SaveChanges();
            }

            if (vacation.VacationRequestApproval.Any(d => d.SigningUserId == CurrentUser.Id && d.Approved.Value))
            {
                vacation.StatusId = (int)VacationsStatus.Approved;
                vacationService.SendVacationStatusEmail(vacation.Id, true);
            }
            else
            {
                vacation.StatusId = (int)VacationsStatus.Rejected;
                vacationService.SendVacationRequestEmail(vacation.Id);
            }

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class EditVacationRequestApprovalFirstPriorityCommandValidator : AbstractValidator<EditVacationRequestApprovalFirstPriorityCommand>
    {
        public EditVacationRequestApprovalFirstPriorityCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditVacationRequestApprovalFirstPriorityCommand);
        }
    }

    public class EditVacationRequestApprovalFirstPriorityCommandAuthorization : AuthorizationSetup<EditVacationRequestApprovalFirstPriorityCommand>
    {
        public EditVacationRequestApprovalFirstPriorityCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
