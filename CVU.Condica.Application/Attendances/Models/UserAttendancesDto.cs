﻿using System;
using System.Collections.Generic;

namespace CVU.Condica.Application.Attendances.Models
{
    public class UserAttendancesDto
    {
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public IEnumerable<AttendanceDto> Attendances { get; set; }
    }
}
