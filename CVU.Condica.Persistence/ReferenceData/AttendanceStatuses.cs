﻿namespace CVU.Condica.Persistence.ReferenceData
{
    public enum AttendanceStatuses
    {
        Present = 1,
        Absent = 2,
        Concediu = 3,
        Liber = 4
    }
}
