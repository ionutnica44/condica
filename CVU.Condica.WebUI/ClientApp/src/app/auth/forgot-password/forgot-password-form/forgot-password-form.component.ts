import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserClient, ResetPasswordSecurityCodeComamnd } from 'app/client-api';


@Component({
  selector: 'forgot-password-form',
  templateUrl: './forgot-password-form.component.html',
})
export class ForgotPasswordFormComponent implements OnInit {
  forgotForm = null;
  forgotModel: ResetPasswordSecurityCodeComamnd;
  loggingIn: boolean;
  constructor(private _fb: FormBuilder, private _account: UserClient, private _toastr: ToastrService, private _router: Router) {
    this.forgotForm = _fb.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
    }), {
        updateOn: 'blur'
      };
    this.forgotModel = new ResetPasswordSecurityCodeComamnd();
    this.loggingIn = false;
  }

  ngOnInit() {
  }


  forgotSubmit() {
    this.loggingIn = true;
    this._account.forgotPassword(this.forgotModel).subscribe((response) => {
      this._toastr.info('A verification link has been sent to your email account', 'Email');
      this.loggingIn = false;
      setTimeout(() => {
        this._router.navigateByUrl('/auth/login');
      }, 300);
    }, (e) => {
      this.loggingIn = false;
    });
  }
}
