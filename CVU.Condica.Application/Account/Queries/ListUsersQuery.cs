﻿using CVU.Condica.Application.Account.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using CVU.Condica.Persistence.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Queries
{
    public class ListUsersQuery : PaginatedQueryParameter, IRequest<PaginatedModel<UserDto>>
    {

    }
    public class ListUsersQueryHandler : RequestHandler<ListUsersQuery, PaginatedModel<UserDto>>
    {
        private readonly IPaginationService paginationService;
        public ListUsersQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<PaginatedModel<UserDto>> Handle(ListUsersQuery request, CancellationToken cancellationToken)
        {
            var users = appDbContext.User
                .Include(d => d.Company)
                .Include(d => d.Position)
                .Include(d => d.Role)
                .Include(d => d.SigningUsers)
                .ThenInclude(d => d.Signing)
                .Include(d => d.UserDaysOff)
                .Include(d => d.SignPriority)
                .Include(d => d.Attendances)
                .AsQueryable();
            var paginatedResponse = paginationService.PaginatedResults<User, UserDto>(users, request);

            return new PaginatedModel<UserDto>(paginatedResponse);

        }
    }

    public class ListUsersQueryAuthorization : AuthorizationSetup<ListUsersQuery>
    {
        public ListUsersQueryAuthorization()
        {
        }
    }
}
