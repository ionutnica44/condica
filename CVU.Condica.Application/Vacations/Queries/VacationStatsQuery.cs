﻿using CVU.Condica.Application.Vacations.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Vacations.Queries
{
    public class VacationStatsQuery : IRequest<IEnumerable<VacationStatsDto>>
    {
    }

    public class VacationStatsQueryHandler : IRequestHandler<VacationStatsQuery, IEnumerable<VacationStatsDto>>
    {
        private readonly AppDbContext appDbContext;
        public VacationStatsQueryHandler(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public async Task<IEnumerable<VacationStatsDto>> Handle(VacationStatsQuery request, CancellationToken cancellationToken)
        {
            var vacations = appDbContext.User
                .Include(d => d.VacationUserManagement)
                .Select(d => new VacationStatsDto
                {
                    UserName = $"{d.FirstName} {d.LastName}",
                    Vacations = d.VacationUserManagement
                }).AsEnumerable();
            return vacations;
        }
    }
    public class VacationStatsQueryAuthorization : AuthorizationSetup<VacationStatsQuery>
    {
        public VacationStatsQueryAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }

}
