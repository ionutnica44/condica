import { EditProfileCommand, ResendActivationCommand } from './../../../client-api';
import { Component, OnInit, AfterViewInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { UserClient, RegisterCommand, SwaggerResponse, UserDto, ReferenceClient } from 'app/client-api';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';
import * as clone from 'lodash/clone';

import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { findObjectDiff } from 'app/shared/utilities/findFormDiff';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html'
})

export class EditComponent implements OnInit, AfterViewInit, OnDestroy {
  disableSave = false;
  user: UserDto;
  registerModel: RegisterCommand;
  userRegisterForm = null;
  isEdit: boolean;
  idResendActivation: ResendActivationCommand;
  loggingIn: boolean;
  resended = false;
  private editModal: any;
  private roleDropdown: any;
  private companyDropdown: any;
  private positionDropdown: any;
  private signingUsersDropdown: any;

  private originalSigningUsers: { [id: number]: string };

  roles: any = [];
  companies: any = [];
  positions: any = [];
  signingUsers: any;
  selectedSigningUsers: any;
  clonedUser: any = <EditProfileCommand>{};
  userOld: any = {} as EditProfileCommand;
  @Output() newEvent: EventEmitter<any> = new EventEmitter();
  constructor(private _account: UserClient, private _referenceClient: ReferenceClient, private _toastr: ToastrService, private _fb: FormBuilder) {
    this.registerModel = new RegisterCommand();
    this.userRegisterForm = _fb.group({
      firstName: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern('[a-zA-Z ]*')])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern('[a-zA-Z ]*')])],
      phoneNumber: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.pattern("^[0-9]{10}$")])],
      email: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(3), Validators.maxLength(50), Validators.pattern("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})")])],
      roleId: ['', Validators.compose([])],
      companyId: ['', Validators.compose([])],
      positionId: ['', Validators.compose([])],
      isBlocked: [''],
      canSign: [''],
      selectedSigningUsers: ['']
    });
    this.disableSave = false;
    this.resended = false;
    this.loggingIn = false;
  }

  ngOnInit() { }

  ngAfterViewInit() {

    this.getRoles();
    this.getCompanies();
    this.getPositions();
    this.getSigningUsers();

    setTimeout(() => {

      this.roleDropdown = ($('.role-dropdown') as any).dropdown();
      this.companyDropdown = ($('.company-dropdown') as any).dropdown();
      this.positionDropdown = ($('.position-dropdown') as any).dropdown();

      this.signingUsersDropdown =
        ($('.signingUsers-dropdown') as any)
          .dropdown({
            onAdd: this.onAdd.bind(this),
            onRemove: this.onRemove.bind(this)
          });
      this.editModal = ($('.userEdit') as any).modal({ closable: false });
    }, 500);
  }

  addUser() {
    this.clonedUser = new RegisterCommand();
    this.roleDropdown.dropdown('clear');
    this.companyDropdown.dropdown('clear');
    this.positionDropdown.dropdown('clear');
    this.isEdit = false;
    this.signingUsers = _.cloneDeep(this.originalSigningUsers);
    this.signingUsersDropdown.dropdown('clear');
    console.log('Signing users:', this.signingUsers);
    setTimeout(() => this.signingUsersDropdown.dropdown('set selected', ['marius dumitrascu', 'iustinian nita']), 200)

    this.editModal.modal('show');
  }

  editUser(user: UserDto) {
    this.resended = false;
    if (user) {
      this.isEdit = true;
      this.signingUsers = _.cloneDeep(this.originalSigningUsers);
      (<HTMLInputElement>document.getElementById("email")).disabled = true;
      this._account.getUser(user.userId)
        .subscribe((response: SwaggerResponse<UserDto>) => {
          this.clonedUser = response.result;
          this.roleDropdown.dropdown('set selected', this.clonedUser.roleId);
          this.companyDropdown.dropdown('set selected', this.clonedUser.companyId);
          this.positionDropdown.dropdown('set selected', this.clonedUser.positionId);
          console.log('signing user: ', typeof this.signingUsers)
          console.log('nume si prenume: ', this.clonedUser.firstName + " " + this.clonedUser.lastName)
          let idDelete = _.invert(this.signingUsers)[this.clonedUser.firstName + " " + this.clonedUser.lastName];
          if (idDelete != undefined) {
            delete (this.signingUsers[idDelete])
          }
          this.signingUsersDropdown.dropdown('set selected', Object.values(this.clonedUser.signingUsers));
          this.editModal.modal('show');
          this.userOld = _.clone(this.clonedUser);
          console.log(' this.userOld', this.userOld);

        });
    }
  }

  getRoles() {
    this._referenceClient.getRoles().subscribe((response: SwaggerResponse<any>) => {
      this.roles = response.result;
    });
  }

  getCompanies() {
    this._referenceClient.getCompanies().subscribe((response: SwaggerResponse<any>) => {
      this.companies = response.result;
    });
  }

  getPositions() {
    this._referenceClient.getPositions().subscribe((response: SwaggerResponse<any>) => {
      this.positions = response.result;
    });
  }

  getSigningUsers() {
    this._referenceClient.getSigningUsers()
      .subscribe((response: SwaggerResponse<any>) => {
        this.originalSigningUsers = response.result;
      });
  }

  handleChanges(event, field) {
    this.clonedUser[field] = event.target.value;
  }

  onAdd(addedValue, addedText, $addedChoice) {
    this.selectedSigningUsers = this.selectedSigningUsers || {};
    const id = _.invert(this.signingUsers)[addedText];
    this.selectedSigningUsers[id] = addedText;
  }
  onRemove(removedValue, removedText, $removedChoice) {
    this.selectedSigningUsers = this.selectedSigningUsers || {};
    const id = _.invert(this.signingUsers)[removedText];
    delete this.selectedSigningUsers[id];
  }

  save() {
    const method: any = this.clonedUser.userId ? this._account['editUser'].bind(this._account) : this._account['register'].bind(this._account);
    const message = this.clonedUser.userId ? 'User updated' : 'Registered';
    this.disableSave = true;
    this.loggingIn = true;
    this.clonedUser.signingUsers = this.selectedSigningUsers;
    const diff = findObjectDiff(this.userOld, this.clonedUser);
    const dataToSend = new EditProfileCommand(diff);
    dataToSend.userId = this.clonedUser.userId;
    const userAction: any = this.clonedUser.userId ? dataToSend : this.clonedUser;
    method(userAction).subscribe((response: SwaggerResponse<any>) => {
      this._toastr.success(message, 'Succesfully');
      this.editModal.modal('hide');
      this.loggingIn = false;
      this.newEvent.emit(null);
      setTimeout(() => {
        this.resetForm();
        this.disableSave = false;
      }, 100);
    }, (error) => {
      console.log('Error:', error);
      this.editModal.modal('show');
      this.disableSave = false;
      this.loggingIn = false;
    });
  }

  resetForm() {
    this.userRegisterForm.reset();
  }

  onClickCancel() {
    this.resetForm();
    (<HTMLInputElement>document.getElementById("email")).disabled = false;
    this.roleDropdown.dropdown('clear');
    this.companyDropdown.dropdown('clear');
    this.positionDropdown.dropdown('clear');
    this.signingUsersDropdown.dropdown('clear');
    this.clonedUser = new RegisterCommand();
    this.editModal.modal('hide');
  }

  resendActivationCode(idResendActivation) {
    this.resended = true;
    this._account.resendActivation(idResendActivation).subscribe((response: SwaggerResponse<void>) => {
      this._toastr.success("Activation email has been resent", 'Succesfully');
    //  this.resended = false;
    }, (error) => {
      this._toastr.success("Activation email has not been resent", 'Failed');
      this.resended = false;
      });
  }
  ngOnDestroy() {
    if (this.editModal) {
      this.editModal.modal('hide');
      this.editModal.remove();
    }

  }
}
