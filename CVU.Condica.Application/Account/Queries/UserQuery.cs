﻿
using CVU.Condica.Application.Account.Models;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Queries
{
    public class UserQuery : IRequest<UserDto>
    {
        public int UserId { get; set; }
    }

    public class UserQueryHandler : RequestHandler<UserQuery, UserDto>
    {
        public UserQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<UserDto> Handle(UserQuery request, CancellationToken cancellationToken)
        {
            var User = appDbContext.User
                .Include(d => d.Company)
                .Include(d => d.Position)
                .Include(d => d.Role)
                .Include(d => d.SigningUsers)
                .ThenInclude(d => d.Signing)
                .Include(d => d.UserDaysOff)
                .Include(d => d.SignPriority)
                .Include(d => d.Attendances)
                .Where(d => d.Id == request.UserId).ToList().AsQueryable();

            var userProjection = User
                .Select(UserDto.Projection)
                .FirstOrDefault(d => d.UserId == request.UserId);

            return userProjection;
        }
    }

    public class UserQueryValidator : AbstractValidator<UserQuery>
    {
        public UserQueryValidator(ValidationService validationService)
        {
            //RuleFor(d => d).Must(x=>validationService.UserExists((int?)x.UserId)).WithMessage(ValidationMessages.UserNotInDatabase);
            RuleFor(x => x).Custom(validationService.ValidateUserQuery);

        }
    }

    public class UsersQueryAuthorization : AuthorizationSetup<UserQuery>
    {
        public UsersQueryAuthorization()
        {
        }
    }
}
