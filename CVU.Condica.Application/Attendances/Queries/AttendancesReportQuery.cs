﻿using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Queries
{
    public class AttendancesReportQuery : IRequest<byte[]>
    {
        public DateTime DateTime { get; set; }
    }
    public class AttendancesReportQueryHandler : IRequestHandler<AttendancesReportQuery, byte[]>
    {
        private readonly AttendancesService attendancesService;

        public AttendancesReportQueryHandler(AttendancesService attendancesService)
        {
            this.attendancesService = attendancesService;
        }

        public async Task<byte[]> Handle(AttendancesReportQuery request, CancellationToken cancellationToken)
        {
            return attendancesService.AttendancesReport(request.DateTime);
        }
    }
    public class AttendancesReportQueryAuthorization : AuthorizationSetup<AttendancesReportQuery>
    {
        public AttendancesReportQueryAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }

}
