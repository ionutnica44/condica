using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{
    public class EditProfileCommand : IRequest<Unit>
    {
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public int? RoleId { get; set; }
        public int? CompanyId { get; set; }
        public int? PositionId { get; set; }
        public bool? IsBlocked { get; set; }
        public bool? CanSign { get; set; }
        public Dictionary<int, string> SigningUsers { get; set; }
    }

    public class EditProfileCommandHandler : RequestHandler<EditProfileCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public EditProfileCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(EditProfileCommand request, CancellationToken cancellationToken)
        {
            var user = appDbContext.User
                .Include(d => d.SigningUsers)
                .First(u => u.Id == (request.UserId ?? CurrentUser.Id));

            if (!string.IsNullOrEmpty(request.FirstName))
            {
                user.FirstName = request.FirstName;
            }
            if (!string.IsNullOrEmpty(request.LastName))
            {
                user.LastName = request.LastName;
            }
            if (!string.IsNullOrEmpty(request.PhoneNumber))
            {
                user.PhoneNumber = request.PhoneNumber;
            }
            if (request.IsBlocked.HasValue)
            {
                user.IsBlocked = request.IsBlocked.Value;
            }
            if (request.CompanyId.HasValue)
            {
                user.CompanyId = request.CompanyId.Value;
            }
            if (request.RoleId.HasValue)
            {
                user.RoleId = request.RoleId.Value;
            }
            if (request.PositionId.HasValue)
            {
                user.PositionId = request.PositionId.Value;
            }

            if (request.CanSign.HasValue)
            {
                if (request.CanSign == true)
                {
                    user.SignPriorityId = (int)SignPriorities.Third;
                }
                else
                {
                    user.SignPriorityId = null;

                    vacationService.UpdateSigningFlow(new List<int> { user.Id });
                }
            }

            //marius si iustin nu vor avea niciodata signing users asignati.
            //update code when creating teams diagram
            if (request.UserId != 1 && request.UserId != 2)
            {
                if (request.SigningUsers != null)
                {
                    var removedUserIds = user.SigningUsers
                        .Where(d => !request.SigningUsers.Any(x => x.Key == d.SigningUserId))
                        .Select(d => d.SigningUserId);

                    appDbContext.SigningUser.RemoveRange(user.SigningUsers);

                    if (!request.SigningUsers.ContainsKey(1))
                    {
                        request.SigningUsers.Add(1, "Marius Dumitrascu");
                    }

                    if (!request.SigningUsers.ContainsKey(2))
                    {
                        request.SigningUsers.Add(2, "Iustinian Nita");
                    }


                    user.SigningUsers = request.SigningUsers.Select(d => new SigningUser { SigningUserId = d.Key }).ToList();

                    vacationService.UpdateSigningFlow(removedUserIds, user.Id);
                }
            }

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }
    public class EditProfileCommandValidator : AbstractValidator<EditProfileCommand>
    {
        public EditProfileCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditProfileCommand);
        }
    }
    public class EditProfileCommandAuthorization : AuthorizationSetup<EditProfileCommand>
    {
        protected override bool CustomAuthorizationConditions(EditProfileCommand command, int roleId)
        {

            bool restrictedPropsEdited = command.RoleId.HasValue || command.PositionId.HasValue || command.CompanyId.HasValue || command.IsBlocked.HasValue || command.SigningUsers != null;

            if (restrictedPropsEdited && roleId != (int)Roles.Administrator)
            {
                return false;
            }

            if (command.UserId.HasValue && roleId != (int)Roles.Administrator)
            {
                return false;
            }

            return true;
        }

    }
}
