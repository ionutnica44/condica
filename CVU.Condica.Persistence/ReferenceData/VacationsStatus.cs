﻿namespace CVU.Condica.Persistence.ReferenceData
{
    public enum VacationsStatus
    {
        Pending = 1,
        Rejected = 2,
        Approved = 3
    }
}
