import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jwt_decode from "jwt-decode";
import { UserClient, SwaggerResponse, TokenDto } from 'app/client-api';

@Injectable()
export class AuthService {
    constructor(private _accountClient: UserClient) {
    }
    
    successAuth(response: SwaggerResponse<TokenDto>) {
        if (response) {
            localStorage.setItem("TokenDto", JSON.stringify(response.result));
            localStorage.setItem("Profile", JSON.stringify(response.result.user));

        }
    }

    checkToken() {
        const token = this.getTokenDto();
        if (token) {
            const todayDate = moment.utc(moment());
            const expires = moment(token.expires);
            if (expires < todayDate) {
                this.logOut();
                this.removeUserFromNotification();
                return false;
            }
            return true;

        } else {
            this.logOut();
            return false;
        }

    }

    failAuth(error) {
        console.log(error);
    }

    getCurrentUser() {
        let profile = localStorage.getItem('Profile');
        if (profile) {
            let user = JSON.parse(profile);
            user.isAdmin = user.roleId === 1;
            user.isEmployee = user.roleId === 2;
            return user;
        }
        return null;
    }

    isLogged() {
        if (this.getCurrentUser()) {
            return true;
        }
        return false;
    }

    hasRole(role) {
        let user = this.getCurrentUser();
        if (user) {
            return user.role && user.role.includes(role);
        } else {
            return false;
        }
    }

    logOut() {
        localStorage.removeItem("TokenDto");
        localStorage.removeItem("Profile");
        return !this.isLogged();
    }

    removeUserFromNotification() {
        const userFirebaseToken = JSON.parse(localStorage.getItem("UserFirebaseToken")).firebaseToken;
        localStorage.removeItem("UserFirebaseToken");
    }

    getTokenDto() {
        const TString = localStorage.getItem("TokenDto");
        if (TString && TString !== '') {
            try {
                return JSON.parse(TString);
            } catch (exception) {
                console.error(exception);
            }
        }
        return null;
    }

    getAccessToken() {
        const accessToken = this.getTokenDto().accessToken;
        if (accessToken) {
            return accessToken;
        }
        return null;
    }

    getTokenProfile() {
        const accessToken = this.getAccessToken();
        if (accessToken) {
            return jwt_decode(accessToken);
        }
        return null;
    }


    getUserLanguage() {
        const userLanguage = this.getCurrentUser();
        if (userLanguage) {
            return JSON.parse(userLanguage.language);
        }
        return [];
    }

    getClaims() {
        const accessToken = this.getTokenProfile();
        if (accessToken) {
            return JSON.parse(accessToken.claims);
        }
        return [];
    }


    updateLocalUser(changedUser) {
        if (changedUser) {
            const currentUser = this.getCurrentUser();
            for (let field in changedUser) {
                if (currentUser[field]) {
                    currentUser[field] = changedUser[field];
                }
            }
            localStorage.setItem("Profile", JSON.stringify(currentUser));
        }
    }


}