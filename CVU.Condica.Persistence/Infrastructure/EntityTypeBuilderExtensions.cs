using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CVU.Condica.Persistence.Infrastructure
{
    /// <summary>
    /// Entity Type Builder Extesntions Class
    /// </summary>
    public static class EntityTypeBuilderExtensions
    {
        /// <summary>
        /// Configures this entity to have seed data from enum values.
        /// </summary>
        /// <typeparam name="TEnum">Enum with reference data</typeparam>
        /// <typeparam name="TEntity">Database Entity</typeparam>
        /// <param name="keyField">The field to map enum key</param>
        /// <param name="valueField">Field to map enum value</param>
        public static void HasReferenceData<TEnum, TEntity>(this EntityTypeBuilder<TEntity> source, Expression<Func<TEntity, int>> keyField, Expression<Func<TEntity, string>> valueField) where TEnum : struct, IConvertible where TEntity : class
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            var entityInstance = Activator.CreateInstance(typeof(TEntity));

            var refData = Enum.GetValues(typeof(TEnum)).Cast<TEnum>().ToList();

            var listToAdd = new List<TEntity>();

            refData.ForEach(enumEntry =>
            {
                var keyExpression = (MemberExpression)keyField.Body;
                var valueExpression = (MemberExpression)valueField.Body;

                string keyFieldName = keyExpression.Member.Name;
                string valueFieldName = valueExpression.Member.Name;

                var objectInstance = Activator.CreateInstance<TEntity>();
                var propertyInfoKey = objectInstance.GetType().GetProperty(keyFieldName);
                var propertyInfoValue = objectInstance.GetType().GetProperty(valueFieldName);

                propertyInfoKey.SetValue(objectInstance, Convert.ChangeType(enumEntry, propertyInfoKey.PropertyType));
                propertyInfoValue.SetValue(objectInstance, Convert.ChangeType(enumEntry, propertyInfoValue.PropertyType));

                listToAdd.Add(objectInstance);
            });

            source.HasData(listToAdd);
        }
    }
}
