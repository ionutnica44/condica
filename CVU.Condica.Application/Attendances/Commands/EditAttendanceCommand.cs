﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Commands
{
    public class EditAttendanceCommand : IRequest<Unit>
    {
        public int AttendanceId { get; set; }
        public DateTime? ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public bool? FromHome { get; set; }

    }
    public class EditAttendanceCommandHandler : RequestHandler<EditAttendanceCommand, Unit>
    {
        public EditAttendanceCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<Unit> Handle(EditAttendanceCommand request, CancellationToken cancellationToken)
        {
            var attendanceDb = appDbContext.Attendance.First(d => d.Id == request.AttendanceId);

            attendanceDb.ArrivalTime = request.ArrivalTime ?? attendanceDb.ArrivalTime;
            attendanceDb.DepartureTime = request.DepartureTime ?? attendanceDb.DepartureTime;
            attendanceDb.FromHome = request.FromHome ?? attendanceDb.FromHome;

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }
    public class EditAttendanceCommandValidator : AbstractValidator<EditAttendanceCommand>
    {
        public EditAttendanceCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditAttendanceCommand);
        }
    }

    public class EditAttendanceCommandAuthorization : AuthorizationSetup<EditAttendanceCommand>
    {
        public EditAttendanceCommandAuthorization()
        {
        }
    }
}
