﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Items.Commands
{
    public class EditItemCommand : IRequest<Unit>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public int? UserId { get; set; }
        public int? ItemTypeId { get; set; }
    }

    public class EditItemCommandHandler : RequestHandler<EditItemCommand, Unit>
    {
        public EditItemCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Unit> Handle(EditItemCommand request, CancellationToken cancellationToken)
        {
            var item = appDbContext.Item.First(d => d.Id == request.Id);

            item.Name = request.Name ?? item.Name;
            item.SerialNumber = request.SerialNumber ?? item.SerialNumber;
            item.UserId = request.UserId ?? item.UserId;
            item.ItemTypeId = request.ItemTypeId ?? item.ItemTypeId;

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class EditItemCommandValidator : AbstractValidator<EditItemCommand>
    {
        public EditItemCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditItemCommand);
        }
    }

    public class EditItemCommandAuthorization : AuthorizationSetup<EditItemCommand>
    {
        public EditItemCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
