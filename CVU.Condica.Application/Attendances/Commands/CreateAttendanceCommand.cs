﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Commands
{
    public class CreateAttendanceCommand : IRequest<int>
    {
        public DateTime? ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public bool? FromHome { get; set; }
        public CreateAttendanceCommand()
        {
            this.ArrivalTime = ArrivalTime ?? DateTime.Now;
        }
    }
    public class CreateAttendanceCommandHandler : RequestHandler<CreateAttendanceCommand, int>
    {
        public CreateAttendanceCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<int> Handle(CreateAttendanceCommand request, CancellationToken cancellationToken)
        {
            var attendance = new Attendance
            {
                ArrivalTime = request.ArrivalTime ?? DateTime.Now,
                DepartureTime = request.DepartureTime ?? null,
                FromHome = request.FromHome ?? false,
                UserId = CurrentUser.Id
            };
            appDbContext.Attendance.Add(attendance);
            appDbContext.SaveChanges();

            return attendance.Id;
        }
    }
    public class CreateAttendanceCommandValidator : AbstractValidator<CreateAttendanceCommand>
    {
        public CreateAttendanceCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateCreateAttendanceCommand);
        }
    }

    public class CreateAttendanceCommandAuthorization : AuthorizationSetup<CreateAttendanceCommand>
    {
        public CreateAttendanceCommandAuthorization()
        {
        }
    }
}
