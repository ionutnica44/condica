import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import * as _ from 'lodash';

@Injectable()

export class PermisionsGuard implements CanActivate {

  constructor(private _authService: AuthService, private _router: Router) {
    window["_"] = _;
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = this._authService.getCurrentUser();
    if (!user) {
      this._authService.logOut();
      return false;
    }

    const hasClaims = route.data && route.data.claim ? route.data.claim : null;

    // Get user claims
    const userClaims = this._authService.getClaims();

    if (hasClaims) {
      if (Array.isArray(hasClaims)) {
        const inter = _.intersection(userClaims, hasClaims);
        if (inter && inter.length) {
          return true;
        } else {
          this._router.navigateByUrl('/admin/no-access');
          return false;
        }
      } else {
        const inter = _.intersection(userClaims, [hasClaims]);
        if (inter && inter.length) {
          return true;
        } else {
          this._router.navigateByUrl('/admin/no-access');
          return false;
        }
      }
    } else {
      return true;
    }
    // return false;
    // return true;
  }

}

