import { VacationEditComponent } from './vacation-edit/vacation-edit.component';
import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { VacationsClient, SwaggerResponse, VacationRequestApprovalDto, VacationDto, VacationQuery, DeleteVacationCommand, Unit } from 'app/client-api';
import { RequestOptions } from 'services/requestOptions';
import { AuthService } from 'services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vacations',
  templateUrl: './vacations.component.html',
  styleUrls: ['./vacations.component.css']
})
export class VacationsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(VacationEditComponent) vacationEdit: VacationEditComponent;
  vacations: VacationDto[] = [];
  
  private deleteModal: any;
  private statisticsModal: any;
  vacationToDelete: number;
  deleteVacCommand = new DeleteVacationCommand();
  loading: boolean = true;
  statistics: any;
  disabledButtonsDate = new Date();
  modalStatistics : any;
  page: RequestOptions<VacationQuery> = new RequestOptions<VacationQuery>();

  constructor(private _vacationClient: VacationsClient, private _authService: AuthService, private _toastr: ToastrService) {
    this.deleteVacCommand = new DeleteVacationCommand();
    
  }
  refreshDaysList(event) {
    if (event === "refresh") {
      this.getvacationsList();
    }
  }

  ngOnInit() {
    this.getvacationsList();
  }

  ngAfterViewInit() {
    this.deleteModal = ($('.delete-vacation') as any).modal({ closable: false });
    this.statisticsModal = ($('.statistics') as any).modal({closable: false});
  }

  addVacation() {
    this.vacationEdit.addvacationEdit();
  }
  editVacation(vacation: VacationDto) {
    this.vacationEdit.editVacation(vacation);
  }
  deleteVacation(vacation: VacationDto) {
    if (vacation) {
      this.vacationToDelete = vacation.id;
      this.deleteVacCommand.id = vacation.id;
      this.deleteModal.modal('show');
    }
  }
  delete() {
    this._vacationClient.deleteVacation(this.vacationToDelete, this.deleteVacCommand).subscribe((response: SwaggerResponse<Unit>) => {
      this._toastr.success('Vacation deleted');
      this.deleteModal.modal('hide');
      this.getvacationsList();
    }, (e) => {
      this._toastr.error('Error');
      this.deleteModal.modal('show');
    });

  }
  getvacationsList = () => {
    let user = this._authService.getCurrentUser();
    this.page.filter.userId = user ? user.userId : null;
    this._vacationClient.listVacations(this.page.filter.userId, this.page.filter).subscribe((response: SwaggerResponse<VacationDto[]>) => {
      this.page.setPaginationInfo(response);
      this.vacations = response.result;
    //  console.log("remaining to approve", this.vacations)
     // console.log("approved by",  this.vacations[0].approvedBy)
    //  console.log("rejected by", this.vacations[0].rejectedBy)
      this.statistics = response.headers;
      this.modalStatistics = JSON.parse(response.headers.vacationuserstatus);
      this.loading = false;
    });
  }
  onClickCancel() {
    this.vacationToDelete = 0;
    this.deleteModal.modal('hide');
  }
  onClickCancelStatistics(){
    this.statisticsModal.modal('hide');
  }
  ngOnDestroy() {
    this.deleteModal.modal('hide');
    this.deleteModal.remove();
    this.statisticsModal.remove();
  }

  openStatistics(){
    this.statisticsModal.modal('show');
  }

}
