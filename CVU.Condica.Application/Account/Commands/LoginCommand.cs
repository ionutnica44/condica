using CVU.Condica.Application.Account.Models;
using CVU.Condica.Application.Account.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Persistence.Models;
using CVU.Utilities.Security;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{
    public class LoginCommand : IRequest<TokenDto>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class LoginCommandHandler : RequestHandler<LoginCommand, TokenDto>
    {
        private readonly TokenProviderOptions tokenOptions;

        public LoginCommandHandler(IServiceProvider serviceProvider, IOptions<TokenProviderOptions> tokenOptions) : base(serviceProvider)
        {
            this.tokenOptions = tokenOptions.Value;
        }

        public override async Task<TokenDto> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var User = appDbContext.User
                .Include(d => d.Role)
                .Include(d => d.Company)
                .Include(d => d.Position)
                .FirstOrDefault(d => d.Email == request.Email);

            var identity = User.GetIdentity(request.Password);

            var userDto = UserDto.Create(User);

            var token = GenerateToken(userDto, User.Role.Name);

            return token;
        }
        private TokenDto GenerateToken(UserDto userDto, string userRoles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userDto.Email),
                new Claim(JwtRegisteredClaimNames.Jti, tokenOptions.JtiGenerator().Result),
                new Claim(JwtRegisteredClaimNames.Sid, userDto.UserId.ToString()),
                new Claim("roles", string.Join(",", userRoles)),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(tokenOptions.IssuedAt).ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                new Claim("Company", userDto.CompanyId.HasValue ? userDto.CompanyId.Value.ToString() : "0")
            };

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: tokenOptions.Issuer,
                audience: tokenOptions.Audience,
                claims: claims,
                notBefore: tokenOptions.NotBefore,
                expires: tokenOptions.Expiration,
                signingCredentials: tokenOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            // Serialize and return the response
            return new TokenDto
            {
                AccessToken = encodedJwt,
                Expires = jwt.ValidTo,
                User = userDto
            };
        }

    }

    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateLoginCommand);
        }
    }
}