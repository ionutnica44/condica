﻿using CVU.Condica.Application.Vacations.Utils;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Infrastructure.BackgroundWorkers
{
    public class VacationUpdaterService : IHostedService, IDisposable
    {
        private readonly ILogger<VacationUpdaterService> logger;
        private readonly IVacationService vacationService;
        private Timer timer;

        public VacationUpdaterService(ILogger<VacationUpdaterService> logger, IVacationService vacationService)
        {
            this.logger = logger;
            this.vacationService = vacationService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Vacations updating...");
            timer = new Timer(DoWork, null, new TimeSpan(0, 0, 10), new TimeSpan(30, 0, 0, 0));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            vacationService.UpdateVacationDays();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Vacation updating service stopped.");

            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
