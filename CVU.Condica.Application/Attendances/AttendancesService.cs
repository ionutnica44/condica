﻿using CVU.Condica.Application.Attendances.Queries;
using MediatR;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Drawing;

namespace CVU.Condica.Application.Attendances
{
    public class AttendancesService : IAttendancesService
    {
        private readonly IMediator mediator;
        public AttendancesService(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public byte[] AttendancesReport(DateTime dateTime)
        {
            var data = mediator.Send(new AttendancesListQuery { StartDate = new DateTime(dateTime.Year, dateTime.Month, 1), EndDate = new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month)) }).Result;

            byte[] dataByteArray;
            using (var package = new ExcelPackage())
            {
                var ws = package.Workbook.Worksheets.Add("Attendances");

                //TitleCell
                ws.Cells["A1:AF1"].Merge = true;
                var mergedCellsTitle = ws.Cells[ws.MergedCells[1, 1]];
                mergedCellsTitle.Value = $"CVU Intelligence - Prezente - {dateTime:MMMM yyyy}";
                mergedCellsTitle.Style.Font.Color.SetColor(Color.DarkGreen);
                mergedCellsTitle.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                mergedCellsTitle.Style.Font.Bold = true;
                mergedCellsTitle.Style.Font.Size = 14;
                mergedCellsTitle.Style.Font.Name = "Arial Narrow";
                ws.Column(1).Width = 20;

                for (int i = 1; i <= DateTime.DaysInMonth(dateTime.Year, dateTime.Month); i++)
                {
                    ws.Cells[2, i + 1].Value = i;
                }

                //Data rows
                var firstRow = 3;
                foreach (var user in data)
                {
                    ws.Cells[firstRow, 1].Value = $"{user.FirstName} {user.LastName}";
                    foreach (var day in user.Attendances)
                    {
                        ws.Cells[firstRow, day.ArrivalTime.Day + 1].Value = day.Status;
                    }
                    firstRow++;
                }
                package.Save();
                dataByteArray = package.GetAsByteArray();
            }

            return dataByteArray;
        }


    }
}
