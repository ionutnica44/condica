﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CVU.Condica.Persistence.Models
{
    public class VacationUserManagement
    {
        public VacationUserManagement()
        {
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Year { get; set; }
        public int TotalDays { get; set; }
        public int TakenDays { get; set; }

        public virtual User User { get; set; }
    }

    public class VacationUserManagementConfiguration : IEntityTypeConfiguration<VacationUserManagement>
    {
        public void Configure(EntityTypeBuilder<VacationUserManagement> builder)
        {
            builder
                .HasOne(d => d.User)
                .WithMany(d => d.VacationUserManagement)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }

    }
}
