﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Reference
{
    public class PositionsQuery : IRequest<Dictionary<int, string>>
    {
    }

    public class PositionQueryHandler : RequestHandler<PositionsQuery, Dictionary<int, string>>
    {
        public PositionQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<Dictionary<int, string>> Handle(PositionsQuery request, CancellationToken cancellationToken)
        {
            var positions = appDbContext.Position
                .ToDictionary(d => d.Id, d => d.Name);

            return positions;
        }
    }
}
