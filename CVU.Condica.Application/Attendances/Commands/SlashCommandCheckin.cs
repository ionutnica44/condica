﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Commands
{
    public class SlashCommandCheckin : IRequest<string>
    {

        [JsonIgnore]
        public DateTime ArrivalTime { get; set; }
        [JsonIgnore]
        public string CustomUserEmail { get; set; }

    }
    public class SlashCommandCheckinHandler : IRequestHandler<SlashCommandCheckin, string>
    {
        private readonly AppDbContext appDbContext;
        public SlashCommandCheckinHandler(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Task<string> Handle(SlashCommandCheckin request, CancellationToken cancellationToken)
        {
            var customUser = appDbContext.User.FirstOrDefault(d => d.Email == request.CustomUserEmail);
            customUser.Attendances.Add(new Attendance { ArrivalTime = request.ArrivalTime });
            appDbContext.SaveChanges();

            return new Task<string>(() => "Checkin created successfully.");

        }
    }
    public class SlashCommandCheckinValidator : AbstractValidator<SlashCommandCheckin>
    {
        public SlashCommandCheckinValidator(ValidationService validationService)
        {
            RuleFor(d => new CreateAttendanceCommand { ArrivalTime = d.ArrivalTime }).Custom(validationService.ValidateCreateAttendanceCommand);
        }
    }
    public class SlashCommandCheckinAuthorization : AuthorizationSetup<SlashCommandCheckin>
    {
        public SlashCommandCheckinAuthorization()
        {
            //NeedsIp(true);
        }
    }

}
