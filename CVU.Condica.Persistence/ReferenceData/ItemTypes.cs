﻿namespace CVU.Condica.Persistence.ReferenceData
{
    public enum ItemTypes
    {
        Laptop = 1,
        Mouse = 2,
        Keyboard = 3,
        ComputerScreen = 4,
        HeadPhones = 5,
        Computer = 6,
        Furniture = 7
    }
}
