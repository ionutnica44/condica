﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Vacations.Queries
{
    public class VacationsReportQuery : IRequest<byte[]>
    {
        public DateTime DateTime { get; set; }
    }
    public class VacationsReportQueryHandler : IRequestHandler<VacationsReportQuery, byte[]>
    {
        private readonly IVacationService vacationService;

        public VacationsReportQueryHandler(IVacationService vacationService)
        {
            this.vacationService = vacationService;
        }

        public async Task<byte[]> Handle(VacationsReportQuery request, CancellationToken cancellationToken)
        {
            return vacationService.GenerateVacationReport(request.DateTime);
        }
    }
    public class VacationsReportQueryAuthorization : AuthorizationSetup<VacationsReportQuery>
    {
        public VacationsReportQueryAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
