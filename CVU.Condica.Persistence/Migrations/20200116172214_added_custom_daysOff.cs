﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CVU.Condica.Persistence.Migrations
{
    public partial class added_custom_daysOff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCustomDaysOff",
                table: "DaysOff",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "UserDaysOff",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    DayOffId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDaysOff", x => new { x.DayOffId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserDaysOff_DaysOff_DayOffId",
                        column: x => x.DayOffId,
                        principalTable: "DaysOff",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserDaysOff_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.InsertData(
            //    table: "User",
            //    columns: new[] { "Id", "CompanyId", "CreatedAt", "Email", "FirstName", "IsActivated", "IsBlocked", "LastName", "LastUpdatedAt", "Password", "PhoneNumber", "PositionId", "RoleId", "SecurityCode", "SecurityCodeExpiresAt", "SignPriorityId" },
            //    values: new object[] { 3, 1, new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ionut.nica@cvu.ro", "Ionut", true, false, "Nica", new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AA7K81530367D3n5yedJkG+KnczUiMh7hiMsVwzrvMGFL0s+VfFVYtJM6fIFtOC2Yw==", "", 3, 1, null, null, 3 });

            //migrationBuilder.InsertData(
            //    table: "User",
            //    columns: new[] { "Id", "CompanyId", "CreatedAt", "Email", "FirstName", "IsActivated", "IsBlocked", "LastName", "LastUpdatedAt", "Password", "PhoneNumber", "PositionId", "RoleId", "SecurityCode", "SecurityCodeExpiresAt", "SignPriorityId" },
            //    values: new object[] { 4, 1, new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test@cvu.ro", "Test", true, false, "Test", new DateTime(2018, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AA7K81530367D3n5yedJkG+KnczUiMh7hiMsVwzrvMGFL0s+VfFVYtJM6fIFtOC2Yw==", "", 3, 1, null, null, 3 });

            migrationBuilder.CreateIndex(
                name: "IX_UserDaysOff_UserId",
                table: "UserDaysOff",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDaysOff");

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DropColumn(
                name: "IsCustomDaysOff",
                table: "DaysOff");
        }
    }
}
