﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using FluentValidation;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Items.Commands
{
    public class CreateItemCommand : IRequest<Unit>
    {
        public string Name { get; set; }
        // public byte[] ImageData { get; set; }
        public string SerialNumber { get; set; }
        public int? UserId { get; set; }
        public int ItemTypeId { get; set; }

        public static Persistence.Models.Item Create(CreateItemCommand model)
        {
            return new Persistence.Models.Item
            {
                Name = model.Name,
                // Image = Convert.ToBase64String(model.ImageData),
                SerialNumber = model.SerialNumber,
                UserId = model.UserId,
                ItemTypeId = model.ItemTypeId
            };
        }
    }

    public class CreateItemCommandandler : RequestHandler<CreateItemCommand, Unit>
    {
        public CreateItemCommandandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Unit> Handle(CreateItemCommand request, CancellationToken cancellationToken)
        {
            var item = CreateItemCommand.Create(request);

            appDbContext.Item.Add(item);

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class CreateItemCommandValidator : AbstractValidator<CreateItemCommand>
    {
        public CreateItemCommandValidator()
        {
            RuleFor(x => x).Custom(ValidationService.ValidateCreateItemCommand);
        }
    }

    public class CreateItemCommandAuthorization : AuthorizationSetup<CreateItemCommand>
    {
        public CreateItemCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
