﻿using CVU.Condica.Application.Reference;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.ReferenceData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CVU.Condica.WebUI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReferenceController : BaseController
    {
        [HttpGet("roles")]
        public Dictionary<int, string> GetRoles()
        {
            var roles = new Dictionary<int, string>();

            foreach (var claim in Enum.GetValues(typeof(Roles)))
            {
                roles.Add((int)claim, claim.ToString());
            }

            return roles;
        }

        [HttpGet("companies")]
        public Task<Dictionary<int, string>> GetCompanies()
        {
            return Mediator.Send(new CompaniesQuery());
        }

        [HttpGet("positions")]
        public Task<Dictionary<int, string>> GetPositions()
        {
            return Mediator.Send(new PositionsQuery());
        }

        [HttpGet("sign-priority")]
        public Task<Dictionary<int, string>> GetPriorities()
        {
            return Mediator.Send(new SignPrioritiesQuery());
        }

        [HttpGet("signing-users")]
        public Task<Dictionary<int, string>> GetSigningUsers()
        {
            return Mediator.Send(new SigningUsersQuery());
        }

        [HttpGet("users")]
        public Task<Dictionary<int, string>> GetUsers()
        {
            return Mediator.Send(new UsersQuery());
        }

        [HttpGet("itemTypes")]
        public Dictionary<int, string> GetItemTypes()
        {
            var itemTypes = new Dictionary<int, string>();

            foreach (var claim in Enum.GetValues(typeof(ItemTypes)))
            {
                itemTypes.Add((int)claim, claim.ToString());
            }

            return itemTypes;
        }
    }
}