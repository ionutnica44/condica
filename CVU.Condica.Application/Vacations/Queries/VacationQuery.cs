﻿using CVU.Condica.Application.Vacations.Models;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Vacations.Queries
{
    public class VacationQuery : PaginatedQueryParameter, IRequest<VacationPaginatedModel<VacationDto>>
    {
        public int? UserId { get; set; }
    }
    public class ListVacationQueryHandler : RequestHandler<VacationQuery, VacationPaginatedModel<VacationDto>>
    {
        private readonly IPaginationService paginationService;

        public ListVacationQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<VacationPaginatedModel<VacationDto>> Handle(VacationQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Persistence.Models.Vacation> vacation;

            if (request.UserId.HasValue)
            {
                vacation = appDbContext.Vacation
                    .Include(d => d.VacationStatus)
                    .Include(d => d.User)
                    .Where(x => x.UserId == request.UserId)
                    .AsQueryable();
            }
            else
            {
                vacation = appDbContext.Vacation.Where(x => x.UserId == CurrentUser.Id)
                    .AsQueryable();
            }
            var paginatedResponse = paginationService.PaginatedResults<Persistence.Models.Vacation, VacationDto>(vacation, request);

            var userVacationStatus = appDbContext.VacationUserManagement
                .Where(d => d.UserId == CurrentUser.Id)
                .Select(d => new VacationUserManagementDto
                {
                    Year = d.Year,
                    TotalDays = d.TotalDays,
                    TakenDays = d.TakenDays,
                    RemainingDays = d.TotalDays - d.TakenDays
                })
                .OrderBy(d => d.Year)
                .AsEnumerable();

            return new VacationPaginatedModel<VacationDto>(paginatedResponse, userVacationStatus);
        }
    }

    public class VacationQueryValidator : AbstractValidator<VacationQuery>
    {
        public VacationQueryValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateVacationQuery);
        }
    }

    public class VacationQueryAuthorization : AuthorizationSetup<VacationQuery>
    {
        public VacationQueryAuthorization()
        {
        }
    }
}
