using CVU.Condica.Persistance.ReferenceData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public partial class User
    {
        public User()
        {
            Attendances = new HashSet<Attendance>();
            Vacations = new HashSet<Vacation>();
            VacationRequestApproval = new HashSet<VacationRequestApproval>();
            VacationUserManagement = new HashSet<VacationUserManagement>();
            SigningUsers = new HashSet<SigningUser>();
            RequestingUsers = new HashSet<SigningUser>();
            Items = new HashSet<Item>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }
        public int CompanyId { get; set; }
        public int PositionId { get; set; }
        public int? SignPriorityId { get; set; }

        public bool IsBlocked { get; set; }
        public bool IsActivated { get; set; }
        public string SecurityCode { get; set; }
        public DateTime? SecurityCodeExpiresAt { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public DateTime ContractDate { get; set; }

        public virtual Role Role { get; set; }
        public virtual Company Company { get; set; }
        public virtual Position Position { get; set; }
        public virtual SignPriority SignPriority { get; set; }

        public virtual ICollection<Attendance> Attendances { get; set; }
        public virtual ICollection<Vacation> Vacations { get; set; }
        public virtual ICollection<VacationRequestApproval> VacationRequestApproval { get; set; }
        public virtual ICollection<VacationUserManagement> VacationUserManagement { get; set; }
        public virtual ICollection<SigningUser> SigningUsers { get; set; }
        public virtual ICollection<SigningUser> RequestingUsers { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual ICollection<UserDaysOff> UserDaysOff { get; set; }

    }

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasOne(d => d.Role)
                .WithMany(d => d.Users)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(d => d.Company)
                .WithMany(d => d.Users)
                .HasForeignKey(d => d.CompanyId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(d => d.SignPriority)
                .WithMany(d => d.Users)
                .HasForeignKey(d => d.SignPriorityId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(d => d.Position)
                .WithMany(d => d.Users)
                .HasForeignKey(d => d.PositionId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasData(
                new User()
                {
                    Id = 1,
                    Email = "marius@cvu.ro",
                    FirstName = "Marius",
                    LastName = "Dumitrascu",
                    IsActivated = true,
                    RoleId = (int)Roles.Administrator,
                    PositionId = 3,
                    IsBlocked = false,
                    CreatedAt = new DateTime(2018, 1, 1),
                    LastUpdatedAt = new DateTime(2018, 1, 1),
                    PhoneNumber = "",
                    Password = "AA7K81530367D3n5yedJkG+KnczUiMh7hiMsVwzrvMGFL0s+VfFVYtJM6fIFtOC2Yw==",
                    CompanyId = 1,
                    SignPriorityId = 1
                },
                new User()
                {
                    Id = 2,
                    Email = "iustinian.nita@cvu.ro",
                    FirstName = "Iustinian",
                    LastName = "Nita",
                    IsActivated = true,
                    RoleId = (int)Roles.Administrator,
                    PositionId = 3,
                    IsBlocked = false,
                    CreatedAt = new DateTime(2018, 1, 1),
                    LastUpdatedAt = new DateTime(2018, 1, 1),
                    PhoneNumber = "",
                    Password = "AA7K81530367D3n5yedJkG+KnczUiMh7hiMsVwzrvMGFL0s+VfFVYtJM6fIFtOC2Yw==",
                    CompanyId = 1,
                    SignPriorityId = 2
                });
            builder.Property(d => d.ContractDate)
                .HasDefaultValue(new DateTime(DateTime.Now.Year, 1, 1));
        }

    }
}
