import { Component, AfterViewInit } from '@angular/core';
@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})

export class LoginComponent implements AfterViewInit {
    constructor() {
    }

    ngAfterViewInit() {
        ($('body') as any).addClass('login');
    }
}