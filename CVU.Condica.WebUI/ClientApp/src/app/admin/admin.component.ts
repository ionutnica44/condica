import { AuthService } from "../../services/auth/auth.service";
import { Component, OnDestroy, OnInit, AfterViewChecked, AfterViewInit } from "@angular/core";
import { Router, } from '@angular/router';
import { SidebarChange, windowLoadListener, windowSizeListener } from "app/shared/utilities/utilities";

@Component({
    selector: "admin-component",
    templateUrl: "./admin.component.html"
})


export class AdminComponent implements OnInit, AfterViewInit, OnDestroy {
    isAdmin: boolean;
    isVisible: boolean;
    message;
    contentChangeObs: any;

    constructor(private _router: Router, private _authService: AuthService) {
        this.isVisible = false;
    }

    ngOnInit() {
        SidebarChange();
        windowLoadListener();
        windowSizeListener();
    }


    ngAfterViewInit() {
    }



    hideMessage() {
        this.isVisible = false;
    }


    ngOnDestroy() {
        ($(".pusher") as any).addClass("tweaked");
        ($(".ui.sidebar") as any).sidebar("pull page");
    }
}
