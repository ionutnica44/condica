﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Reference
{
    public class SignPrioritiesQuery : IRequest<Dictionary<int, string>>
    {
    }

    public class SignPrioritiesQueryHandler : RequestHandler<SignPrioritiesQuery, Dictionary<int, string>>
    {
        public SignPrioritiesQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<Dictionary<int, string>> Handle(SignPrioritiesQuery request, CancellationToken cancellationToken)
        {
            var positions = appDbContext.SignPriority
                .ToDictionary(d => d.Id, d => d.Name);

            return positions;
        }
    }
}
