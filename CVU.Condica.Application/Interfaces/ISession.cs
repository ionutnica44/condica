using CVU.Condica.Application.Account.Models;
using Microsoft.AspNetCore.Http;

namespace CVU.Condica.Application
{
    public interface ISession
    {
        CurrentUser CurrentUser { get; set; }
        int CurrentCompany { get; set; }
        IHttpContextAccessor HttpContextAccessor { get; set; }
    }
}