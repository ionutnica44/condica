﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public class Vacation
    {
        public Vacation()
        {
            VacationRequestApproval = new HashSet<VacationRequestApproval>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumberOfDays { get; set; }
        public int StatusId { get; set; }

        public virtual User User { get; set; }
        public virtual VacationStatus VacationStatus { get; set; }

        public virtual ICollection<VacationRequestApproval> VacationRequestApproval { get; set; }
    }

    public class VacationConfiguration : IEntityTypeConfiguration<Vacation>
    {
        public void Configure(EntityTypeBuilder<Vacation> builder)
        {
            builder
                .HasOne(d => d.User)
                .WithMany(d => d.Vacations)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(d => d.VacationStatus)
                .WithMany(d => d.Vacations)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.Restrict);
        }

    }
}
