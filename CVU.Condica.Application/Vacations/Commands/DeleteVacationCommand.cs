﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Vacations.Commands
{
    public class DeleteVacationCommand : IRequest<Unit>
    {
        public int Id { get; set; }

    }

    public class DeleteVacationCommandHandler : RequestHandler<DeleteVacationCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public DeleteVacationCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(DeleteVacationCommand request, CancellationToken cancellationToken)
        {
            var vacation = appDbContext.Vacation.First(d => d.Id == request.Id);

            vacationService.UpdateUserVacationDays(CurrentUser.Id, vacation.NumberOfDays, vacation.Id, true);

            appDbContext.Vacation.Remove(vacation);

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class DeleteDaysOffCommandValidator : AbstractValidator<DeleteVacationCommand>
    {
        public DeleteDaysOffCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateDeleteVacationCommand);
        }
    }

    public class DeleteDaysOffCommandAuthorization : AuthorizationSetup<DeleteVacationCommand>
    {
        public DeleteDaysOffCommandAuthorization()
        {

        }
    }
}
