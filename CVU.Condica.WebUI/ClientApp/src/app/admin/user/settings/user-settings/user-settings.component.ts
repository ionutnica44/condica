import { findObjectDiff } from 'app/shared/utilities/findFormDiff';
import { UserClient, SwaggerResponse, EditProfileCommand } from './../../../../client-api';
import { UserDto } from 'app/client-api';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';

@Component({
  selector: 'user-settings',
  templateUrl: './user-settings.component.html',
})
export class UserSettingsComponent implements OnChanges, OnInit {
  @Input() loading: boolean;
  @Input() user: UserDto;
  clonedUser: any = {} as EditProfileCommand;
  userOld: EditProfileCommand = {} as EditProfileCommand;
  editUserForm: FormGroup;
  constructor(private _fb: FormBuilder, private _toastr: ToastrService, private _account: UserClient) {
    this.editUserForm = _fb.group({
      firstName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.minLength(3)])],
      lastName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.minLength(3)])],
      phoneNumber: [null, Validators.compose([Validators.required, Validators.maxLength(24), Validators.minLength(3), Validators.pattern('[0-9]+')])],
      email: []

    }, {
      updateOn: 'blur'
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.clonedUser = new EditProfileCommand(this.user);
    this.userOld = _.clone(this.clonedUser);
   
  }


  ngOnInit() { 
    console.log("user inventory", this.user.items);
  }



  saveUserSettings() {
    const diff = findObjectDiff(this.userOld, this.clonedUser);
    const dataToSend = new EditProfileCommand(diff);
    this._account.editUser(dataToSend).subscribe((response: SwaggerResponse<void>) => {
      this._toastr.success('Your profile has been updated', 'Success');
    });
  }

}
