
import { ErrorInterceptor } from './../services/auth/error.interceptor';
import { JwtInterceptor } from './../services/auth/jwt.interceptor';
import { AuthService } from './../services/auth/auth.service';
import { PermisionsGuard } from './../services/auth/guards/permisions.guard';
import { AuthGuard } from './../services/auth/guards/auth.guard';
import { AuthModule } from './auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './/app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { AdminModule } from './admin/admin.module';
import { UserClient, ReferenceClient, AttendancesClient, DaysOffClient, VacationsClient, ItemClient } from 'app/client-api';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      enableHtml: true,
      preventDuplicates: true,
      progressBar: true,
      progressAnimation: 'decreasing',
      tapToDismiss: true
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    AuthModule,
    AdminModule,
    AppRoutingModule
  ],
  providers: [
    AuthGuard,
    PermisionsGuard,
    AuthService,
    UserClient,
    ReferenceClient,
    DaysOffClient,
    AttendancesClient,
    VacationsClient,
    ItemClient,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
