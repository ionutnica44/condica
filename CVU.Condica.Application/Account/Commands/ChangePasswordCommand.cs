using CVU.Condica.Application.Account.Utils;
using CVU.Condica.Application.Enums;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Security;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Account.Commands
{
    public class ChangePasswordCommand : IRequest<Unit>
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordCommandHandler : RequestHandler<ChangePasswordCommand, Unit>
    {
        public ChangePasswordCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Unit> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            var User = appDbContext.User.First(d => d.Id == CurrentUser.Id);

            User.GetIdentity(request.CurrentPassword);

            User.Password = Crypto.HashPassword(AuthorizationVariables.Salt + request.NewPassword);

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator()
        {
            RuleFor(x => x).Custom(ValidationService.ValidateChangePasswordCommand);
        }
    }

    public class ChangePasswordCommandAuthorization : AuthorizationSetup<ChangePasswordCommand>
    {
        public ChangePasswordCommandAuthorization()
        {

        }
    }
}
