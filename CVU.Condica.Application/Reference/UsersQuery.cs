﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Reference
{
    public class UsersQuery : IRequest<Dictionary<int, string>>
    {
    }

    public class UsersQueryHandler : RequestHandler<UsersQuery, Dictionary<int, string>>
    {
        public UsersQueryHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async override Task<Dictionary<int, string>> Handle(UsersQuery request, CancellationToken cancellationToken)
        {
            var users = appDbContext.User
                .ToDictionary(d => d.Id, d => d.FirstName + " " + d.LastName);

            return users;
        }
    }
}
