﻿using System;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Items.Models
{
    public class ItemShortDto
    {
        public string Name { get; set; }
        public string ItemType { get; set; }

        public static Expression<Func<Persistence.Models.Item, ItemShortDto>> Projection
        {
            get
            {
                return p => new ItemShortDto()
                {
                    Name = p.Name,
                    ItemType = p.ItemType.Name
                };
            }
        }
    }
}
