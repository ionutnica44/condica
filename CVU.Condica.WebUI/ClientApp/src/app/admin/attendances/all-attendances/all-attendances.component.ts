import { Component, OnInit } from '@angular/core';
import { UserClient, SwaggerResponse, UserDto, ListUsersQuery, AttendancesClient, AttendancesListQuery } from 'app/client-api';
import { RequestOptions } from 'services/requestOptions';
import * as _ from 'lodash';
import * as moment from 'moment';
import { saveAs } from 'file-saver';

import { getDates } from 'app/shared/utilities/utilities';

class DataItem {
  constructor(public name: string, public dates: any[] = [], public totalTime?: string) { }
}

@Component({
  selector: 'app-all-attendances',
  templateUrl: './all-attendances.component.html',
  styleUrls: ['./all-attendances.component.css']
})
export class AllAttendancesComponent implements OnInit {
  week = new AttendancesListQuery();
  attendance: any;
  dayDate: Date;
  currentDay = new Date();
  data: DataItem[] = [];
  page: RequestOptions<ListUsersQuery> = new RequestOptions<ListUsersQuery>();

  headerRange: string[] = [];

  constructor(private _account: UserClient, private _attendancesClient: AttendancesClient) {
    this.page.filter.itemsPerPage = 40;
    this.getCurrentWeek();
  }

  ngOnInit() {
    this.getCurrentWeek();
    this.getAttendances();
  }

  getAttendances() {
    this._attendancesClient.listAttendances(this.week)
      .subscribe((response: SwaggerResponse<any[]>) => {
        //console.log("response from getAttendances", response.result);
        this.attendance = response.result;
      //  console.log('Get date range:', getDates(this.week.startDate, this.week.endDate));
        this.generateData();
      });
  }

  generateData() {
    this.data = [];
    const range = getDates(this.week.startDate, this.week.endDate);
    this.attendance.forEach((item) => {
      const model = new DataItem(item.firstName + " " + item.lastName);
      const attendances = {};
      item.attendances.forEach((a) => {
        const dateDay = new Date(a.arrivalTime).getDay();
        attendances[dateDay] = !a.id ? (a.status.indexOf('Day Off') > -1 ? 'Day Off' : 'Concediu') : 'Prezent' + (a.attendanceDuration == null ? " ": ("<br>" + a.attendanceDuration + " " + "h")) ;
      });
      range.forEach((date) => {
        const status = attendances[new Date(date).getDay()] || 'Absent';
        if(status == "Absent" &&  +this.currentDay.setMilliseconds(0) < +date.setMilliseconds(0)) {
          model.dates.push("-")
        } else
        model.dates.push(status);
      });
      this.data.push(model);
    });

    // generate table header
    this.headerRange = range.map((date) => {
      return moment(date).format("ddd MMM DD");
    });


  }

  getCurrentWeek() {
    console.log("this.week", this.week);
    const date = new Date();
    let diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
    console.log("diff", diff);
    this.week.startDate = new Date(date.setDate(diff));
   // this.week.endDate = new Date(date.setDate(diff+ 4));
     this.week.endDate = new Date(date.setDate(this.week.startDate.getDate() + 4));
     this.setStartAndEndHour(this.week.startDate, this.week.endDate);
    // console.log("this.week.startDate", this.week.startDate)
    // console.log("this.week.endDate", this.week.endDate)
    this.getAttendances();
  }
//refactor
/*   nextWeek() {
    this.week.startDate = new Date(this.week.startDate.getTime() + (7 * 24 * 60 * 60 * 1000));
    this.week.endDate = new Date(this.week.endDate.getTime() + (7 * 24 * 60 * 60 * 1000));

    this.getAttendances();
  }

  previousWeek() {
    this.week.startDate = new Date(this.week.startDate.getTime() - (7 * 24 * 60 * 60 * 1000));
    this.week.endDate = new Date(this.week.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));
    this.getAttendances();
  } */
  
  changeWeek(typeOfChange: string){
    const change =  7 * 24 * 60 * 60 * 1000;
    this.week.startDate = (typeOfChange == 'next') ? new Date(this.week.startDate.getTime() + change) : new Date(this.week.startDate.getTime() - change);
    this.week.endDate = (typeOfChange == 'next') ? new Date(this.week.endDate.getTime() + change) : new Date(this.week.endDate.getTime() - change);
    this.getAttendances();
  }
  setStartAndEndHour(startDate, endDate) {
    startDate.setHours(0);
    startDate.setMinutes(0);
    endDate.setHours(23);
    endDate.setMinutes(59);
  }

  getReport(){
    this._attendancesClient.getAttendancesReport(this.week.startDate).subscribe((response: SwaggerResponse<any>) =>{

      saveAs(response.result.data, response.result.fileName)
    })
  }

  
}
