﻿using CVU.Condica.Persistence.Infrastructure;
using CVU.Condica.Persistence.ReferenceData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace CVU.Condica.Persistence.Models
{
    public class ItemType
    {
        public ItemType()
        {
            Items = new HashSet<Item>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }

    public class ItemTypeConfiguration : IEntityTypeConfiguration<ItemType>
    {
        public void Configure(EntityTypeBuilder<ItemType> builder)
        {
            builder.HasReferenceData<ItemTypes, ItemType>(d => d.Id, d => d.Name);
        }
    }
}
