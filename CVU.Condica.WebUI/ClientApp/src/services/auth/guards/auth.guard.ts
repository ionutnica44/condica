import { AuthService } from './../auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _router: Router, private _authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let nepermis = ['/admin/approval'];
    let nepermisNotAdmin = ['/admin/all-attendances', '/admin/inventory'];
    //console.log('State:', state);


    let checkedToken = this._authService.checkToken();
    //  console.log('True on check token');
    // return true;

    // if (this._authService.isLogged() && checkedToken) {
    //   if (!this._authService.getCurrentUser().canSign) {
    //     if (nepermis.indexOf(state.url) >= 0)
    //       return false;
    //     else return true;
    //   }
    //   // logged in so return true
    //   //  console.log('True on is logged');
    //   return true;
    // }  
    if (this._authService.isLogged() && checkedToken) {
      if (!this._authService.getCurrentUser().canSign) {
        if (nepermis.indexOf(state.url) >= 0)
          return false;
      }

      if (!this._authService.getCurrentUser().isAdmin) {
        if (nepermisNotAdmin.indexOf(state.url) >= 0)
          return false;
      }
      return true;
    }
    
    this._router.navigateByUrl('/auth/login');
    // not logged in so redirect to login page with the return url and return false
    return false;
  }

}