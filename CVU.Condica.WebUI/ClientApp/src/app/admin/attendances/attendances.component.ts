import { EditAttendanceCommand, CreateAttendanceCommand, DaysOffClient, SwaggerResponse, DaysOffDto, AttendancesClient, AttendanceDto, Unit } from 'app/client-api';
import { Component, OnInit, ChangeDetectionStrategy, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';
import { CalendarEvent, CalendarEventAction, CalendarView, DAYS_OF_WEEK, CalendarEventTitleFormatter, CalendarAngularDateFormatter, CalendarDateFormatter } from 'angular-calendar';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours, startOfMonth, getMonth } from 'date-fns';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { CustomEventTitleFormatter } from 'helpers/custom-event-title-formatter.provider';
import 'semantic-ui-calendar/src/definitions/modules/calendar';
import { ToastrService } from 'ngx-toastr';
//import {AuthService} from 'src/services/auth/auth.service.ts';
import { AttendancesModalComponent } from './attendances-modal/attendances-modal.component';

import { find } from 'lodash';
import { create } from 'domain';


const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
@Component({
  selector: 'app-attendances',
  templateUrl: './attendances.component.html',
  styleUrls: ['./attendances.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter
    }]
})
export class AttendancesComponent implements OnDestroy {
  @ViewChild(AttendancesModalComponent) attendancesModal: AttendancesModalComponent;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  compareDateCurrent = new Date();
  compareDateMin = new Date();
  compareDateMax = new Date();
  weekStartsOn = DAYS_OF_WEEK.SUNDAY;
  refresh: Subject<any> = new Subject();
  excludeDays: number[] = [0, 6];
  activeDayIsOpen: boolean = true;
  events: CalendarEvent[] = [];
  checkedIn = true;
  checkedOut = true;
  disableButton = false;
  createdAt = new Date();


  todayAttendance: EditAttendanceCommand;

  fromHome: boolean;
  constructor(private _attendancesClient: AttendancesClient, private _toastr: ToastrService) {

    this.getUserAttendances();
   
  }

  refreshAttendancesList(event) {
    if (event === "refresh") {
      this.getUserAttendances();
    }
  }

  getUserAttendances = () => {
    this.compareDateMin = startOfMonth(this.viewDate);
    this.compareDateMax = endOfMonth(this.viewDate);
    const startOf = startOfMonth(this.viewDate);
    const endOf = endOfMonth(this.viewDate);
    this._attendancesClient.getUserAttendances(startOf, endOf)
      .subscribe((response: SwaggerResponse<any[]>) => {

        this.events = this.parseAttendancesBookings(response.result);
        this.createdAt = moment.parseZone(response.result[0].createdAt).toDate();
        let todayAttendance = response.result[0].attendances.find(item => {
          return moment().format('L') === moment(item.arrivalTime).format('L');
        });
        if (todayAttendance)
          if (todayAttendance.status !== 'Prezent') {
            this.checkedIn = true;
            this.checkedOut = true;
          } else {
            this.checkedIn = todayAttendance.arrivalTime ? true : false;
            this.checkedOut = todayAttendance.departureTime ? true : false;
          }
        else {
          this.checkedIn = false;
          this.checkedOut = true;
        }
        this.todayAttendance = todayAttendance;
        this.refreshView();
      });

  }

  private parseAttendancesBookings(userAttendancesBookings: any[]): CalendarEvent[] {
    return userAttendancesBookings.filter(item => item.attendances && item.attendances.length).map((event) => {
      const attendances = event.attendances;
      return attendances.map((item) => {
        return {
          start: item.arrivalTime ? moment.parseZone(item.arrivalTime).toDate() : null,
          end: item.departureTime ? moment.parseZone(item.departureTime).toDate() : null,
          title: item.status,
          id: item.id,
          createdAt: moment.parseZone(item.createdAt).toDate(),
          fromHome: item.fromHome,
          cssClass: 'red',
          totalTime: item.attendanceDuration,
          allDay: false,
        };
      });
    })['flat']();
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
          this.createdAt.setHours(0);
          this.createdAt.setMinutes(0);
          this.createdAt.setSeconds(0);
          this.createdAt.setMilliseconds(0);
    if (date >= startOfMonth(this.viewDate) && date < endOfMonth(this.viewDate)) {
      if (date <= new Date()) {
        if (events.length) {
          let dayType = events[0].title;
          if (dayType === 'Liber' || dayType === 'Concediu') {
            this._toastr.info("You can't edit this day");
          } else if (dayType === 'Prezent') {
            this.attendancesModal.editAttendanceModal(events);
          }
        } else if(date < this.createdAt) {
          console.log("date", date);
          console.log("created", this.createdAt);
          this._toastr.info("You can't edit days before your account was created");
        }
        else{
          this.attendancesModal.addAttendance(date);
        }
      } else {
        this._toastr.info("You can't edit a day from the future");

      }
    }
  }

  check(item?: any) {
    this.disableButton = true;
    setTimeout(() => {
      this.disableButton = false;
    }, 0);
    this.attendancesModal.clonedAttendance = new EditAttendanceCommand();
    this.attendancesModal.clonedAttendance.fromHome = item ? item.fromHome : false;
    if (item) {
      this.attendancesModal.clonedAttendance.attendanceId = item.id;
      this.attendancesModal.clonedAttendance.departureTime = new Date();
    }
    //    console.log('Save model:', this.attendancesModal.clonedAttendance);
    this.attendancesModal.save();
  }

  refreshView() {
    this.refresh.next();
  }

  ngOnDestroy() {
  }


}