import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch
} from "rxjs/operators";
import { SwaggerResponse, IPaginatedQueryParameter } from "app/client-api";

export class PaginationResponse {
  TotalCount?: number;
  PageSize?: number;
  CurrentPage?: number;
  TotalPages?: number;
}

export class ColumnResponse {
  constructor(public label: string, public prop: string, public sortAsc: boolean = false, public sortDesc: boolean = false, public isSortable: boolean = true) { }
}

export interface IRequestOptions<T extends IPaginatedQueryParameter> {
  filter: T;
  info: PaginationResponse;
  columns: ColumnResponse[];
  getParamters(...args: any[]): any[];
  setPaginationInfo(response: SwaggerResponse<any>): number;

  pageChanged(page: number, callback): void;
  setColumns(columns: ColumnResponse): void;
}

export class RequestOptions<T extends IPaginatedQueryParameter> implements IRequestOptions<IPaginatedQueryParameter> {
  filter: T;
  columns: ColumnResponse[] = [];
  info: PaginationResponse;
  constructor() {
    this.filter = {
      itemsPerPage: 10,
      orderField: "id desc",
      page: 1,
      fields: "",
      searchBy: ""
    } as T;

    this.info = {
      CurrentPage: this.filter.page,
      PageSize: this.filter.itemsPerPage,
      TotalCount: undefined
    } as PaginationResponse;
  }

  getParamters(...args: any[]): any[] {
    return [
      args,
      this.filter.page,
      this.filter.itemsPerPage,
      this.filter.orderField,
      this.filter.fields,
      this.filter.searchBy || ""
    ];
  }

  setPaginationInfo(response: SwaggerResponse<any>): number {
    this.info = this.getPagesInfo(response);
    this.filter.page = this.info.CurrentPage;
    return this.info.TotalCount;
  }

  setColumns(columns) {
    if (columns) {
      this.columns = columns;
      this.disableTableCell(columns);
      return this.columns;
    }
  }


  applySort(onColumn: any, columns: any, callback) {
    this.disableTableCell(columns);
    columns.forEach((column: any, key: number) => {
      if (onColumn !== column) {
        column.sortAsc = false;
        column.sortDesc = false;
      }
    });
    if (onColumn.sortDesc) {
      onColumn.sortDesc = false;
      onColumn.sortAsc = true;
    } else if (onColumn.sortAsc) {
      onColumn.sortAsc = false;
      onColumn.sortDesc = true;
    } else if (!onColumn.sortAsc) {
      onColumn.sortAsc = true;
      onColumn.sortDesc = false;
    } else {
      onColumn.sortAsc = false;
      onColumn.sortDesc = true;
    }
    const sort = onColumn.sortDesc === true ? 'desc' : 'asc';
    this.filter.orderField = onColumn.prop + ' ' + sort;

    this.pageChanged(1, callback);
  }

  async resetSortColumn(columns: any) {
    columns.forEach((column: any, key: number) => {
      column.sortAsc = false;
      column.sortDesc = false;
    });
    return this;
  }

  disableTableCell(columns: any) {
    columns.forEach((column: any, key: number) => {
      if ((column.sortAsc && column.sortDesc) === null) {
        column.isSortable = false;
      }
    });
  }


  pageChanged(page: number, callback): void {
    this.filter.page = page;
    callback();
  }

  private getPagesInfo(response: SwaggerResponse<any>): PaginationResponse {
    if (response && response.headers) {
      return JSON.parse(response.headers["x-pagination"]) as PaginationResponse;
    }
    return null;
  }
}
