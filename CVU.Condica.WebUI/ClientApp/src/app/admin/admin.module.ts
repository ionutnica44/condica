import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DaysOffComponent } from './days-off/days-off.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { NavTopMenuComponent } from './layout/nav-topMenu/nav-topMenu.component';
import { NavSidebarComponent } from './layout/nav-sidebar/nav-sidebar.component';
import { NavFooterComponent } from './layout/nav-footer/nav-footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserComponent } from './user/user.component';
import { EditComponent } from './user/edit/edit.component';
import { SettingsComponent } from './user/settings/settings.component';
import { UserChangePasswordComponent } from './user/settings/user-change-password/user-change-password.component';
import { UserSettingsComponent } from './user/settings/user-settings/user-settings.component';
import { DayEditComponent } from './days-off/day-edit/day-edit.component';
import { AttendancesComponent } from './attendances/attendances.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { AttendancesModalComponent } from './attendances/attendances-modal/attendances-modal.component';
import { VacationsComponent } from './vacations/vacations.component';
import { VacationEditComponent } from './vacations/vacation-edit/vacation-edit.component';
import { ApprovalComponent } from './vacations/approval/approval.component';
import { AllAttendancesComponent } from './attendances/all-attendances/all-attendances.component';
import { InventoryComponent } from './inventory/inventory.component';
import { InventoryModalComponent } from './inventory/inventory-modal/inventory-modal.component';




@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgxPaginationModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
  ],
  declarations: [
    AdminComponent,
    NavTopMenuComponent,
    NavSidebarComponent,
    NavFooterComponent,
    DashboardComponent,
    NotFoundComponent,
    UserComponent,
    EditComponent,
    SettingsComponent,
    UserChangePasswordComponent,
    UserSettingsComponent,
    DaysOffComponent,
    DayEditComponent,
    AttendancesComponent,
    AttendancesModalComponent,
    VacationsComponent,
    VacationEditComponent,
    ApprovalComponent,
    AllAttendancesComponent,
    InventoryComponent,
    InventoryModalComponent
  ]
})
export class AdminModule { }
