import { Component, OnInit, Input } from '@angular/core';
import { UserDto, EditProfileCommand, SwaggerResponse, UserClient } from 'app/client-api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
})

export class SettingsComponent implements OnInit {
  @Input() activeMenu: number = 1;
  user: UserDto = new UserDto();
  loading: boolean;

  constructor(private _account: UserClient, private _toastr: ToastrService) {
    this.loading = true;
  }

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this._account.currentUser().subscribe((response: SwaggerResponse<UserDto>) => {
      this.loading =  false;
      this.user = response.result;
    });
  }

  changeActiveMenu(menu: number) {
    this.activeMenu = menu;
  }


}
