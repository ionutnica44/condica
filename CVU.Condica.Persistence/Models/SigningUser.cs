﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CVU.Condica.Persistence.Models
{
    public class SigningUser
    {
        public int UserId { get; set; }
        public int SigningUserId { get; set; }

        public virtual User User { get; set; }
        public virtual User Signing { get; set; }
    }

    public class SigningUserConfiguration : IEntityTypeConfiguration<SigningUser>
    {
        public void Configure(EntityTypeBuilder<SigningUser> builder)
        {
            builder.HasKey(d => new { d.UserId, d.SigningUserId });

            builder
                .HasOne(d => d.Signing)
                .WithMany(d => d.RequestingUsers)
                .HasForeignKey(d => d.SigningUserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(d => d.User)
                .WithMany(d => d.SigningUsers)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
