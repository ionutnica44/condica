﻿using CVU.Condica.Application.DaysOff.Commands;
using CVU.Condica.Application.DaysOff.NewFolder;
using CVU.Condica.Application.Vacations.Models;
using CVU.Condica.Infrastructure.Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CVU.Condica.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class DaysOffController : BaseController
    {
        [HttpPost("get-all")]
        public IEnumerable<DaysOffDto> ListDaysOff([FromBody] ListDaysOffQuery request)
        {
            var mediatorResponse = Mediator.Send(request).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);

            return mediatorResponse.Items;
        }

        [HttpPost]
        public Task<Unit> CreateDaysOff([FromBody] CreateDaysOffCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPatch("{id}")]
        public Task<Unit> EditDaysOff(int id, [FromBody] EditDaysOffCommand command)
        {
            command.Id = id;
            return Mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public Task<Unit> DeleteDaysOff(int id, [FromBody] DeleteDaysOffCommand command)
        {
            command.Id = id;
            return Mediator.Send(command);
        }
    }
}