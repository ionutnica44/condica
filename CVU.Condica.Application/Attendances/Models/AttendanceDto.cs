﻿using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using System;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Attendances.Models
{
    public class AttendanceDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public bool FromHome { get; set; }
        public string Status { get; set; }
        public string AttendanceDuration { get; set; }
        public static Expression<Func<Attendance, AttendanceDto>> Projection
        {
            get
            {
                return p => new AttendanceDto()
                {
                    Id = p.Id,
                    UserId = p.UserId,
                    ArrivalTime = p.ArrivalTime,
                    DepartureTime = p.DepartureTime,
                    FromHome = p.FromHome,
                    Status = p.ArrivalTime != DateTime.MinValue ? AttendanceStatuses.Present.ToString() : AttendanceStatuses.Absent.ToString(),
                };
            }
        }
        public static AttendanceDto Create(Attendance attendance)
        {
            return Projection.Compile().Invoke(attendance);
        }
    }
}
