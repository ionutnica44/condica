﻿using CVU.Condica.Application.Items.Commands;
using CVU.Condica.Application.Items.Models;
using CVU.Condica.Application.Items.Queries;
using CVU.Condica.Infrastructure.Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CVU.Condica.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class ItemController : BaseController
    {
        [HttpPost("get-all")]
        public IEnumerable<ItemDto> ListItems([FromBody] ListItemQuery request)
        {
            var mediatorResponse = Mediator.Send(request).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);

            return mediatorResponse.Items;
        }

        [HttpGet("{id}")]
        public Task<ItemDto> GetItem([FromRoute] int id)
        {
            return Mediator.Send(new ItemQuery { ItemId = id });
        }

        [HttpPost]
        public Task<Unit> CreateItem([FromBody] CreateItemCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPatch("{id}")]
        public Task<Unit> EditItem(int id, [FromBody] EditItemCommand command)
        {
            command.Id = id;
            return Mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public Task<Unit> DeleteItem(int id, [FromBody] DeleteItemCommand command)
        {
            command.Id = id;
            return Mediator.Send(command);
        }
    }
}