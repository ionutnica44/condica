﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.VacationRequestApprovals.Commands
{
    public class EditVacationRequestApprovalCommand : IRequest<Unit>
    {
        public int Id { get; set; }
        public bool? Approved { get; set; }
        public string Comment { get; set; }
    }

    public class EditVacationRequestApprovalCommandHandler : RequestHandler<EditVacationRequestApprovalCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public EditVacationRequestApprovalCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(EditVacationRequestApprovalCommand request, CancellationToken cancellationToken)
        {
            var vacationRequest = appDbContext.VacationRequestApproval.FirstOrDefault(d => d.Id == request.Id);

            var vacation = appDbContext.Vacation
                .Include(d => d.VacationRequestApproval)
                .ThenInclude(d => d.SigningUser)
                .Include(d => d.User)
                .FirstOrDefault(d => d.Id == vacationRequest.VacationId);

            vacationRequest.Comment = request.Comment;

            if (request.Approved.HasValue)
            {
                vacationRequest.Approved = request.Approved;

                if (vacationRequest.Approved.Value)
                {
                    var signingUsers = appDbContext.SigningUser
                        .Include(d => d.Signing)
                        .Where(d => d.UserId == vacation.UserId)
                        .OrderByDescending(d => d.Signing.SignPriorityId)
                        .ToList()
                        .Select(d => (d.SigningUserId, d.Signing.SignPriorityId.Value))
                        .AsEnumerable();

                    var oldSignersCount = vacation.VacationRequestApproval.Count();

                    vacation.VacationRequestApproval.AddNextSigningUsers(signingUsers);

                    appDbContext.SaveChanges();

                    if (vacation.VacationRequestApproval.All(d => d.Approved.HasValue && d.Approved.Value))
                    {
                        vacation.StatusId = (int)VacationsStatus.Approved;
                        vacationService.SendVacationStatusEmail(vacation.Id, true);
                    }
                    else if (oldSignersCount != vacation.VacationRequestApproval.Count())
                    {
                        vacationService.SendVacationRequestEmail(vacation.Id);
                    }
                }
                else
                {
                    appDbContext.SaveChanges();

                    vacation.StatusId = (int)VacationsStatus.Rejected;

                    vacationService.SendVacationStatusEmail(vacation.Id, false);

                    vacationService.UpdateUserVacationDays(vacation.UserId, vacation.NumberOfDays, vacation.Id, true);
                }
            }

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class EditVacationRequestApprovalCommandValidator : AbstractValidator<EditVacationRequestApprovalCommand>
    {
        public EditVacationRequestApprovalCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditVacationRequestApprovalCommand);
        }
    }

    public class EditVacationRequestApprovalAuthorization : AuthorizationSetup<EditVacationRequestApprovalCommand>
    {
        public EditVacationRequestApprovalAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
            NeedsRole((int)Roles.Employee);
        }
    }
}
