﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Attendances.Commands
{
    public class SlashCommandCheckout : IRequest<string>
    {

        public DateTime DepartureTime { get; set; }
        public string CustomUserEmail { get; set; }

    }
    public class SlashCommandCheckoutHandler : IRequestHandler<SlashCommandCheckout, string>
    {
        private readonly AppDbContext appDbContext;

        public SlashCommandCheckoutHandler(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Task<string> Handle(SlashCommandCheckout request, CancellationToken cancellationToken)
        {
            var attendance = appDbContext.User.FirstOrDefault(d => d.Email == request.CustomUserEmail)
                .Attendances.FirstOrDefault(d => d.ArrivalTime.Date == request.DepartureTime.Date);
            attendance.DepartureTime = request.DepartureTime;
            appDbContext.SaveChanges();

            return new Task<string>(() => "Successfully checked out.");

        }
    }
    public class SlashCommandCheckoutValidator : AbstractValidator<SlashCommandCheckout>
    {
        public SlashCommandCheckoutValidator(ValidationService validationService)
        {
            RuleFor(d => d).Custom(validationService.ValidateSlashCommandCheckout);
        }
    }
    public class SlashCommandCheckoutAuthorization : AuthorizationSetup<SlashCommandCheckout>
    {
        public SlashCommandCheckoutAuthorization()
        {
            //NeedsIp(true);
        }
    }
}
