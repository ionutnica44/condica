﻿using System;
using System.Collections.Generic;

namespace CVU.Condica.Application.Vacations.Utils
{
    public interface IVacationService
    {
        void SendVacationRequestEmail(int vacationId);
        void SendVacationStatusUpdated(int vacationId, int signingUser, string signingUserName, string resigningUserName);
        void SendVacationStatusEmail(int vacationId, bool Approved);
        int CalculateNumberOfBusinessDays(DateTime StartDate, DateTime EndDate);
        int CalculateNumberOfBussinesDaysWithDaysOff(DateTime StartDate, DateTime EndDate);
        int CalculateNumberOfVacationDays(DateTime startDate, DateTime endDate);
        void UpdateUserVacationDays(int UserId, int NumberOfDays, int? VacationId = null, bool Delete = false);
        void UpdateSigningFlow(IEnumerable<int> signingUserIds, int? userId = null);
        byte[] GenerateVacationReport(DateTime dateTime);
        public bool UpdateVacationDays();
    }
}
