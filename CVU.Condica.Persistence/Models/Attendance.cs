﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CVU.Condica.Persistence.Models
{
    public class Attendance
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public bool FromHome { get; set; }
        public virtual User User { get; set; }
    }

    public class AttendanceConfiguration : IEntityTypeConfiguration<Attendance>
    {
        public void Configure(EntityTypeBuilder<Attendance> builder)
        {
            builder
                .HasOne(d => d.User)
                .WithMany(d => d.Attendances)
                .HasForeignKey(d => d.UserId);
        }

    }
}
