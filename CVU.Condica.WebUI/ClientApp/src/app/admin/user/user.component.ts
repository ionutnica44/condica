
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EditComponent } from './edit/edit.component';
import { UserClient, UserDto, SwaggerResponse, ListUsersQuery,ResendActivationCommand } from 'app/client-api';
import { RequestOptions } from 'services/requestOptions';
import { AuthService } from 'services/auth/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
})
export class UserComponent implements AfterViewInit, OnDestroy {
  @ViewChild(EditComponent) userEditComponent: EditComponent;
  loading: boolean;
  usersHardcoded: any;
  user: any; //ADD USERDTO
  currentUser: any;
  users: UserDto[] = [];
  page: RequestOptions<ListUsersQuery> = new RequestOptions<ListUsersQuery>();

  constructor(private _toastr: ToastrService, private _account: UserClient, private _auth: AuthService) {
    this.loading = true;
    this.page.filter.orderField = "firstName asc";
    this.currentUser = _auth.getCurrentUser();
  }

  ngAfterViewInit() {
    this.getUsers();

  }

  getUsers = () => {
    this._account.listUsers(this.page.filter).subscribe((response: SwaggerResponse<UserDto[]>) => {
      this.page.setPaginationInfo(response);
      this.users = response.result;
      this.loading = false;
    });
    this.userEditComponent.getSigningUsers();
  }
  addUser() {
    this.userEditComponent.addUser();
  }

  edit(user: UserDto) {
    this.userEditComponent.editUser(user);
  }
  ngOnDestroy(): void {

  }
}
