export enum ApplicationClaim {

    // claims by role
    FacilityManager = 1701,
    BuildingManager = 1707,
    Dispecer = 1702,
    TeamManager = 1703,
    TeamWorker = 1704,
    Representant = 1705,
    Member = 1706,

    // ticket
    CreateTicket = 1001,
    ReadTicket = 1002,
    UpdateTicketStatus = 1003,
    UpdateTicketPriority = 1004,
    UpdateTicketAssignee = 1005,
    SetTicketCost = 1006,
    ApproveTicketCost = 1007,
    SetTicketResponsable = 1008,
    UpdateTicket = 1013,
    GiveFeedback = 1014,
    LinkIssue = 1015,

    CreateTicketComment = 1009,
    ReadTicketComment = 1010,
    UpdateTicketComment = 1011,
    DeleteTicketComment = 1012,
    CreateInternalComment = 1016,


    // ticket category
    CreateTicketCategory = 1101,
    UpdateTicketCategory = 1102,
    ReadTicketCategory = 1103,
    ArchiveTicketCategory = 1104,

    // building
    CreateBuilding = 1201,
    ReadBuilding = 1202,
    UpdateBuilding = 1203,

    CreateFloor = 1204,
    ReadFloor = 1205,
    UpdateFloor = 1206,

    CreateCommuneSpace = 1207,
    ReadCommuneSpace = 1208,
    UpdateCommuneSpace = 1209,

    //vendor
    CreateVendor = 1301,
    ReadVendor = 1302,
    UpdateVendor = 1303,
    DeleteVendor = 1304,

    //facility users management
    ReadFacilityUser = 1401,
    CreateFacilityUser = 1402,
    UpdateFacilityUser = 1403,

    //tenant
    CreateTenant = 1501,
    ReadTenant = 1502,
    UpdateTenant = 1503,
    AssignTenantToFloor = 1504,

    CreateTenantUser = 1505,
    ReadTenantUser = 1506,
    UpdateTenantUser = 1507,

    //booking
    CreateBooking = 1601,
    ReadBooking = 1602,
    UpdateBooking = 1603,

}



export class RouteClaim {
    constructor(public claim: ApplicationClaim[]) { }
}

