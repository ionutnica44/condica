import { Component, OnInit, Output, EventEmitter, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { ItemClient, Unit,SwaggerResponse,ReferenceClient, CreateItemCommand, EditItemCommand, ItemDto} from 'app/client-api';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-inventory-modal',
  templateUrl: './inventory-modal.component.html'
})
export class InventoryModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @Output() event: EventEmitter<string> = new EventEmitter();
  private inventoryModal : any;
  inventoryForm = null;
  allUsers: { [id: number]: string };
  private allUserssDropdown: any;
  categories: any = [];
  selectedUser: any;
  disableSave = false;
  categoriesDropdown: any;
  itemTypeId: any;
  userId: any;
  allItems: ItemDto[];
  editItemModel: EditItemCommand;
  constructor(private _inventory: ItemClient, private _fb: FormBuilder, private _referenceClient: ReferenceClient, private _toastr: ToastrService) { 
    this.inventoryForm = _fb.group({
        name:['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
        serial:['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
        itemTypeId:['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
        userId:[''],
        image:['']
    });
    this.editItemModel = new EditItemCommand();

  }

  ngOnInit() {
    this.inventoryModal = ($('.inventory') as any).modal({closable:false});
  }
  ngAfterViewInit(){
    this.getAllUsers();
    this.getCategories();
    this.allUserssDropdown = ($('.allUsers-dropdown') as any).dropdown();
    this.categoriesDropdown = ($('.categories-dropdown') as any).dropdown();
  }

  getAllUsers() {
    this._referenceClient.getUsers()
      .subscribe((response: SwaggerResponse<any>) => {
        this.allUsers = response.result;
      });
  }
  getCategories() {
    this._referenceClient.getItemTypes()
      .subscribe((response: SwaggerResponse<any>) => {
        this.categories = response.result;
      });
  }
  addItem(){
    this.editItemModel = new CreateItemCommand();
    this.allUserssDropdown.dropdown('clear');
    this.categoriesDropdown.dropdown('clear');
    this.inventoryModal.modal('show');

  }
  handleChanges(event, field) {
    this.editItemModel[field] = event.target.value;
  }
  resetForm(){
    this.inventoryForm.reset();
  }
  editItem(item){
    if (item) {
      this.editItemModel = { ...item };
      this.categoriesDropdown.dropdown('set selected', item.itemTypeId);
      this.allUserssDropdown.dropdown('set selected', item.userId);
      this.inventoryModal.modal('show');
    }
  }
  onClickCancel() {
    this.resetForm();
    this.inventoryModal.modal('hide');
  }

  save(){
    const method: any = this.editItemModel.id? this._inventory['editItem'].bind(this._inventory,this.editItemModel.id,this.editItemModel) : this._inventory['createItem'].bind(this._inventory,this.editItemModel);
    const message = this.editItemModel.id? 'Item updated succesfully': "Item added succesfully";
    this.disableSave = true;
    method().subscribe((response: SwaggerResponse<Unit>) =>{
      this._toastr.success(message);
      this.inventoryModal.modal('hide');
      //this.newEvent.emit(null);
      this.resetForm();
      this.event.emit('refresh');
    },(error) =>{
      this.inventoryModal.modal('show');
      this.disableSave = false;
      this.event.emit('error');
    });
  }
  ngOnDestroy(){
    this.inventoryModal.modal('hide');
    this.inventoryModal.remove();
  }
}
