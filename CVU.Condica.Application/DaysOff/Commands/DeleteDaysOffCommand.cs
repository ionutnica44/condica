﻿using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.DaysOff.Commands
{
    public class DeleteDaysOffCommand : IRequest<Unit>
    {
        public int Id { get; set; }
    }

    public class DeleteDaysOffCommandHandler : RequestHandler<DeleteDaysOffCommand, Unit>
    {
        public DeleteDaysOffCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Unit> Handle(DeleteDaysOffCommand request, CancellationToken cancellationToken)
        {
            var daysOff = appDbContext.DaysOff.First(d => d.Id == request.Id);

            appDbContext.Remove(daysOff);

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class DeleteDaysOffCommandValidator : AbstractValidator<DeleteDaysOffCommand>
    {
        public DeleteDaysOffCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateDeleteDaysOffCommand);

            //RuleFor(x => x.Id).Must(validationService.VerifyStartDateGraterThenNowForDaysOff).WithMessage(ValidationMessages.DateHasPassed);
            //RuleFor(x => x.Id).Must(validationService.DayOffExists).WithMessage(ValidationMessages.DayOffNotInDatabase);
        }
    }

    public class DeleteDaysOffCommandAuthorization : AuthorizationSetup<DeleteDaysOffCommand>
    {
        public DeleteDaysOffCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
