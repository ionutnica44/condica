using CVU.Condica.Application.Account.Commands;
using CVU.Condica.Application.Account.Models;
using CVU.Condica.Application.Account.Queries;
using CVU.Condica.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CVU.Condica.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        [HttpPost("login")]
        public Task<TokenDto> Login([FromBody] LoginCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpGet("current-user")]
        public Task<UserDto> CurrentUser()
        {
            return Mediator.Send(new CurrentUserQuery());
        }

        [HttpPost("get-all")]
        public IEnumerable<UserDto> ListUsers([FromBody] ListUsersQuery request)
        {
            var mediatorResponse = Mediator.Send(request).Result;

            Response.Headers.AddPagination(mediatorResponse.PagedSummary);

            return mediatorResponse.Items;
        }

        [HttpGet("{id}")]
        public Task<UserDto> GetUser([FromRoute] int id)
        {
            return Mediator.Send(new UserQuery { UserId = id });
        }

        [HttpPost("register")]
        public Task<int> Register([FromBody] RegisterCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPost("activate")]
        public Task Activate([FromBody] ActivateCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPatch("edit-user")]
        public Task EditUser([FromBody] EditProfileCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPost("change-password")]
        public Task ChangePassword([FromBody] ChangePasswordCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPost("reset-password-code")]
        public Task ForgotPassword([FromBody] ResetPasswordSecurityCodeComamnd command)
        {
            return Mediator.Send(command);
        }

        [HttpPost("reset-password")]
        public Task ResetPasswordWithCode([FromBody] ResetPasswordCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPost("resend-activation")]
        public Task ResendActivation([FromBody] ResendActivationCommand command)
        {
            return Mediator.Send(command);
        }
    }
}
