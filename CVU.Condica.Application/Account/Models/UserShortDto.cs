﻿using CVU.Condica.Persistence.Models;
using System;
using System.Linq.Expressions;

namespace CVU.Condica.Application.Account.Models
{
    public class UserShortDto
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public string Company { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsBlocked { get; set; }
        public string Position { get; set; }

        public static Expression<Func<User, UserShortDto>> Projection
        {
            get
            {
                return p => new UserShortDto()
                {
                    UserId = p.Id,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    Role = p.Role.Name,
                    Email = p.Email,
                    Company = p.Company.Name,
                    PhoneNumber = p.PhoneNumber,
                    IsBlocked = p.IsBlocked,
                    Position = p.Position.Name

                };
            }
        }

        public static UserShortDto Create(User User)
        {
            return Projection.Compile().Invoke(User);
        }
    }
}
