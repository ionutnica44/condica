﻿using CVU.Condica.Application;
using CVU.Condica.Application.Account.Models;
using CVU.Condica.Application.Attendances.Commands;
using CVU.Condica.Application.Attendances.Models;
using CVU.Condica.Application.Attendances.Queries;
using CVU.Condica.Common.Setings;
using CVU.Condica.Persistence.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CVU.Condica.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class AttendancesController : BaseController
    {
        private readonly OfficeChatOptions officeChat;
        private readonly AppDbContext appDbContext;
        private readonly ISession session;

        public AttendancesController(IOptions<OfficeChatOptions> officeChatOptions, AppDbContext appDbContext, ISession session)
        {
            this.officeChat = officeChatOptions.Value;
            this.appDbContext = appDbContext;
            this.session = session;
        }

        [HttpPost("get-all")]
        public IEnumerable<object> ListAttendances([FromBody] AttendancesListQuery request)
        {
            var mediatorResponse = Mediator.Send(request).Result;

            return mediatorResponse;
        }

        [HttpPost("user-attendances")]
        public IEnumerable<object> GetUserAttendances([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            var request = new AttendancesListQuery(currentUser: true)
            {
                StartDate = startDate,
                EndDate = endDate
            };

            var mediatorResponse = Mediator.Send(request).Result;

            return mediatorResponse;
        }

        [HttpGet("{id}")]
        public Task<AttendanceDto> GetAttendance([FromRoute] int id)
        {
            return Mediator.Send(new AttendanceQuery { AttendanceId = id });
        }

        [HttpPost]
        public Task PostAttendance([FromBody] CreateAttendanceCommand command)
        {
            return Mediator.Send(command);
        }

        [HttpPatch("{id}")]
        public Task<Unit> EditAttendance([FromRoute] int id, [FromBody] EditAttendanceCommand command)
        {
            command.AttendanceId = id;
            return Mediator.Send(command);
        }

        [HttpGet("attendancesReport")]
        public FileContentResult GetAttendancesReport([FromQuery] DateTime dateTime)
        {
            var byteArr = Mediator.Send(new AttendancesReportQuery { DateTime = dateTime }).Result;
            return File(byteArr, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"Attendances report {dateTime.ToString("MMMM yyyy", CultureInfo.InvariantCulture)}.xlsx");
        }

        [HttpPost("officechat-checkin")]
        [AllowAnonymous]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult OfficeChatCheckin([FromForm] string data)
        {
            var mattermostId = Request.Form["user_id"];
            IEnumerable<MmUserDto> mattermostUsers;
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {officeChat.ApiToken}");
                var response = httpClient.GetAsync(officeChat.UsersUrl).Result;
                mattermostUsers = JsonConvert.DeserializeObject<IEnumerable<MmUserDto>>(response.Content.ReadAsStringAsync().Result);
            }
            var internalEmail = mattermostUsers.FirstOrDefault(d => d.Id == mattermostId).Email;
            var customUser = appDbContext.User.FirstOrDefault(d => d.Email == internalEmail);
            session.CurrentUser = new CurrentUser { Email = customUser.Email, CompanyId = customUser.CompanyId, Id = customUser.Id, Role = (Persistance.ReferenceData.Roles)customUser.RoleId };


            var resp = Mediator.Send(new SlashCommandCheckin { CustomUserEmail = internalEmail, ArrivalTime = DateTime.Now });
            return Ok(resp.Result);
        }

        [HttpPost("officechat-checkout")]
        [AllowAnonymous]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult OfficeChatCheckout([FromForm] string data)
        {
            var mattermostId = Request.Form["user_id"];
            IEnumerable<MmUserDto> mattermostUsers;
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {officeChat.ApiToken}");
                var response = httpClient.GetAsync(officeChat.UsersUrl).Result;
                mattermostUsers = JsonConvert.DeserializeObject<IEnumerable<MmUserDto>>(response.Content.ReadAsStringAsync().Result);
            }
            var internalEmail = mattermostUsers.FirstOrDefault(d => d.Id == mattermostId).Email;
            var customUser = appDbContext.User.FirstOrDefault(d => d.Email == internalEmail);
            session.CurrentUser = new CurrentUser { Email = customUser.Email, CompanyId = customUser.CompanyId, Id = customUser.Id, Role = (Persistance.ReferenceData.Roles)customUser.RoleId };

            var resp = Mediator.Send(new SlashCommandCheckout { CustomUserEmail = internalEmail, DepartureTime = DateTime.Now });
            return Ok(resp.Result);
        }

    }
}
