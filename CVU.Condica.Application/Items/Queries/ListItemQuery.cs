﻿using CVU.Condica.Application.Items.Models;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Pagination;
using CVU.Condica.Persistance.ReferenceData;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Items.Queries
{
    public class ListItemQuery : PaginatedQueryParameter, IRequest<PaginatedModel<ItemDto>>
    {
    }
    public class ListItemQueryHandler : RequestHandler<ListItemQuery, PaginatedModel<ItemDto>>
    {
        private readonly IPaginationService paginationService;

        public ListItemQueryHandler(IServiceProvider serviceProvider, IPaginationService paginationService) : base(serviceProvider)
        {
            this.paginationService = paginationService;
        }

        public async override Task<PaginatedModel<ItemDto>> Handle(ListItemQuery request, CancellationToken cancellationToken)
        {
            var item = appDbContext.Item
                .Include(d => d.User)
                .Include(d => d.ItemType)
                .AsQueryable();

            var paginatedResponse = paginationService.PaginatedResults<Persistence.Models.Item, ItemDto>(item, request);

            return new PaginatedModel<ItemDto>(paginatedResponse);
        }
    }

    public class ListItemQueryAuthorization : AuthorizationSetup<ListItemQuery>
    {
        public ListItemQueryAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
