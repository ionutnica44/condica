using System;
using System.Linq;

namespace CVU.Condica.Common.Extensions
{
    public static class StringExtensions
    {
        public static string SplitCamelCase(this string inputString)
        {
            return inputString.Aggregate(string.Empty, (result, next) =>
            {
                if (char.IsUpper(next) && result.Length > 0)
                {
                    result += ' ';
                }
                return result + next;
            });
        }

        public static string GenerateRandomString(int length)
        {
            var random = new Random();
            string chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
