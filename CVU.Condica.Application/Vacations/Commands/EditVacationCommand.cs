﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Vacations.Commands
{
    public class EditVacationCommand : IRequest<Unit>
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class EditVacationCommandHandler : RequestHandler<EditVacationCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public EditVacationCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(EditVacationCommand request, CancellationToken cancellationToken)
        {
            var vacation = appDbContext.Vacation.First(d => d.Id == request.Id);

            var numberOfVacationDays = vacationService.CalculateNumberOfVacationDays(request.StartDate, request.EndDate);

            vacationService.UpdateUserVacationDays(CurrentUser.Id, numberOfVacationDays, vacation.Id);

            vacation.StartDate = request.StartDate;
            vacation.EndDate = request.EndDate;
            vacation.NumberOfDays = numberOfVacationDays;

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class EditDaysOffCommandValidator : AbstractValidator<EditVacationCommand>
    {
        public EditDaysOffCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditVacationCommand);
        }
    }

    public class EditDaysOffCommandAuthorization : AuthorizationSetup<EditVacationCommand>
    {
        public EditDaysOffCommandAuthorization()
        {
        }
    }
}
