import { PasswordValidators } from './../../../helpers/password-validators';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ResetPasswordCommand, UserClient } from 'app/client-api';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit {
  registerConfirmForm = null;
  private code: string;
  resetModel: ResetPasswordCommand;
  loggingIn: boolean;
  constructor(private _account: UserClient, private _route: ActivatedRoute, private _router: Router, private _fb: FormBuilder, private _toastr: ToastrService) {
    this.registerConfirmForm = this._fb.group({
      code: [null, Validators.compose([Validators.maxLength(4), Validators.minLength(4)])],
      password: [null, Validators.compose([
        // 1. Password Field is Required
        Validators.required,
        // 2. check whether the entered password has a number
        PasswordValidators.patternValidator(/\d/, { hasNumber: true }),
        // 3. check whether the entered password has upper case letter
        PasswordValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        // 4. check whether the entered password has a lower-case letter
        PasswordValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
        // 5. check whether the entered password has a special character
        PasswordValidators.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
        // 6. Has a minimum length of 8 characters
        Validators.minLength(8)])
      ],
      confirmPassword: [null, Validators.compose([Validators.required])]
    },
      {
        // check whether our password and confirm password match
        validator: PasswordValidators.passwordMatchValidator,
        updateOn: 'blur'
      });
    this.resetModel = new ResetPasswordCommand();
    this.loggingIn = false;
  }

  ngOnInit() {
    this._route.queryParams.subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.code = params['SecurityCode'] || 0;
    });
  }

  changePassword() {
    this.resetModel.securityCode = this.code;
    this.loggingIn = true;
    this._account.resetPasswordWithCode(this.resetModel).subscribe((response) => {
      this._toastr.success('Password was changed', 'Succes');
      setTimeout(() => {
        this._router.navigateByUrl('/auth/login');
      }, 300);
      this.loggingIn = false;
    }, (e) => {
      this.loggingIn = false;
    });
  }

}
