using System;
namespace CVU.Condica.Application.Account.Models
{
    public class TokenDto
    {
        public string AccessToken { get; set; }
        public DateTime Expires { get; set; }
        public UserDto User { get; set; }
    }
}
