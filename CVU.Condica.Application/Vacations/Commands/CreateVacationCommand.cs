﻿using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistence.Models;
using CVU.Condica.Persistence.ReferenceData;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Vacations.Commands
{
    public class CreateVacationCommand : IRequest<Unit>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public static Vacation Create(CreateVacationCommand model)
        {
            return new Vacation
            {
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                StatusId = (int)VacationsStatus.Pending
            };
        }
    }

    public class CreateVacationCommandHandler : RequestHandler<CreateVacationCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public CreateVacationCommandHandler(
            IServiceProvider serviceProvider,
            IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(CreateVacationCommand request, CancellationToken cancellationToken)
        {
            var vacation = CreateVacationCommand.Create(request);

            vacation.UserId = CurrentUser.Id;

            vacation.NumberOfDays = vacationService.CalculateNumberOfVacationDays(request.StartDate, request.EndDate);

            var signingUsers = appDbContext.SigningUser
                        .Include(d => d.Signing)
                        .Where(d => d.UserId == vacation.UserId)
                        .ToList()
                        .Select(d => (d.SigningUserId, d.Signing.SignPriorityId.Value))
                        .AsEnumerable();

            vacation.VacationRequestApproval.AddNextSigningUsers(signingUsers);

            if (vacation.VacationRequestApproval.Count == 0)
            {
                vacation.StatusId = (int)VacationsStatus.Approved;
            }

            appDbContext.Vacation.Add(vacation);

            vacationService.UpdateUserVacationDays(vacation.UserId, vacation.NumberOfDays);

            appDbContext.SaveChanges();

            vacationService.SendVacationRequestEmail(vacation.Id);

            return Unit.Value;
        }
    }

    public class CreateVacationCommandValidator : AbstractValidator<CreateVacationCommand>
    {
        public CreateVacationCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateCreateVacationCommand);
        }
    }

    public class CreateVacationCommandAuthorization : AuthorizationSetup<CreateVacationCommand>
    {
        public CreateVacationCommandAuthorization()
        {
        }
    }
}