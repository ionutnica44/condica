import { AuthRoutingModule } from './auth/auth-routing.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthComponent } from './auth/auth.component';
import { AdminRoutingModule } from './admin/admin-routing.module';

const routes: Routes = [

  // { path: '', component: AuthComponent, pathMatch: "full" },
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },

  { path: "**", redirectTo: "auth/login" },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AuthRoutingModule,
    AdminRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
