using CVU.Condica.Application.Enums;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Security;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace CVU.Condica.Application.Account.Commands
{
    public class ResetPasswordCommand : IRequest<Unit>
    {
        public string SecurityCode { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordCommandHandler : RequestHandler<ResetPasswordCommand, Unit>
    {

        public ResetPasswordCommandHandler(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override async Task<Unit> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var User = appDbContext.User.First(f => f.SecurityCode == request.SecurityCode);

            var newPassword = Crypto.HashPassword(AuthorizationVariables.Salt + request.Password);

            User.Password = newPassword;
            User.SecurityCodeExpiresAt = null;
            User.SecurityCode = null;

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class ResetPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
    {
        public ResetPasswordCommandValidator(ValidationService validationService)
        {
            RuleFor(x => x).Custom(validationService.ValidateResetPasswordCommand);
        }
    }
}
