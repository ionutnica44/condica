using CVU.Condica.Application.Account.Commands;
using CVU.Condica.Common.Setings;
using CVU.Condica.Infrastructure.BackgroundWorkers;
using CVU.Condica.Persistence.Models;
using CVU.Condica.WebUI.Config;
using CVU.Utilities.Email;
using CVU.Utilities.Filters;
using CVU.Utilities.Security;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NSwag;
using NSwag.Generation.Processors.Security;
using System;
using System.Text;

namespace CVU.Condica.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["SecurityKey"]));
        }

        public IConfiguration Configuration { get; }

        private readonly SymmetricSecurityKey _securityKey;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SmtpOptions>(this.Configuration.GetSection("Smtp"));
            services.Configure<OfficeChatOptions>(this.Configuration.GetSection("OfficeChat"));

            ServicesSetup.ConfigureEntity(services, Configuration);
            ServicesSetup.ConfigurePolicies(services);
            ServicesSetup.ConfigureInjection(services);

            var tokenConfigurationOptions = Configuration.GetSection(nameof(TokenProviderOptions));
            services.Configure<TokenProviderOptions>(options =>
            {
                options.Issuer = tokenConfigurationOptions[nameof(TokenProviderOptions.Issuer)];
                options.Audience = tokenConfigurationOptions[nameof(TokenProviderOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_securityKey, SecurityAlgorithms.HmacSha256);
            });

            // Add services
            services.AddHostedService<VacationUpdaterService>();
            services.AddCors();
            services.AddMediatR(typeof(RegisterCommandHandler).Assembly);
            services.AddOptions();
            services.AddMemoryCache();

            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(CustomExceptionFilter));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<RegisterCommandValidator>())
                .AddNewtonsoftJson(options =>
                 {
                     options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                     options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

                 });
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwt =>
            {

                jwt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = !string.IsNullOrEmpty(tokenConfigurationOptions[nameof(TokenProviderOptions.Issuer)]),
                    ValidIssuer = tokenConfigurationOptions[nameof(TokenProviderOptions.Issuer)],

                    ValidateAudience = !string.IsNullOrEmpty(tokenConfigurationOptions[nameof(TokenProviderOptions.Audience)]),
                    ValidAudience = tokenConfigurationOptions[nameof(TokenProviderOptions.Audience)],

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = _securityKey,

                    ValidateLifetime = true,

                    ClockSkew = TimeSpan.Zero,
                    AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                };
            });

            // Customise default API behavour
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddSwaggerDocument(document =>
            {
                // Add an authenticate button to Swagger for JWT tokens
                document.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT"));
                document.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}. You can get a JWT token from /Authorization/Authenticate.",
                }));

                // Post process the generated document
                document.PostProcess = d => d.Info.Title = "Condica API REST Client!";
            });


            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "wwwroot";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppDbContext appDbContext)
        {
            appDbContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            //app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseAuthentication();

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseSwaggerUi3(settings =>
            {
                settings.Path = "/api";
                settings.DocumentPath = "/api/specification.json";
            });

            app.UseRouting();
            app.UseEndpoints(routes =>
            {
                routes.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwaggerUi3();
            app.UseOpenApi();

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });
        }
    }
}
