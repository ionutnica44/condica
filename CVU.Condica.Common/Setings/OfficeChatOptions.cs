﻿namespace CVU.Condica.Common.Setings
{
    public class OfficeChatOptions
    {
        public string ApiToken { get; set; }
        public string UsersUrl { get; set; }
        public string IpAddress { get; set; }
    }
}
