
export const windowSizeListener = () => {
    window.addEventListener('resize', SidebarChange);
    window.addEventListener('resize', SidebarCloseOnClickItem);
    window.addEventListener('resize', ChangeButtonsPosition);
};

export const windowLoadListener = () => {
    window.addEventListener('load', () => {
        // windowSizeListener();
        SidebarChange();
        SidebarCloseOnClickItem();
        ChangeButtonsPosition();

    });
};

export const ChangeButtonsPosition = () => {
    const width = window.innerWidth;
    const buttons = ($('.buttons') as any);
    if (buttons) {
        if (width < 1230) {
            buttons.addClass('vertical');
            buttons.find('.or').hide();
        } else {
            buttons.removeClass('vertical');
            buttons.find('.or').show();
        }
    }
};


export const SidebarChange = () => {
    const width = window.innerWidth;
    const sidebar = ($('#sidebar') as any);
    if (sidebar) {
        if (width < 991) {
            sidebar.addClass('mobile-status').removeClass('visible');
            $('.pusher').addClass('tweaked');
            sidebar.sidebar({
                context: ".dashboard",
                transition: "overlay",
            });
        } else {
            sidebar.addClass('visible').removeClass('mobile-status');
            $('.pusher').removeClass('tweaked');
            sidebar.sidebar({
                context: ".dashboard",
                transition: "push",
                closable: false,
                dimPage: false,
                useLegacy: true,
                scrollLock: false
            });
        }
    }
};


export const SidebarCloseOnClickItem = () => {
    const sidebar = '.ui.sidebar';
    $(document).on('click', '.dashboard .mobile-status a.item', function () {
        ($(sidebar) as any).sidebar('toggle');
    });
};

export function disableWeekends(date: Date, type: 'day' | 'month' | 'year') {
    let condition = new Date(date).getDay() === 0 || new Date(date).getDay() === 6;
    condition = condition && type === 'day';
    return condition;
}

Date.prototype['addDays'] = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

export function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}