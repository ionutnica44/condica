﻿using CVU.Condica.Application.DaysOff.Models;
using CVU.Condica.Application.Vacations.Utils;
using CVU.Condica.Application.Validation;
using CVU.Condica.Common.Authorization;
using CVU.Condica.Persistance.ReferenceData;
using CVU.Condica.Persistence.Models;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.DaysOff.Commands
{
    public class EditDaysOffCommand : DaysOffBaseModel, IRequest<Unit>
    {
        public int Id { get; set; }

    }

    public class EditDaysOffCommandHandler : RequestHandler<EditDaysOffCommand, Unit>
    {
        private readonly IVacationService vacationService;

        public EditDaysOffCommandHandler(IServiceProvider serviceProvider, IVacationService vacationService) : base(serviceProvider)
        {
            this.vacationService = vacationService;
        }

        public override async Task<Unit> Handle(EditDaysOffCommand request, CancellationToken cancellationToken)
        {
            var daysOff = appDbContext.DaysOff
                .Include(d => d.UserDaysOff)
                .First(d => d.Id == request.Id);

            daysOff.Name = request.Name;
            daysOff.StartDate = request.StartDate;
            daysOff.EndDate = request.EndDate;
            daysOff.NumberOfDays = vacationService.CalculateNumberOfBusinessDays(request.StartDate, request.EndDate);

            if (request.ForEveryone)
            {
                appDbContext.UserDaysOff.RemoveRange(daysOff.UserDaysOff);
            }

            if (request.CustomUsersIds != null)
            {
                appDbContext.UserDaysOff.RemoveRange(daysOff.UserDaysOff);
                var userDaysOffToAdd = new List<UserDaysOff>();
                foreach (var customUserId in request.CustomUsersIds)
                {
                    userDaysOffToAdd.Add(new UserDaysOff
                    {
                        DayOffId = daysOff.Id,
                        UserId = customUserId
                    });
                }
                appDbContext.UserDaysOff.AddRange(userDaysOffToAdd);
            }

            appDbContext.SaveChanges();

            return Unit.Value;
        }
    }

    public class EditDaysOffCommandValidator : AbstractValidator<EditDaysOffCommand>
    {
        public EditDaysOffCommandValidator(ValidationService validationService, AppDbContext appDbContext)
        {
            RuleFor(x => x).Custom(validationService.ValidateEditDaysOffCommand);

            //RuleFor(x => x.Name).NotNull();

            //RuleFor(x => x)
            //    .Must(x => !validationService.DayOffIntersectsExistingDayOff(x.StartDate.ToLocalTime(), x.EndDate.ToLocalTime(), x.Id))
            //    .When(d => !d.CustomUsersIds.Any() || !d.ForEveryone)
            //    .WithMessage(ValidationMessages.DayOffAlreadyExists);

            //RuleFor(x => x.Id).Must(validationService.DayOffExists).WithMessage(ValidationMessages.DayOffNotInDatabase);

            //RuleFor(x => x)
            //    .Custom((x, customContext) => validationService.CustomDayOffIntersectsExistingDayOff(x, customContext));

            //RuleFor(x => x).Must(x => validationService.DateHasPassed(x.StartDate.ToLocalTime())).WithMessage(ValidationMessages.DateHasPassed)
            //    .Must(x => validationService.IsWeekend(x.StartDate.ToLocalTime())).WithMessage(ValidationMessages.IsWeekend);

            //RuleFor(x => x).Must(x => validationService.DateHasPassed(x.EndDate)).WithMessage(ValidationMessages.DateHasPassed)
            //    .Must(x => validationService.IsWeekend(x.EndDate)).WithMessage(ValidationMessages.IsWeekend);

            //RuleFor(x => x)
            //    .Must(x => validationService.EndDateEarlierThanStartDateForDaysOff(x.StartDate, x.EndDate, x.Id))
            //    .WithMessage(ValidationMessages.EndDateAfterStartDate);

            //RuleFor(x => x).Must(x=> !validationService.DayOffTypeChanged(x)).WithMessage("You cannot change the day off type. Please recreate it.");
            //RuleFor(x => x).Must(validationService.ValidateDaysOff).WithMessage(ValidationMessages.DaysOffMultipleUsers);

        }
    }

    public class EditDaysOffCommandAuthorization : AuthorizationSetup<EditDaysOffCommand>
    {
        public EditDaysOffCommandAuthorization()
        {
            NeedsRole((int)Roles.Administrator);
        }
    }
}
