using CVU.Condica.Common.Authorization;
using CVU.Condica.Common.Setings;
using CVU.Utilities.ErrorHandling;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace CVU.Condica.Application.Infrastructure
{
    public class RequestAuthorizationBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly ISession currentRequest;
        private readonly IOptions<OfficeChatOptions> opt;

        public RequestAuthorizationBehaviour(ISession currentRequest, IOptions<OfficeChatOptions> opt)
        {
            this.currentRequest = currentRequest;
            this.opt = opt;
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var authorizationService = new AuthorizationService<TRequest>(request);

            if (authorizationService.AllowAnnonymous)
            {
                return next();
            }

            if (currentRequest.CurrentUser == null && !authorizationService.AllowAnnonymous)
            {
                throw new AppException("You must be logged in to view this section", HttpStatusCode.Unauthorized);
            }

            var currentUser = currentRequest.CurrentUser;

            if (!authorizationService.ValidateCustomAuthorizationConditions(request, (int)currentUser.Role))
            {
                throw new AppException("You are not allowed to view or modify this section", HttpStatusCode.Forbidden);
            }

            if (authorizationService.NeedsInternalIp && currentRequest.HttpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() != opt.Value.IpAddress)
            {
                throw new AppException("You are not allowed to view or modify this section using this ip address", HttpStatusCode.Forbidden);

            }
            if (!authorizationService.Roles.Any())
            {
                return next();
            }


            if (!authorizationService.Roles.Contains((int)currentUser.Role))
            {
                throw new AppException("You are not allowed to view or modify this section", HttpStatusCode.Forbidden);
            }




            return next();
        }

    }
}
