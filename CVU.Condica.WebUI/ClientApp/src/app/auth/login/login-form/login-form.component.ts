import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'services/auth/auth.service';
import { UserClient, LoginCommand, IUserDto, SwaggerResponse, TokenDto } from 'app/client-api';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent implements OnInit {

  user: IUserDto;
  loginForm = null;
  loginModel: LoginCommand;
  loggingIn: boolean;

  constructor(private _toastr: ToastrService, private _fb: FormBuilder, private _account: UserClient, private _router: Router, private _authService: AuthService) {

    this.loginForm = _fb.group({
      userName: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      keepSessionActive: [null]
    }, { updateOn: 'submit' });

    this.loginModel = new LoginCommand();
    this.loggingIn = false;
  }

  ngOnInit() {

  }

  logIn() {
    this.loggingIn = true;
    console.log('loginModel', this.loginModel);
    this._account.login(this.loginModel).subscribe((response: SwaggerResponse<TokenDto>) => {
      this._authService.successAuth(response);
      this.loggingIn = false;
      this._router.navigateByUrl('/admin/home');
      this._toastr.success('You are logged!', 'Info');
      // const isAdmin = this._authService.hasRole('FacilityManager') || this._authService.hasRole('BuildingManager');
      // if (isAdmin) {
      //   this._router.navigateByUrl('/admin/home');
      //   this._toastr.success('You are logged as admin', 'Info');
      // } else {
      //   this._router.navigateByUrl('/admin/tickets/all-open');
      //   this._toastr.success('You are logged!', 'Info');
      // }
    }, (e) => {
      this.loggingIn = false;
    });
  }
}

