using Microsoft.EntityFrameworkCore;

namespace CVU.Condica.Persistence.Models
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }

        public virtual DbSet<AccessLogEntries> AccessLogEntries { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<SigningUser> SigningUser { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<DaysOff> DaysOff { get; set; }
        public virtual DbSet<Vacation> Vacation { get; set; }
        public virtual DbSet<VacationStatus> VacationStatus { get; set; }
        public virtual DbSet<VacationRequestApproval> VacationRequestApproval { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<Attendance> Attendance { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<SignPriority> SignPriority { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemType> ItemType { get; set; }
        public virtual DbSet<VacationUserManagement> VacationUserManagement { get; set; }
        public virtual DbSet<UserDaysOff> UserDaysOff { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }
    }
}
