import { AttendancesClient, Unit, CreateAttendanceCommand } from './../../../client-api';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { EditAttendanceCommand, SwaggerResponse } from 'app/client-api';
import { ToastrService } from 'ngx-toastr';
import { startOfDay, endOfDay } from 'date-fns';
import { type } from 'os';
import * as moment from 'moment';

@Component({
  selector: 'app-attendances-modal',
  templateUrl: './attendances-modal.component.html'
})
export class AttendancesModalComponent implements OnInit, OnDestroy {
  clonedAttendance: EditAttendanceCommand = new EditAttendanceCommand();
  @Output() eventOut: EventEmitter<string> = new EventEmitter();
  //minTime = moment();
  //maxTime = moment();
  attendanceId: number;
  disableSave = false;
  private attendanceModal: any;
  constructor(private _attendancesClient: AttendancesClient, private _toastr: ToastrService) { 
    this.disableSave = false;
  }

  ngOnInit() {
    setTimeout(() => {
      this.attendanceModal = ($('.attendance') as any).modal({ closable: false });
    }, 200);
    //this.minTime.set({ hour: 7, minute: 0, second: 0, millisecond: 0 })
    //this.maxTime.set({ hour: 11, minute: 0, second: 0, millisecond: 0 })
  }


  editAttendanceModal(event) {
    this.clonedAttendance = new EditAttendanceCommand();
    this.clonedAttendance.fromHome = false;
    let min = event[0].start ? moment(event[0].start) : moment();
    min = min.set({ hour: 8, minute: 0, second: 0, millisecond: 0 });
    let max = event[0].end ? moment(event[0].end) : moment(event[0].start);
    max = max.set({ hour: 23, minute: 59, second: 0, millisecond: 0 });
    
    this.blabla('#arrival', new Date(max.toISOString()), new Date(min.toISOString()),'arrivalTime');
    this.blabla('#departure',new Date(max.toISOString()), new Date(min.toISOString()),'departureTime');
    this.clonedAttendance.attendanceId = event[0].id;
    this.clonedAttendance.fromHome = event[0].fromHome;
    this.clonedAttendance.arrivalTime = event[0].start;
    this.clonedAttendance.departureTime = event[0].end;
    this.iniEditCalendar(event);
    this.attendanceModal.modal('show');
  }

  addAttendance(date: Date) {
    this.clonedAttendance = new CreateAttendanceCommand();
    this.clonedAttendance.fromHome = false;
    let min = date ? moment(date) : moment();
    min = min.set({ hour: 8, minute: 0, second: 0, millisecond: 0 });
    let max = date ? moment(date) : moment();
    max = max.set({ hour: 23, minute: 59, second: 0, millisecond: 0 });
    this.blabla('#arrival', new Date(max.toISOString()), new Date(min.toISOString()),'arrivalTime');
    this.blabla('#departure', new Date(max.toISOString()), new Date(min.toISOString()),'departureTime');
    this.attendanceModal.modal('show');

  }

  save() {

    if (this.clonedAttendance.attendanceId) {
      this.disableSave = true;
      this._attendancesClient.editAttendance(this.clonedAttendance.attendanceId, this.clonedAttendance).subscribe((response: SwaggerResponse<Unit>) => {
        this.disableSave = false;
        this.attendanceModal.modal('hide');
        this.eventOut.emit('refresh');
        this.resetCalendarPicker();
        this._toastr.success('Edit successful');
      }, (e) => {
      //  this._toastr.error('Error');
        this.eventOut.emit('error');
        this.disableSave = false;
      });
    } else {
      this.disableSave = true;
      this._attendancesClient.postAttendance(this.clonedAttendance).subscribe((response: SwaggerResponse<void>) => {
        this.disableSave = false;
        this.attendanceModal.modal('hide');
        this.eventOut.emit('refresh');
        this.resetCalendarPicker();
        this._toastr.success('Added successfully');
      }, (e) => {
      //  this._toastr.error('Error');
        this.eventOut.emit('error');
        this.disableSave = false;
      });
    }
  }

  blabla(id: string, maxDate: Date, minDate: Date, changeProperty: string ){
    ($(id) as any).calendar({
      type: 'datetime',
      startMode: 'hour',
       maxDate,
       minDate,
    //   tslint:disable-next-line:no-shadowed-variable
      onChange: (date: Date) => {
        this.clonedAttendance[changeProperty] = date;
      },
    });
  }
  iniEditCalendar(att) {
    ($('#arrival') as any).calendar('set date', att[0].start);
    ($('#departure') as any).calendar('set date', att[0].end);
  }


  resetCalendarPicker() {
    ($('#arrival') as any).calendar('clear');
    ($('#departure') as any).calendar('clear');
  }

  onClickCancel() {
    this.resetCalendarPicker();
    this.attendanceModal.modal('hide');

  }

  ngOnDestroy() {
    if (this.attendanceModal) {
      this.resetCalendarPicker();
      this.attendanceModal.modal('hide');
      this.attendanceModal.remove();
    }
  }
}
