export const findObjectDiff = (obj1, obj2) => {
    let diff = {};
    if (obj1 && obj2) {
        if (typeof obj1 === typeof obj2) {
            for (let field in obj1) {
                if (field === 'createdAt' || field === 'updatedAt') {
                    continue;
                }
                if (obj2[field] !== undefined) {
                    if (obj1[field] !== obj2[field]) {
                        diff[field] = obj2[field];
                    }
                }
            }
        }
    }
    return diff;
};